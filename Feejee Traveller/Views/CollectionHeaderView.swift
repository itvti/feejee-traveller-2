//
//  CollectionHeaderView.swift
//  Feejee Traveller
//
//  Created by Vinay Prasad on 5/8/18.
//  Copyright © 2018 Vinay Prasad. All rights reserved.
//

import UIKit

class CollectionHeaderView: UICollectionReusableView {
    @IBOutlet weak var titleLabel: UILabel!
}
