//
//  Stories.swift
//  Feejee Traveller
//
//  Created by Vinay Prasad on 9/5/18.
//  Copyright © 2018 Vinay Prasad. All rights reserved.
//

import UIKit

struct Stories {
    var storyID: String
    var storyTitle: String
    var storyDescription: String
    var storyImage: String
    var storyLink: String
    var storyLangID: String
    
    
}

extension Stories{
    init(){
        self.storyID = ""
        self.storyTitle = ""
        self.storyDescription = ""
        self.storyImage = ""
        self.storyLink = ""
        self.storyLangID = ""
    }
}

