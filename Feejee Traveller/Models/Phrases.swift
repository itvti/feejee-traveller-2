//
//  Phrases.swift
//  Feejee Traveller
//
//  Created by Vinay Prasad on 9/5/18.
//  Copyright © 2018 Vinay Prasad. All rights reserved.
//

import UIKit

struct Phrases {
    var phraseID: String
    var phraseWord: String
    var phraseTranslation: String
    var phraseLangID: String
    
    
}

extension Phrases{
    init(){
        self.phraseID = ""
        self.phraseWord = ""
        self.phraseTranslation = ""
        self.phraseLangID = ""
    }
}


