//
//  BagViewController.swift
//  Feejee Traveller
//
//  Created by Vinay Prasad on 28/3/18.
//  Copyright © 2018 Vinay Prasad. All rights reserved.
//

import UIKit

class BagViewController: UIViewController {
    @IBOutlet weak var tvOptions: UITableView!
    @IBOutlet weak var cvOptions: UICollectionView!
    
    let options = ["Shopping", "EatingOut", "NightLife", "Flights", "TravelGuide", "Events"]
    @IBOutlet weak var btnLanguage: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.updateImage()
        
     
        //getData()
    }
    override func viewDidAppear(_ animated: Bool) {
      // self.getData()

        self.updateImage()
        
        DispatchQueue.global(qos: .userInitiated).async {
            FTDataManager.checkBusinessUpdates()
            
            DispatchQueue.main.async {
                self.getData()
            }
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getData(){
        self.title = FTDataManager.getTBL(phrase: "FeejeeBag").uppercased()
        if(UIDevice.current.userInterfaceIdiom == .phone){
            tvOptions.isHidden = false
            tvOptions.delegate = self
            tvOptions.dataSource = self
            tvOptions.reloadData()
        }
        else{
            cvOptions.isHidden = false
            cvOptions.delegate = self
            cvOptions.dataSource = self
            cvOptions.reloadData()
        }
    }
    func reloadViews(){
        if(UIDevice.current.userInterfaceIdiom == .phone){
           
            tvOptions.reloadData()
        }
        else{
          
            cvOptions.reloadData()
        }
    }
    func updateImage(){
        switch FTDataManager.languageID {
        case "1":
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "english").withRenderingMode(.alwaysOriginal), style:.plain, target: self, action: #selector(changeLanguage))
            self.getData()
               // self.reloadViews()

        case "2":
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "chinese").withRenderingMode(.alwaysOriginal), style:.plain, target: self, action: #selector(changeLanguage))
               self.getData()
//                self.tvOptions.reloadData()
//                self.cvOptions.reloadData()
         //   self.getData()

        case "3":
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "japanese").withRenderingMode(.alwaysOriginal), style:.plain, target: self, action: #selector(changeLanguage))
               self.getData()
//                self.tvOptions.reloadData()
//                self.cvOptions.reloadData()
           

        default:
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "english").withRenderingMode(.alwaysOriginal), style:.plain, target: self, action: #selector(changeLanguage))
//                self.tvOptions.reloadData()
//                self.cvOptions.reloadData()
        }
    }
    @IBAction func changeLanguage() {
        let enAction = UIAlertAction(title: FTDataManager.getTranslationByLanguage(phrase: "English", langID: FTDataManager.languageID),
                                     style: .default) { (action) in
                                        FTDataManager.saveLanguage(langID: "1")
                                        self.updateImage()
                                        
        }
        let zhAction = UIAlertAction(title: FTDataManager.getTranslationByLanguage(phrase: "Chinese", langID: FTDataManager.languageID),
                                     style: .default) { (action) in
                                        FTDataManager.saveLanguage(langID: "2")
                                        self.updateImage()
        }
        let jpAction = UIAlertAction(title: FTDataManager.getTranslationByLanguage(phrase: "Japanese", langID: FTDataManager.languageID),
                                     style: .default) { (action) in
                                        FTDataManager.saveLanguage(langID: "3")
                                        self.updateImage()
                                        
                                        
        }
        let cancelAction = UIAlertAction(title: FTDataManager.getTBL(phrase: "Cancel"),
                                         style: .cancel) { (action) in
                                            // Respond to user selection of the action
        }
        
        var alert = UIAlertController(title: FTDataManager.getTBL(phrase: "SelectLanguage"),
                                      message: "",
                                      preferredStyle: .actionSheet)
        if(UIDevice.current.userInterfaceIdiom == .pad){
            alert = UIAlertController(title: FTDataManager.getTBL(phrase: "SelectLanguage"),
                                      message: "",
                                      preferredStyle:  .alert)
        }
        
        alert.addAction(enAction)
        alert.addAction(zhAction)
        alert.addAction(jpAction)
        
        alert.addAction(cancelAction)
        
        // On iPad, action sheets must be presented from a popover.
        //alert.popoverPresentationController?.barButtonItem = self.btnLanguage
        
        self.present(alert, animated: true) {
            // The alert was presented
        }
    }
}
extension BagViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(indexPath.section == 0){
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "placesVC") as? PlacesViewController
            controller?.placeTypeID = "3"
            controller?.titleTag = options[indexPath.section]
            let backItem = UIBarButtonItem()
            backItem.title = ""
            navigationItem.backBarButtonItem = backItem
            self.navigationController?.pushViewController(controller!, animated: true)
        }
        if(indexPath.section == 1){
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "placesVC") as? PlacesViewController
            controller?.placeTypeID = "5"
            controller?.titleTag = options[indexPath.section]
            let backItem = UIBarButtonItem()
            backItem.title = ""
            navigationItem.backBarButtonItem = backItem
            self.navigationController?.pushViewController(controller!, animated: true)
        }
        if(indexPath.section == 2){
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "placesVC") as? PlacesViewController
            controller?.placeTypeID = "10"
            controller?.titleTag = options[indexPath.section]
            
            let backItem = UIBarButtonItem()
            backItem.title = ""
            navigationItem.backBarButtonItem = backItem
            self.navigationController?.pushViewController(controller!, animated: true)
        }
        if(indexPath.section ==  3){
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "flightsVC") as? FlightsViewController
            let backItem = UIBarButtonItem()
            backItem.title = ""
            navigationItem.backBarButtonItem = backItem
            self.navigationController?.pushViewController(controller!, animated: true)
        }
        if(indexPath.section == 4){
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "guideVC") as? TravelGuideVC
            let backItem = UIBarButtonItem()
            backItem.title = ""
            navigationItem.backBarButtonItem = backItem
            self.navigationController?.pushViewController(controller!, animated: true)
        }
        if(indexPath.section == 5){
            var storyboard = UIStoryboard(name: "Main", bundle: nil)
            
            if(UIDevice.current.userInterfaceIdiom == .pad){
                 storyboard = UIStoryboard(name: "Main_iPad", bundle: nil)
            }
            let controller = storyboard.instantiateViewController(withIdentifier: "eventsVC") as? EventsVC
            let backItem = UIBarButtonItem()
            backItem.title = ""
            navigationItem.backBarButtonItem = backItem
            self.navigationController?.pushViewController(controller!, animated: true)
        }
       
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 10
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
}
extension BagViewController: UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return options.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "bagCell") as! FTTableViewCell
        
        cell.label1.text =  FTDataManager.getTBL(phrase: options[indexPath.section]).uppercased()
        cell.imgView1.image = UIImage(named: options[indexPath.section])
        
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
}

extension BagViewController: UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if(indexPath.item == 0){
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "placesVC") as? PlacesViewController
            controller?.placeTypeID = "3"
            controller?.titleTag = options[indexPath.item]
            let backItem = UIBarButtonItem()
            backItem.title = ""
            navigationItem.backBarButtonItem = backItem
            self.navigationController?.pushViewController(controller!, animated: true)
        }
        if(indexPath.item == 1){
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "placesVC") as? PlacesViewController
            controller?.placeTypeID = "5"
            controller?.titleTag = options[indexPath.item]
            let backItem = UIBarButtonItem()
            backItem.title = ""
            navigationItem.backBarButtonItem = backItem
            self.navigationController?.pushViewController(controller!, animated: true)
        }
        if(indexPath.item == 2){
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "placesVC") as? PlacesViewController
            controller?.placeTypeID = "10"
            controller?.titleTag = options[indexPath.item]
            let backItem = UIBarButtonItem()
            backItem.title = ""
            navigationItem.backBarButtonItem = backItem
            self.navigationController?.pushViewController(controller!, animated: true)
        }
        if(indexPath.item ==  3){
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "flightsVC") as? FlightsViewController
            //controller?.titleTag = options[indexPath.item]
            let backItem = UIBarButtonItem()
            backItem.title = ""
            
            navigationItem.backBarButtonItem = backItem
            self.navigationController?.pushViewController(controller!, animated: true)
        }
        if(indexPath.item == 4){
            let storyboard = UIStoryboard(name: "Main_iPad", bundle: nil)
   
            let controller = storyboard.instantiateViewController(withIdentifier: "guideVC") as? TravelGuideVC
            let backItem = UIBarButtonItem()
            backItem.title = ""
            navigationItem.backBarButtonItem = backItem
            self.navigationController?.pushViewController(controller!, animated: true)
        }
        if(indexPath.item == 5){
            let storyboard = UIStoryboard(name: "Main_iPad", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "eventsVC") as? EventsVC
            let backItem = UIBarButtonItem()
            backItem.title = ""
            navigationItem.backBarButtonItem = backItem
            self.navigationController?.pushViewController(controller!, animated: true)
        }
    }
}

extension BagViewController: UICollectionViewDataSource{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return options.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "bagCell", for: indexPath) as! FTCollectionViewCell
        
        cell.label1.text = FTDataManager.getTBL(phrase: options[indexPath.item]).uppercased()
        cell.imgView1.image = UIImage(named: options[indexPath.item])!
        
        return cell
    }
    
}

extension BagViewController: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (cvOptions.frame.size.width - 40 ) / 3
        
        
        return CGSize(width: width , height: width * 0.75 )
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        
        let cellHeight = ((cvOptions.frame.size.width - 40 ) / 3 ) * 0.75
        let height = cvOptions.frame.size.height - (cellHeight * 2)


        return UIEdgeInsets(top: height / 3, left: 0, bottom: height / 3, right: 0)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        let cellHeight = ((cvOptions.frame.size.width - 40 ) / 3 ) * 0.75
        let height = cvOptions.frame.size.height - (cellHeight * 2)


        return height / 3
    }
}
