//
//  PlacesViewController.swift
//  Feejee Traveller
//
//  Created by Vinay Prasad on 28/3/18.
//  Copyright © 2018 Vinay Prasad. All rights reserved.
//

import UIKit
import SwiftyJSON
import Kingfisher

class PlacesViewController: UIViewController {
    @IBOutlet weak var cvPlaces: UICollectionView!
    @IBOutlet weak var txtIntro: UITextView!
    
    var arrPlaces : [Business] = [Business()]
    var placeTypeID : String = ""
    var HUD = LottieHUD("spinner")
    var titleTag : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        cvPlaces.delegate = self
        cvPlaces.dataSource = self
        
        HUD.size = CGSize(width: 50, height: 50)
        HUD.contentMode = .scaleAspectFill
      
        
        
        
        DispatchQueue.global(qos: .userInitiated).async {
              self.HUD.showHUD()
            FTDataManager.checkBusinessUpdates()
            self.getData()
            
            DispatchQueue.main.async {
                self.updateImage()
                self.HUD.stopHUD()
            }
        }

    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
       
       
        getData()

    }
    
    func getData(){
        if(UIDevice.current.userInterfaceIdiom == .pad){
                txtIntro.font = UIFont(name: "AvenirNext-Regular", size: 22)
        }
               
              
               switch titleTag{
               case "Shopping" :
                   txtIntro.text = FTDataManager.getTBL(phrase: "DutyFreeIntro")
                   self.arrPlaces = FTDataManager.getPlacesDataByLanguageAndType(langID: FTDataManager.languageID, typeID: self.placeTypeID)
               case "EatingOut" :
                   txtIntro.text = FTDataManager.getTBL(phrase: "EatIntro")
                   self.arrPlaces = FTDataManager.getPlacesDataByLanguageAndType(langID: FTDataManager.languageID, typeID: self.placeTypeID) +  FTDataManager.getPlacesDataByLanguageAndType(langID: FTDataManager.languageID, typeID: "11")
               case "NightLife" :
                   txtIntro.text = FTDataManager.getTBL(phrase: "BarIntro")
                   self.arrPlaces = FTDataManager.getPlacesDataByLanguageAndType(langID: FTDataManager.languageID, typeID: self.placeTypeID)
               default:
                   txtIntro.text = FTDataManager.getTBL(phrase: "BulaPlacesIntro")
                   self.arrPlaces = FTDataManager.getPlacesDataByLanguageAndType(langID: FTDataManager.languageID, typeID: self.placeTypeID)
               }
               
        
        let temp = arrPlaces
        arrPlaces = temp.sorted(by: {  $0.bName < $1.bName })
        
        
          DispatchQueue.main.async {
            self.title = FTDataManager.getTBL(phrase: self.titleTag).uppercased()
            self.txtIntro.scrollRangeToVisible(NSMakeRange(0, 0))
            self.cvPlaces.reloadData()
        }
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func updateImage(){
        switch FTDataManager.languageID {
        case "1":
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "english").withRenderingMode(.alwaysOriginal), style:.plain, target: self, action: #selector(changeLanguage))
            self.getData()
            self.cvPlaces.reloadData()
        case "2":
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "chinese").withRenderingMode(.alwaysOriginal), style:.plain, target: self, action: #selector(changeLanguage))
            self.getData()
            self.cvPlaces.reloadData()
        case "3":
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "japanese").withRenderingMode(.alwaysOriginal), style:.plain, target: self, action: #selector(changeLanguage))
            self.getData()
            self.cvPlaces.reloadData()
        default:
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "english").withRenderingMode(.alwaysOriginal), style:.plain, target: self, action: #selector(changeLanguage))
        }
    }
    @IBAction func changeLanguage() {
        let enAction = UIAlertAction(title: FTDataManager.getTranslationByLanguage(phrase: "English", langID: FTDataManager.languageID),
                                     style: .default) { (action) in
                                        FTDataManager.saveLanguage(langID: "1")
                                        self.updateImage()
                                        
        }
        let zhAction = UIAlertAction(title: FTDataManager.getTranslationByLanguage(phrase: "Chinese", langID: FTDataManager.languageID),
                                     style: .default) { (action) in
                                        FTDataManager.saveLanguage(langID: "2")
                                        self.updateImage()
        }
        let jpAction = UIAlertAction(title: FTDataManager.getTranslationByLanguage(phrase: "Japanese", langID: FTDataManager.languageID),
                                     style: .default) { (action) in
                                        FTDataManager.saveLanguage(langID: "3")
                                        self.updateImage()
                                        
                                        
        }
        let cancelAction = UIAlertAction(title: FTDataManager.getTBL(phrase: "Cancel"),
                                         style: .cancel) { (action) in
                                            // Respond to user selection of the action
        }
        
        var alert = UIAlertController(title: FTDataManager.getTBL(phrase: "SelectLanguage" ),
                                      message: "",
                                      preferredStyle: .actionSheet)
        if(UIDevice.current.userInterfaceIdiom == .pad){
            alert = UIAlertController(title: FTDataManager.getTBL(phrase: "SelectLanguage"),
                                      message: "",
                                      preferredStyle:  .alert)
        }
        
        alert.addAction(enAction)
        alert.addAction(zhAction)
        alert.addAction(jpAction)
        
        alert.addAction(cancelAction)
        
        // On iPad, action sheets must be presented from a popover.
        //alert.popoverPresentationController?.barButtonItem = self.btnLanguage
        
        self.present(alert, animated: true) {
            // The alert was presented
        }
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension PlacesViewController: UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        var name = "Main"
        
        if(UIDevice.current.userInterfaceIdiom == .pad){
            name = "Main_iPad"
        }
        
        let storyboard = UIStoryboard(name: name, bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "placesDVC") as? PlacesDetailViewController
        
        controller?.currentPlace = arrPlaces[indexPath.item]
        let backItem = UIBarButtonItem()
        backItem.title = ""
        
        navigationItem.backBarButtonItem = backItem
        self.navigationController?.pushViewController(controller!, animated: true)
    }
}
extension PlacesViewController: UICollectionViewDataSource{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrPlaces.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "placeCell", for: indexPath) as! FTCollectionViewCell
        let place = arrPlaces[indexPath.item]
        
        cell.label1.text = place.bName.uppercased()
        let url = URL(string: place.bImage)
     
        cell.imgView1.kf.setImage(with: url)
        return cell
        
    }

}

extension PlacesViewController: UICollectionViewDelegateFlowLayout{

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = cvPlaces.frame.size.width
        
        if(UIDevice.current.userInterfaceIdiom == .pad){
            return CGSize(width: (width-50)/4, height: (width-50)/4)
        }
        else{
            return CGSize(width: (width-10)/2, height: (width-10)/2)
        }
    }
}
