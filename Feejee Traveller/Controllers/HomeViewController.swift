
//  HomeViewController.swift
//  Feejee Traveller

//  Created by Vinay Prasad on 28/3/18.
//  Copyright © 2018 Vinay Prasad. All rights reserved.

import UIKit
import SwiftyJSON
import CTPanoramaView
import Alamofire
class HomeViewController: UIViewController {
    
    @IBOutlet weak var imgLoad: UIImageView?
    @IBOutlet weak var btnLanguage: UIButton?
    @IBOutlet weak var backView: UIView?
    @IBOutlet weak var panoView: CTPanoramaView!
    @IBOutlet weak var cvFeatured: UICollectionView!
    @IBOutlet weak var btnFeejee: UIButton?
    @IBOutlet weak var btnAbout: UIButton?
    @IBOutlet weak var btnBag: UIButton?
    @IBOutlet weak var btnMore: UIButton?
    @IBOutlet weak var lblWeather: UILabel?
    @IBOutlet weak var lblPop: UILabel?
    @IBOutlet weak var btnLangWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var btnLangHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var cvWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var cvHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var btnBagXConstraint: NSLayoutConstraint!
    @IBOutlet weak var btnStoreXConstraint: NSLayoutConstraint!
    @IBOutlet weak var btnBagHConstraint: NSLayoutConstraint!
    @IBOutlet weak var btnStoreHConstraint: NSLayoutConstraint!
    @IBOutlet weak var imgLogoHConstraint: NSLayoutConstraint!
    @IBOutlet weak var imgLogoWConstraint: NSLayoutConstraint!
    @IBOutlet weak var btnLangX: NSLayoutConstraint!
    @IBOutlet weak var imgLogoX: NSLayoutConstraint!
    @IBOutlet weak var lblWeatherX: NSLayoutConstraint!
     @IBOutlet weak var imgWeather: UIImageView?
    
    private let manager = NetworkReachabilityManager(host: "www.feejeetraveller.com/tba/calls_v2_ios/checkCalls.php")
    
    let network = NetworkManager.sharedInstance
    var HUD = LottieHUD("spinner")
    var arrTours : [Tour] = [Tour()]
    var arrRentals : [Rental] = [Rental()]
    
    var arrFeatured: [JSON] = [JSON]()

    var boolWeather = false

    override func viewDidLoad() {
        super.viewDidLoad()
        HUD.contentMode = .scaleAspectFill
        HUD.size = CGSize(width: 50, height: 50)
        loadPanoramaView()
        
        let timer = Timer.scheduledTimer(timeInterval: 60, target: self, selector: #selector(weatherChange(tapGestureRecognizer:)), userInfo: nil, repeats: true)
        
        timer.fire()
       
        btnMore?.isHidden = true
        btnBag?.isHidden = true
        btnAbout?.isHidden = true
    
  
        self.updateUI()
    }
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .all
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let launchedBefore = UserDefaults.standard.bool(forKey: "launchedBefore")
        self.cvFeatured.isHidden = true
    
        if(launchedBefore){
            self.updateImage()
        }
    
        if launchedBefore  {
        
            self.cvFeatured.reloadData()
            DispatchQueue.global(qos: .userInitiated).async {
                self.HUD.showHUD()
                FTDataManager.saveTranslationData()
               
                FTDataManager.saveFeaturedData()
                FTDataManager.saveCurrency2()
        
                if(FTDataManager.checkUpdates()){
                    self.arrFeatured = FTDataManager.getFeaturedData()
                }
                DispatchQueue.main.async {
                     self.updateText()
                    self.HUD.stopHUD()
                    self.updateImage()
                    
                    self.cvFeatured.reloadData()
                }
            }
        } else {
            UserDefaults.standard.set(true, forKey: "launchedBefore")
          
            DispatchQueue.global(qos: .userInitiated).async {
                self.HUD.showHUD()
                FTDataManager.saveTranslationData()
                FTDataManager.saveFeaturedData()
                FTDataManager.saveCurrency2()

                if(FTDataManager.checkUpdates()){
                    self.arrFeatured = FTDataManager.getFeaturedData()
                }
                
                DispatchQueue.main.async {

                    self.HUD.stopHUD()
                    self.updateImage()
                    
                    self.cvFeatured.reloadData()
                }
                
            }
        }
        
    }
    func updateUI(){
        let tmpH = UIScreen.main.nativeBounds.size.height

        if(UIDevice.current.userInterfaceIdiom == .pad){
            cvWidthConstraint.constant = (self.view.frame.size.width - 60) * 0.75
            cvHeightConstraint.constant = ( self.view.frame.size.width - 60 ) * 0.5
            
            if(tmpH > 2048){
                imgLogoHConstraint.constant = 188
                imgLogoWConstraint.constant = 400
                btnLangWidthConstraint.constant = 75
                btnLangHeightConstraint.constant = 75
            }
            else if (tmpH == 2048) {
                imgLogoHConstraint.constant = 141
                imgLogoWConstraint.constant = 300
                btnLangWidthConstraint.constant = 60
                btnLangHeightConstraint.constant = 60
            }
            else{
                imgLogoHConstraint.constant = 94
                imgLogoWConstraint.constant = 200
                btnLangWidthConstraint.constant = 50
                btnLangHeightConstraint.constant = 50
            }
            btnBagHConstraint.constant =  150
            btnStoreHConstraint.constant = 150
         
            
            let width = (btnStoreHConstraint.constant + btnBagHConstraint.constant) + 60
            btnBagXConstraint.constant =  (self.view.frame.size.width - width) / 2
            btnStoreXConstraint.constant = (self.view.frame.size.width - width) / 2
            self.view.layoutIfNeeded()
         
        }
        else{
            
            if(tmpH == 1136 || tmpH == 1334 || tmpH == 1920 || tmpH == 2208){
                btnLangX.constant = 33
                lblWeatherX.constant = 33
                imgLogoX.constant = 93
            }
            else if(tmpH == 2048){
                btnLangX.constant = 104
                lblWeatherX.constant = 104
                imgLogoX.constant = 164
            }
            else{
                btnLangX.constant = 104
                lblWeatherX.constant = 104
                imgLogoX.constant = 164
            }
            cvWidthConstraint.constant = self.view.frame.size.width - 40
            cvHeightConstraint.constant = (self.view.frame.size.width - 40 )
            self.view.layoutIfNeeded()
        }
        btnMore?.isHidden = false
        btnBag?.isHidden = false
        btnAbout?.isHidden = false
        btnBag?.layer.cornerRadius = 5
        btnBag?.layer.borderColor = UIColor.clear.cgColor
        btnBag?.layer.borderWidth = 1.0
        btnBag?.layer.masksToBounds = true
        
        btnMore?.layer.cornerRadius = 5
        btnMore?.layer.borderColor = UIColor.clear.cgColor
        btnMore?.layer.borderWidth = 1.0
        btnMore?.layer.masksToBounds = true
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(weatherChange(tapGestureRecognizer:)))
        lblWeather?.isUserInteractionEnabled = true
        lblWeather?.addGestureRecognizer(tapGestureRecognizer)
        lblWeather?.text = FTDataManager.getWeather(city: "Nadi")
    }
    func openSettings(){
        if let url = URL(string: UIApplicationOpenSettingsURLString){
            if #available(iOS 10.0, *){
                UIApplication.shared.open(url, completionHandler: nil)
       
            } else{
                UIApplication.shared.openURL(url)
            
            }
        }
    }
    func showOfflineAlert(){
        let openSettings = UIAlertAction(title: FTDataManager.getTranslationByLanguage(phrase: "NetSettings", langID: FTDataManager.languageID),
                                         style: .default) {   (action) in self.openSettings()
        }

        let cancelAction = UIAlertAction(title: FTDataManager.getTranslationByLanguage(phrase: "ViewOffline", langID: FTDataManager.languageID),
                                         style: .destructive) { (action) in
                                            // Respond to user selection of the action
        }
        
        let alert = UIAlertController(title: FTDataManager.getTranslationByLanguage(phrase: "NoNet", langID: FTDataManager.languageID),
                                      message: "",
                                      preferredStyle: .alert)
      
        
        alert.addAction(openSettings)
  
        
        alert.addAction(cancelAction)
        
        // On iPad, action sheets must be presented from a popover.
        //alert.popoverPresentationController?.barButtonItem = self.btnLanguage
        
        self.present(alert, animated: true) {
            // The alert was presented
        }
    }

    override func viewWillDisappear(_ animated: Bool){
        self.navigationController?.setNavigationBarHidden(false, animated: true)
        super.viewWillDisappear(true)

    }
    func isNetworkReachable() -> Bool {
        return manager?.isReachable ?? false
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: true)
        super.viewWillAppear(true)
        
        
       
        self.checkURL()

        self.updateUI()
        
    }
    func checkURL(){
        let headers = [
            "cache-control": "no-cache",
        ]
        
        let request = NSMutableURLRequest(url: NSURL(string: "http://www.feejeetraveller.com/tba/calls_v2_ios/checkCalls.php")! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
             //   print(error)
                self.showOfflineAlert()
            } else {
                let httpResponse = response as? HTTPURLResponse
               // print(httpResponse)
                
            }
        })
        
        dataTask.resume()
    }
    func getdoc(){
        guard var stringPath = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true).first else {return}
        stringPath = stringPath + "Vouchers"
        
        // New Folder is your folder name
        if !FileManager.default.fileExists(atPath: stringPath) {
            do {
                try FileManager.default.createDirectory(atPath: stringPath, withIntermediateDirectories: false, attributes: nil)
            } catch let error {
               
                return
            }
        }
        
    }
    func updateText(){
        btnBag?.setTitle(FTDataManager.getTBL(phrase: "FeejeeBag"), for: UIControlState.normal)
        btnMore?.setTitle(FTDataManager.getTBL(phrase: "FeejeeStore"), for: UIControlState.normal)
        btnAbout?.setTitle(FTDataManager.getTBL(phrase: "About"), for: UIControlState.normal)
        btnFeejee?.setTitle(FTDataManager.getTBL(phrase: "WhatIsFeejee"), for: UIControlState.normal)

        if(UIDevice.current.userInterfaceIdiom == .pad){
            btnMore?.titleLabel?.font = UIFont(name: "Avenir-Heavy", size: 23)
            btnAbout?.titleLabel?.font = UIFont(name: "Avenir-Heavy", size: 23)
            btnBag?.titleLabel?.font = UIFont(name: "Avenir-Heavy", size: 23)
            lblWeather?.font = UIFont(name: "Avenir", size: 28)
        }
    }
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    @IBAction func changeLanguage() {
        let enAction = UIAlertAction(title: FTDataManager.getTranslationByLanguage(phrase: "English", langID: FTDataManager.languageID),
                                     style: .default) { (action) in
                                        FTDataManager.saveLanguage(langID: "1")
                                        self.updateImage()
                                        
        }
        let zhAction = UIAlertAction(title: FTDataManager.getTranslationByLanguage(phrase: "Chinese", langID: FTDataManager.languageID),
                                     style: .default) { (action) in
                                        FTDataManager.saveLanguage(langID: "2")
                                        self.updateImage()
        }
        let jpAction = UIAlertAction(title: FTDataManager.getTranslationByLanguage(phrase: "Japanese", langID: FTDataManager.languageID),
                                     style: .default) { (action) in
                                        FTDataManager.saveLanguage(langID: "3")
                                        self.updateImage()
                                        
                                        
        }
        let cancelAction = UIAlertAction(title: FTDataManager.getTBL(phrase: "Cancel"),
                                         style: .cancel) { (action) in
                                            // Respond to user selection of the action
        }
        
        var alert = UIAlertController(title: FTDataManager.getTranslationByLanguage(phrase: "SelectLanguage", langID: FTDataManager.languageID),
                                      message: "",
                                      preferredStyle: .actionSheet)
        if(UIDevice.current.userInterfaceIdiom == .pad){
            alert = UIAlertController(title: FTDataManager.getTranslationByLanguage(phrase: "SelectLanguage", langID: FTDataManager.languageID),
                                      message: "",
                                      preferredStyle:  .alert)
        }
        
        alert.addAction(enAction)
        alert.addAction(zhAction)
        alert.addAction(jpAction)
        
        alert.addAction(cancelAction)
        
        self.present(alert, animated: true) {
            // The alert was presented
        }

    }
    func updateImage(){
        cvFeatured.delegate = self
        cvFeatured.dataSource = self
        cvFeatured.isHidden = true
        switch FTDataManager.languageID {
        case "1":
            self.btnLanguage?.setBackgroundImage(#imageLiteral(resourceName: "english").withRenderingMode(.alwaysOriginal), for: UIControlState.normal)
            self.updateText()
            
            arrFeatured = FTDataManager.getFeaturedData()
            
            if(arrFeatured.count > 0){
                cvFeatured.reloadData()
                cvFeatured.isHidden = false
            }
            else{
                cvFeatured.isHidden = true
                
            }
           
        case "2":
             self.btnLanguage?.setBackgroundImage(#imageLiteral(resourceName: "chinese").withRenderingMode(.alwaysOriginal), for: UIControlState.normal)
              self.updateText()
             arrFeatured = FTDataManager.getFeaturedData()
             if(arrFeatured.count > 0){
                cvFeatured.reloadData()
                 cvFeatured.isHidden = false
             }  else{
                cvFeatured.isHidden = true
                
            }
        case "3":
              self.btnLanguage?.setBackgroundImage(#imageLiteral(resourceName: "japanese").withRenderingMode(.alwaysOriginal), for: UIControlState.normal)
               self.updateText()
              arrFeatured = FTDataManager.getFeaturedData()
              if(arrFeatured.count > 0){
                cvFeatured.reloadData()
                cvFeatured.isHidden = false
              }  else{
                cvFeatured.isHidden = true
                
            }
        default:
              self.btnLanguage?.setBackgroundImage(#imageLiteral(resourceName: "english").withRenderingMode(.alwaysOriginal), for: UIControlState.normal)
               self.updateText()
              arrFeatured = FTDataManager.getFeaturedData()
              if(arrFeatured.count > 0){
                cvFeatured.reloadData()
                cvFeatured.isHidden = false
              }  else{
                cvFeatured.isHidden = true
                
            }
        }
    }
    @IBAction func btnBagTouched(_ sender: Any) {
        var storyboardName = "Main"
        if(UIDevice.current.userInterfaceIdiom == .pad){ storyboardName = "Main_iPad" }
        
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "bagVC") as? BagViewController
        
        let backItem = UIBarButtonItem()
        backItem.title = ""
        
        navigationItem.backBarButtonItem = backItem
        self.navigationController?.pushViewController(controller!, animated: true)
    }
    
    @IBAction func btnFeejeeTouched(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "aboutVC") as? AboutViewController
        controller?.about = "welcome"
        let backItem = UIBarButtonItem()
        backItem.title = ""
        
        navigationItem.backBarButtonItem = backItem
        self.navigationController?.pushViewController(controller!, animated: true)
    }
    
    @IBAction func btnMoreTouched(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "storeVC") as? StoreViewController
        
        let backItem = UIBarButtonItem()
        backItem.title = ""
        
        navigationItem.backBarButtonItem = backItem
        self.navigationController?.pushViewController(controller!, animated: true)
    }
    @IBAction func btnAboutTouched(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "aboutVC") as? AboutViewController
        
        let backItem = UIBarButtonItem()
        backItem.title = ""
          
        navigationItem.backBarButtonItem = backItem
        self.navigationController?.pushViewController(controller!, animated: true)
    }
    
    func getFeaturedNameForLangID(names: JSON, lang: String)->String{
        //
        for name in names.enumerated(){
            if(name.element.1["lang_id"].stringValue == lang){
                return name.element.1["feature_name"].stringValue
            }
        }

        return ""
    }
    private func loadPanoramaView() {
        // check for iOS version here
        
     
        let os = ProcessInfo().operatingSystemVersion
        if(os.majorVersion <= 10){
            imgLoad?.isHidden = false
            panoView.isHidden = true
            
            if(UIDevice.current.userInterfaceIdiom == .pad){
                imgLoad?.image = UIImage(named: "backview")
            }
            else{
                imgLoad?.image = UIImage(named: "backview_iphone")
            }
        }
        else{
            imgLoad?.isHidden = true
            panoView.isHidden = false
            if(UIDevice.current.userInterfaceIdiom == .pad){
                panoView.image = UIImage(named: "pano_ipad")
            }
            else{ panoView.image = UIImage(named: "pano_ipad")
                
            }
            panoView.controlMethod = .motion
        }
        

    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer){
        let tappedImage = tapGestureRecognizer.view as! UIImageView
        let indexPath = cvFeatured.indexPath(for: tappedImage.superview?.superview as! FTCollectionViewCell)
    
        let featured = arrFeatured[(indexPath?.item)!]
        let tour = FTDataManager.getTourDataByIDAndLanguage(tourID: featured["tour_rental_id"].stringValue, langID: FTDataManager.languageID)
        
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "payVC") as? PaymentViewController
        controller?.webURL =  tour.tourTripAdvisorRatings.components(separatedBy: "||")[1]
        
        let backItem = UIBarButtonItem()
        backItem.title = ""
        
        navigationItem.backBarButtonItem = backItem
        self.navigationController?.pushViewController(controller!, animated: true)
    }
    @objc func weatherChange(tapGestureRecognizer: UITapGestureRecognizer){
      
        if(boolWeather){
            lblWeather?.text = FTDataManager.getWeather(city: "Nadi")
            imgWeather?.image =  UIImage(named: "\(FTDataManager.getWeatherIcon(city: "Nadi")).png")
        }
        else{
            lblWeather?.text = FTDataManager.getWeather(city: "Suva")
            imgWeather?.image =  UIImage(named: "\(FTDataManager.getWeatherIcon(city: "Suva")).png")
        }
        boolWeather = !boolWeather

        arrFeatured = FTDataManager.getFeaturedData()
        if(arrFeatured.count > 0){
            cvFeatured.reloadData()
        }
       
    }
    
}

extension HomeViewController: UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        var name = "Main"
        
        if(UIDevice.current.userInterfaceIdiom == .pad){
            name = "Main_iPad"
        }
        let storyboard = UIStoryboard(name: name, bundle: nil)
        
        let featured = arrFeatured[indexPath.item]
        
        if(featured["homef_type_name"].stringValue == "Tour"){
            let controller = storyboard.instantiateViewController(withIdentifier: "toursDVC") as? ToursDVC
            controller?.currentTour =  FTDataManager.getTourDataByIDAndLanguage(tourID: featured["tour_rental_id"].stringValue, langID: FTDataManager.languageID)
            let backItem = UIBarButtonItem()
            backItem.title = ""
            navigationItem.backBarButtonItem = backItem
            self.navigationController?.pushViewController(controller!, animated: true)

        }
        else{
            let controller = storyboard.instantiateViewController(withIdentifier: "rentalsDVC") as? RentalsDVC
            // load rental data
            controller?.currentRental = FTDataManager.getRentalDataByIDAndLanguage(rentalID: featured["tour_rental_id"].stringValue, langID: FTDataManager.languageID)
            
            let backItem = UIBarButtonItem()
            backItem.title = ""
            navigationItem.backBarButtonItem = backItem
            self.navigationController?.pushViewController(controller!, animated: true)
        }
    }
    func addCornerRadius(sender: UIView){
        let rectShape = CAShapeLayer()
        rectShape.bounds = sender.frame
        rectShape.position = sender.center
        rectShape.path = UIBezierPath(roundedRect: sender.bounds, byRoundingCorners: [.bottomLeft , .bottomRight ], cornerRadii: CGSize(width: 4, height: 4)).cgPath
        rectShape.backgroundColor = UIColor.blue.cgColor
        //self.myView.layer.backgroundColor = UIColor.green.cgColor
        //Here I'm masking the textView's layer with rectShape layer
        sender.layer.mask = rectShape
    }
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "headerCell", for: indexPath) as! CollectionHeaderView
    
        header.titleLabel.text =  FTDataManager.getTBL(phrase: "PopularThings")
        if(UIDevice.current.hasNotch){
            header.titleLabel.font =  UIFont(name: "Avenir-Heavy", size: 22)
        }
        else if(UIScreen.main.nativeBounds.size.height > 2048){
            header.titleLabel.font =  UIFont(name: "Avenir-Heavy", size: 30)
        }
        return header
        
  
    }
}
extension HomeViewController: UICollectionViewDataSource{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrFeatured.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "tourRateCell", for: indexPath) as! FTCollectionViewCell
        
        cell.imgView1.layer.borderColor = UIColor.clear.cgColor
        cell.imgView1.layer.borderWidth = 1.0
        cell.imgView1.layer.cornerRadius = 4.0
        cell.imgView1.layer.masksToBounds = true
        cell.imgView3.clipsToBounds = true
        cell.imgView3.layer.cornerRadius = 4.0
        
        if #available(iOS 11.0, *) {
            cell.imgView3.layer.maskedCorners = [.layerMaxXMaxYCorner, .layerMinXMaxYCorner]
        } else {
            // Fallback on earlier versions
        }
        
        cell.label3.text = FTDataManager.getTBL(phrase: "Book").uppercased()

        let featured = arrFeatured[indexPath.item]
   
      
            
        if(featured["homef_type_name"].stringValue == "Tour"){
        
            var tour = FTDataManager.getTourDataByIDAndLanguage(tourID: featured["tour_rental_id"].stringValue, langID: FTDataManager.languageID)
            let names = featured["names"]
                    
            if !names.isEmpty {
                let tourName = getFeaturedNameForLangID(names: names, lang: FTDataManager.languageID)
                
                if tourName != "" {
                    tour.tourName = tourName
                }
            }
            
            if(tour.tourName.isEmpty){
                cell.label1.text = ""
                cell.label2.text = ""
                cell.label3.text = ""
                cell.imgView1.image = nil
                cell.imgView2.image = nil
                cell.imgView3.isHidden = true
            }
            else{
                cell.label1.text = String(format: "$%.0f %@", ceil(tour.tourPrice * (1-tour.tourDiscount) * FTDataManager.currencyValue), FTDataManager.currencyCode)
                
//                if(tour.tourName.contains("-")){
//                    cell.label2.text = tour.tourName.components(separatedBy: "-")[1].uppercased()
//                }
//                else{
                    cell.label2.text = tour.tourName.uppercased()
               // }
                
                let url = URL(string: tour.tourImage)
                cell.imgView2.isHidden = false
                cell.imgView1.isHidden = false
                cell.imgView3.isHidden = false
                cell.imgView1.kf.setImage(with: url,  options: nil)
                
                if(tour.tourTripAdvisorRatings != "0"){
                    let ratingURL = URL(string: tour.tourTripAdvisorRatings.components(separatedBy: "||")[0])
                    cell.imgView2.kf.setImage(with: ratingURL)
                    let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
                    cell.imgView2.isUserInteractionEnabled = true
                    cell.imgView2.addGestureRecognizer(tapGestureRecognizer)
                }
            }
        }
        else{
            var rental = FTDataManager.getRentalDataByIDAndLanguage(rentalID: featured["tour_rental_id"].stringValue, langID: FTDataManager.languageID)
            
            let names = featured["names"]
                              
              if !names.isEmpty {
                  let rentalName = getFeaturedNameForLangID(names: names, lang: FTDataManager.languageID)
                  
                  if rentalName != "" {
                      rental.rentalName = rentalName
                  }
              }
                      
            if(rental.rentalName.isEmpty){
                cell.label1.text = ""
                cell.label2.text = ""
                cell.label3.text = ""
                cell.imgView1.image = nil
                cell.imgView2.image = nil
                cell.imgView3.isHidden = true
            }
            else{
                cell.label2.text = rental.rentalName.uppercased()
                cell.label1.text = String(format: "$%.0f %@", ceil(rental.rentalPrice * (1 - rental.rentalDiscount) * FTDataManager.currencyValue), FTDataManager.currencyCode)
                let url = URL(string: rental.rentalImage)
                
                cell.imgView1.kf.setImage(with: url,  options: nil)
                cell.imgView2.isHidden = true
                cell.imgView1.isHidden = false
                cell.imgView3.isHidden = false
//                let triangle = TriangleView(frame: CGRect(x:  0, y: cell.label2.frame.origin.y - (cell.label2.frame.size.height * 1.5) , width: cell.label2.frame.size.width , height: cell.label2.frame.size.height * 2.5))
//                triangle.backgroundColor = UIColor.white
            }
        }
        
        
        if(UIDevice.current.userInterfaceIdiom == .pad){
            if(UIScreen.main.bounds.width > 1024){
                cell.label2.font =  UIFont(name: "AvenirNext-Bold", size: 16)
            }
            else{
                cell.label2.font =  UIFont(name: "AvenirNext-Bold", size: 12)
            }
            cell.label1.font =  UIFont(name: "AvenirNext-Bold", size: 28)
        }
        else{ //iPhone
            if(UIScreen.main.bounds.width < 414 ){
                cell.label2.font =  UIFont(name: "Avenir-Medium", size: 7)
            }
            else{
                cell.label2.font =  UIFont(name: "Avenir-Medium", size: 9)
            }
           // cell.label2.font =  UIFont(name: "Avenir-Medium", size: 8)
            cell.label1.font =  UIFont(name: "AvenirNext-Bold", size: 16)
        }
        
        return cell
    }
    
   
}

extension HomeViewController: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width : collectionView.frame.width, height: 70)
   
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        var width = (cvFeatured.frame.size.width - 10 ) / 3
        
        if(UIDevice.current.userInterfaceIdiom == .pad){
           width =  ((self.view.frame.size.width - 10) * 0.5) / 3
        }
        let featured = arrFeatured[indexPath.item]
        if(featured["homef_type_name"].stringValue == "Tour"){
             return CGSize(width: width , height: width * 1.2 )
            
        }
        else{
           return CGSize(width: width , height: width * 1.2)
        }
        
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        if(UIDevice.current.userInterfaceIdiom == .pad){
            let width =  ((self.view.frame.size.width - 20) * 0.5)
            let width2 =  ((self.view.frame.size.width - 20) * 0.75)
            
            return (width2 - width) / 3
        }
        else{
            return 5
        }
    }
    
    
    
}

extension UIDevice {
    var hasNotch: Bool {
        if #available(iOS 11.0, *) {
            let bottom = UIApplication.shared.keyWindow?.safeAreaInsets.bottom ?? 0
            return bottom > 0
        } else {
            // Fallback on earlier versions
            return false
        }
        
    }
}


