//
//  PopUpWebVC.swift
//  Feejee Traveller
//
//  Created by Vinay Prasad on 18/2/19.
//  Copyright © 2019 Vinay Prasad. All rights reserved.
//

import UIKit
import WebKit

import Alamofire

class PopUpWebVC: UIViewController, WKNavigationDelegate, WKUIDelegate, UIWebViewDelegate {
    
    @IBOutlet var navBar: UINavigationBar!
    var pvView: WKWebView!
    var pView: UIWebView!
    
    
    var webURL : String = ""
    var webURL2 = URL(string: "")
    var titleTag : String = ""
        var HUD = LottieHUD("spinner")
    override func viewDidLoad() {
        super.viewDidLoad()

        self.updateTitle()
        
//        HUD.contentMode = .scaleAspectFill
//        HUD.size = CGSize(width: 50, height: 50)
        
        let url = URL(string: webURL)
        
        let os = ProcessInfo().operatingSystemVersion
        
        if(os.majorVersion > 10){
            
            pvView.navigationDelegate = self
            pvView.contentMode = UIViewContentMode.scaleAspectFit
            
            

            pvView.load(URLRequest(url: url!))
          
        }
        else{
            
            pView.loadRequest(URLRequest(url: url!))
        
            
            
        }
        // Do any additional setup after loading the view.
    }
    override func loadView() {
        let os = ProcessInfo().operatingSystemVersion
        if(os.majorVersion > 10){
            super.loadView()
            let webConfiguration = WKWebViewConfiguration()
            pvView = WKWebView(frame: .zero, configuration: webConfiguration)
            pvView.frame  = CGRect(x: 0, y: 66, width: UIScreen.main.bounds.width, height:  (UIScreen.main.bounds.height - 110))
            pvView.navigationDelegate = self
            pvView.uiDelegate = self
            view.addSubview(pvView)
        }
        else{
            super.loadView()
            pView    = UIWebView()
            pView.frame  = CGRect(x: 0, y: 66, width: UIScreen.main.bounds.width, height: (UIScreen.main.bounds.height - 110))
            //      webV.loadRequest(NSURLRequest(url: NSURL(string: "https://www.apple.com")! as URL) as URLRequest)
            pView.delegate = self
            view.addSubview(pView)
        }
    }
    func updateTitle(){
        let closeButton = UIBarButtonItem(image: #imageLiteral(resourceName: "close").withRenderingMode(.alwaysOriginal), style:.plain, target: self, action: #selector(btnDismissTouched(_:)))
        
        let navItem = UINavigationItem(title: "TRIPADVISOR REVIEWS")
        let titleLabel =  UILabel()
        //titleLabel.font = KDataManager.titleFont
        
        
        titleLabel.text =  self.titleTag
        
        titleLabel.addCharacterSpacing()
        navItem.titleView = titleLabel
        navItem.leftBarButtonItem = closeButton
        
        navBar.setItems([navItem], animated: false)
    }

    @IBAction func btnDismissTouched(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
    
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
       // HUD.showHUD()
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
     //   HUD.stopHUD()
    }
    
    func webView(_ webView: WKWebView, createWebViewWith configuration: WKWebViewConfiguration, for navigationAction: WKNavigationAction, windowFeatures: WKWindowFeatures) -> WKWebView? {
        
        if(navigationAction.targetFrame == nil || !navigationAction.targetFrame!.isMainFrame){
            webView.load(navigationAction.request)
        }
        
        return nil
    }
    
    
    @IBAction func back(sender: UIBarButtonItem) {
        let os = ProcessInfo().operatingSystemVersion
        if(os.majorVersion > 10){
            if (self.pvView.canGoBack) {
                self.pvView.goBack()
            }
        }
        else{
            if (self.pView.canGoBack) {
                self.pView.goBack()
            }
        }
    }
    
    @IBAction func forward(sender: UIBarButtonItem) {
        let os = ProcessInfo().operatingSystemVersion
        if(os.majorVersion > 10){
            if (self.pvView.canGoForward) {
                self.pvView.goForward()
            }
        }
        else{
            if (self.pView.canGoForward) {
                self.pView.goForward()
            }
        }
    }
    
    @IBAction func stop(sender: UIBarButtonItem) {
        let os = ProcessInfo().operatingSystemVersion
        if(os.majorVersion > 10){
            pvView.stopLoading()
        }
        else{
            pView.stopLoading()
        }
        
        
    }
    
    @IBAction func reload(sender: UIBarButtonItem) {
        let os = ProcessInfo().operatingSystemVersion
        if(os.majorVersion > 10){
            pvView.reload()
        }
        else{
            pView.reload()
        }
        
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
