//
//  EventsViewController.swift
//  Feejee Traveller
//
//  Created by Vinay Prasad on 5/4/18.
//  Copyright © 2018 Vinay Prasad. All rights reserved.
//

import UIKit

class EventsVC: UIViewController {
    @IBOutlet weak var tvEvents: UITableView!
    @IBOutlet weak var cvEvents: UICollectionView!
    
    var arrEvents: [Event] = [Event()]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    override func viewDidAppear(_ animated: Bool) {
          self.updateImage()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getData(){
        FTDataManager.saveEvents()
        arrEvents = FTDataManager.getEventsData(langID: FTDataManager.languageID)
        
//        if(UIDevice.current.userInterfaceIdiom == .phone){
//            tvEvents.dataSource = self
//            tvEvents.reloadData()
//        }
//        else{
//            cvEvents.dataSource = self
//            cvEvents.reloadData()
//        }
        
        cvEvents.dataSource = self
        cvEvents.reloadData()
    }
    func updateImage(){
        self.title = FTDataManager.getTBL(phrase: "Events").uppercased()
        switch FTDataManager.languageID {
        case "1":
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "english").withRenderingMode(.alwaysOriginal), style:.plain, target: self, action: #selector(changeLanguage))
            self.getData()
            //self.tvOptions.reloadData()
        case "2":
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "chinese").withRenderingMode(.alwaysOriginal), style:.plain, target: self, action: #selector(changeLanguage))
            self.getData()
            //self.tvOptions.reloadData()
        case "3":
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "japanese").withRenderingMode(.alwaysOriginal), style:.plain, target: self, action: #selector(changeLanguage))
            self.getData()
            //self.tvOptions.reloadData()
        default:
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "english").withRenderingMode(.alwaysOriginal), style:.plain, target: self, action: #selector(changeLanguage))
        }
    }
    @IBAction func changeLanguage() {
        let enAction = UIAlertAction(title: FTDataManager.getTranslationByLanguage(phrase: "English", langID: FTDataManager.languageID),
                                     style: .default) { (action) in
                                        FTDataManager.saveLanguage(langID: "1")
                                        self.updateImage()
                                        
        }
        let zhAction = UIAlertAction(title: FTDataManager.getTranslationByLanguage(phrase: "Chinese", langID: FTDataManager.languageID),
                                     style: .default) { (action) in
                                        FTDataManager.saveLanguage(langID: "2")
                                        self.updateImage()
        }
        let jpAction = UIAlertAction(title: FTDataManager.getTranslationByLanguage(phrase: "Japanese", langID: FTDataManager.languageID),
                                     style: .default) { (action) in
                                        FTDataManager.saveLanguage(langID: "3")
                                        self.updateImage()
                                        
                                        
        }
        let cancelAction = UIAlertAction(title: FTDataManager.getTBL(phrase: "Cancel"),
                                         style: .cancel) { (action) in
                                            // Respond to user selection of the action
        }
        
        var alert = UIAlertController(title: FTDataManager.getTBL(phrase: "SelectLanguage"),
                                      message: "",
                                      preferredStyle: .actionSheet)
        if(UIDevice.current.userInterfaceIdiom == .pad){
            alert = UIAlertController(title: FTDataManager.getTBL(phrase: "SelectLanguage"),
                                      message: "",
                                      preferredStyle:  .alert)
        }
        
        alert.addAction(enAction)
        alert.addAction(zhAction)
        alert.addAction(jpAction)
        
        alert.addAction(cancelAction)
        
        // On iPad, action sheets must be presented from a popover.
        //alert.popoverPresentationController?.barButtonItem = self.btnLanguage
        
        self.present(alert, animated: true) {
            // The alert was presented
        }
    }
    
    
    func getFullDate(dateString: String, dateFormat: String)->String{
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd"
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = dateFormat
        
        if let date = dateFormatterGet.date(from: dateString){
            return  dateFormatterPrint.string(from: date)
        }
        else{
            return ""
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension EventsVC: UITableViewDelegate{
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 50
    }
    
}
extension EventsVC: UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return arrEvents.count
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "eventsCell") as! FTTableViewCell
        
        let event = arrEvents[indexPath.section]
        cell.label1.text = event.eventName
        
        let url = URL(string: "\(FTDataManager.homeURL)images/events/\(event.eventImage)")
        print("\(FTDataManager.homeURL)images/events/\(event.eventImage)")
        cell.imgView1.kf.setImage(with: url,  options: [.forceRefresh])
        cell.label2.text = getFullDate(dateString: event.eventStartDate, dateFormat: "MMM")
        cell.label3.text = getFullDate(dateString: event.eventStartDate, dateFormat: "dd")
        cell.label4.text = getFullDate(dateString: event.eventStartDate, dateFormat: "EEE MMM dd") + " - " + getFullDate(dateString: event.eventEndDate, dateFormat: "EEE MMM dd")
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 263
    }
    
}

extension EventsVC: UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrEvents.count
    }

    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "eventsCell", for: indexPath) as! FTCollectionViewCell
        
        let event = arrEvents[indexPath.item]
        cell.label1.text = event.eventName
        
        let url = URL(string: "\(FTDataManager.homeURL)images/events/\(event.eventImage)")
         print("\(FTDataManager.homeURL)images/events/\(event.eventImage)")
        cell.imgView1.kf.setImage(with: url,  options: [.forceRefresh])
        cell.label2.text = getFullDate(dateString: event.eventStartDate, dateFormat: "MMM")
        cell.label3.text = getFullDate(dateString: event.eventStartDate, dateFormat: "dd")
        cell.label4.text = getFullDate(dateString: event.eventStartDate, dateFormat: "EEE MMM dd") + " - " + getFullDate(dateString: event.eventEndDate, dateFormat: "EEE MMM dd")
        return cell
    }
    
    
}


