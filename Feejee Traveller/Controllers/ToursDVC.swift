//
//  ToursDVC.swift
//  Feejee Traveller
//
//  Created by Vinay Prasad on 3/4/18.
//  Copyright © 2018 Vinay Prasad. All rights reserved.
//
import UIKit
import Alamofire
import ImageSlideshow
import SwiftyJSON

class ToursDVC: UIViewController {

    @IBOutlet weak var tvDetails: UITableView!
    @IBOutlet weak var cvGallery: UICollectionView!
    @IBOutlet weak var btnBookNow: UIButton!
    var currentTour = Tour()
    var arrImages = [String]()
    var HUD = LottieHUD("spinner")
    var sections = 6
    override func viewDidLoad() {
        super.viewDidLoad()
   
        HUD.contentMode = .scaleAspectFill
        HUD.size = CGSize(width: 50, height: 50)
        tvDetails.delegate = self
        tvDetails.dataSource = self
        
        tvDetails.estimatedRowHeight = 44.0
        tvDetails.rowHeight = UITableViewAutomaticDimension
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
        
      
        self.updateImage()
        self.getGalleryImages()
        tvDetails.reloadData()
        
    
    }
    func getData(){
        HUD.showHUD()
        DispatchQueue.global(qos: .userInitiated).async {
            self.checkData()
            }
        
    }
    
    func checkData(){
        let headers = [
            "cache-control": "no-cache",
            ]
        
        let request = NSMutableURLRequest(url: NSURL(string: "http://feejeetraveller.com/tba/calls_v2_ios/tours_detail.php?id=\(currentTour.tourID)&lang=\(FTDataManager.languageID)")! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                //   print(error)
                self.currentTour = FTDataManager.getTourDataByIDAndLanguage(tourID: self.currentTour.tourID, langID: FTDataManager.languageID)
            } else
            {
                let responseData = data as? Data
                
                do{
                    let tour =  try JSON(data: responseData!)
                    
                    var allRates = [Rates]()
               
                    if(!tour["rates"].isEmpty){
                        let rates = tour["rates"].array
                        for rate in rates!{
                            if(rate["language_lang_id"].stringValue == FTDataManager.languageID){
                                allRates.append(Rates(rateID: rate["trates_id"].stringValue, ratePickUp: rate["trates_pickup"].stringValue, ratePreNote: rate["trates_prenote"].stringValue, ratePostNote: rate["trates_postnote"].stringValue, rateType: rate["trates_type"].stringValue, rateCost: rate["trates_cost"].doubleValue, rateDisplay: rate["trates_display"].stringValue, rateSpecial: rate["trates_special"].stringValue, rateLanguage: rate["trates_id"].stringValue))
                            }
                        }
                    }
                    self.currentTour = Tour(tourID: tour["tourmulti_id"].stringValue, tourName: tour["tour_name"].stringValue, tourDesc: tour["tour_description"].stringValue, tourHighlight: tour["tour_highlight"].stringValue, tourDuration: tour["tour_duration"].stringValue, tourImage: tour["tour_image"].stringValue, tourGuide: tour["tour_guide"].stringValue, tourVideo: tour["tour_video"].stringValue, tourType: tour["language_lang_id"].stringValue, tourPhone: tour["language_lang_id"].stringValue, tourEmail: tour["language_lang_id"].stringValue, tourWebsite:  tour["language_lang_id"].stringValue, tourFormURL: tour["tour_formurl"].stringValue, tourFeatureName: tour["language_lang_id"].stringValue, tourService: tour["language_lang_id"].stringValue, tourHours: tour["language_lang_id"].stringValue, tourPrice: tour["tour_price"].doubleValue, tourDiscount: tour["tour_discount"].doubleValue , tourTripAdvisorRatings: tour["tripadvisorid"].stringValue, tourAvailable: tour["language_lang_id"].stringValue, tourLanguage: tour["language_lang_id"].stringValue, tourRates: allRates)
                    
                    DispatchQueue.main.async {
                        self.title = self.currentTour.tourName.uppercased()
                        self.btnBookNow.setTitle(FTDataManager.getTBL(phrase: "BookNow").uppercased(), for: .normal)
                        self.tvDetails.reloadData()
                        self.HUD.stopHUD()
                    }
                 
                    
                }
                catch{}
            }
        })
        
        dataTask.resume()
    }
    func updateImage(){
      
        if(currentTour.tourVideo.count > 0){
            sections = 7
        }
        else{
            sections = 6
        }
        
        let share = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(shareTapped))
        switch FTDataManager.languageID {
        case "1":
            let change = UIBarButtonItem(image: #imageLiteral(resourceName: "english").withRenderingMode(.alwaysOriginal), style:.plain, target: self, action: #selector(changeLanguage))
            
            navigationItem.rightBarButtonItems = [share, change]
            self.getData()
         //   self.tvDetails.reloadData()
        case "2":
            let change = UIBarButtonItem(image: #imageLiteral(resourceName: "chinese").withRenderingMode(.alwaysOriginal), style:.plain, target: self, action: #selector(changeLanguage))
             navigationItem.rightBarButtonItems = [share, change]
            self.getData()
        //    self.tvDetails.reloadData()
        case "3":
            let change = UIBarButtonItem(image: #imageLiteral(resourceName: "japanese").withRenderingMode(.alwaysOriginal), style:.plain, target: self, action: #selector(changeLanguage))
             navigationItem.rightBarButtonItems = [share, change]
            self.getData()
       //     self.tvDetails.reloadData()
        default:
            let change = UIBarButtonItem(image: #imageLiteral(resourceName: "english").withRenderingMode(.alwaysOriginal), style:.plain, target: self, action: #selector(changeLanguage))
             navigationItem.rightBarButtonItems = [share, change]
        }
    }
    @IBAction func changeLanguage() {
        let enAction = UIAlertAction(title: FTDataManager.getTranslationByLanguage(phrase: "English", langID: FTDataManager.languageID),
                                     style: .default) { (action) in
                                        FTDataManager.saveLanguage(langID: "1")
                                        self.updateImage()
                                        
        }
        let zhAction = UIAlertAction(title: FTDataManager.getTranslationByLanguage(phrase: "Chinese", langID: FTDataManager.languageID),
                                     style: .default) { (action) in
                                        FTDataManager.saveLanguage(langID: "2")
                                        self.updateImage()
        }
        let jpAction = UIAlertAction(title: FTDataManager.getTranslationByLanguage(phrase: "Japanese", langID: FTDataManager.languageID),
                                     style: .default) { (action) in
                                        FTDataManager.saveLanguage(langID: "3")
                                        self.updateImage()
                                        
                                        
        }
        let cancelAction = UIAlertAction(title: FTDataManager.getTBL(phrase: "Cancel"),
                                         style: .cancel) { (action) in
                                            // Respond to user selection of the action
        }
        
        var alert = UIAlertController(title: FTDataManager.getTranslationByLanguage(phrase: "SelectLanguage", langID: FTDataManager.languageID),
                                      message: "",
                                      preferredStyle: .actionSheet)
        
        if(UIDevice.current.userInterfaceIdiom == .pad){
            alert = UIAlertController(title: FTDataManager.getTranslationByLanguage(phrase: "SelectLanguage", langID: FTDataManager.languageID),
                                      message: "",
                                      preferredStyle:  .alert)
        }
        
        alert.addAction(enAction)
        alert.addAction(zhAction)
        alert.addAction(jpAction)
        
        alert.addAction(cancelAction)
        
        // On iPad, action sheets must be presented from a popover.
        //alert.popoverPresentationController?.barButtonItem = self.btnLanguage
        
        self.present(alert, animated: true) {
            // The alert was presented
        }
        
      //  var url : NSURL = NSURL(fileURLWithPath: "http://maps.google.com/?ll=\(self.text1),\(self.text2)")
        
    }
    @objc func shareTapped(_ sender: Any){
        if let name = NSURL(string: "http://share.feejeetraveller.com/tour.php?id=\(currentTour.tourID)&lang_id=\(FTDataManager.languageID)") {
            let objectsToShare = [name]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            
            
            if(UIDevice.current.userInterfaceIdiom == .pad){
                activityVC.modalPresentationStyle = UIModalPresentationStyle.popover
                activityVC.popoverPresentationController?.barButtonItem = sender as! UIBarButtonItem
                self.present(activityVC, animated: true, completion: nil)
            }
            else{
                self.present(activityVC, animated: true, completion: nil)
            }
        }
        else
        {
            // show alert for not available
        }
    }
    @IBAction func btnBookNowTouched(_ sender: Any) {
        FTDataManager.logStats(id: currentTour.tourID, table: "tour", type: "book")
        if(!currentTour.tourFormURL.isEmpty){
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "payVC") as? PaymentViewController
            controller?.webURL = currentTour.tourFormURL
            
        
            let backItem = UIBarButtonItem()
            backItem.title = ""
            
            navigationItem.backBarButtonItem = backItem
            self.navigationController?.pushViewController(controller!, animated: true)
        }
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getGalleryImages(){
        do{
         
            let imagesURL = URL(string:"\(FTDataManager.homeURL)images/tours.php?id=\(currentTour.tourID)")
           // let strURL = try String(contentsOf: imagesURL!)
          
            FTDataManager.logStats(id: currentTour.tourID, table: "tour", type: "view")
            
            Alamofire.request(imagesURL!)
            .responseString(completionHandler: {
                response in
                
                self.arrImages = response.result.value!.trimmingCharacters(in: .whitespacesAndNewlines).components(separatedBy: "*")
                print(self.arrImages)
                self.arrImages.removeLast()
                
                if(self.arrImages.count > 0 && UIDevice.current.userInterfaceIdiom == .pad){
                    self.cvGallery.dataSource = self
                    //cvGallery.delegate = self
                    self.cvGallery.reloadData()
                }
                else{
                    self.tvDetails.reloadSections([0], with: .none)
                }
                
            })
           
        }
        catch{}
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension ToursDVC: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(indexPath.section == 0){
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "placesVC") as? PlacesViewController
            let backItem = UIBarButtonItem()
            backItem.title = ""
            navigationItem.backBarButtonItem = backItem
            self.navigationController?.pushViewController(controller!, animated: true)
        }
    }
}
extension ToursDVC: UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return sections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(section == 0){
            if(UIDevice.current.userInterfaceIdiom == .pad){
                return 0
            }
            else {return 1}
        }
        else if(section == 5){return currentTour.tourRates.count}
        else {return 1}
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "detailCell") as! FTTableViewCell
  
        if(indexPath.section == 0){
            cell = tableView.dequeueReusableCell(withIdentifier: "imageCell") as! FTTableViewCell
            cell.layer.cornerRadius = 0.0
            
            cell.slideShow.pageControl.currentPageIndicatorTintColor = UIColor.black
            cell.slideShow.pageControl.pageIndicatorTintColor = UIColor.lightGray
            
            var imgURL = [AlamofireSource]()
            for url in arrImages{
                imgURL.append(AlamofireSource(urlString:"\(FTDataManager.homeURL)images/tours/\(currentTour.tourID)/\(url)")!)
            }
            cell.slideShow.setImageInputs(imgURL)
            
            
            return cell
        }
        else if(indexPath.section == 1){
            cell = tableView.dequeueReusableCell(withIdentifier: "detailCell") as! FTTableViewCell
            cell.label1.text = FTDataManager.getTBL(phrase: "Description")
            cell.label2.text = currentTour.tourDesc
            
             return cell
        }
        else if(indexPath.section == 2){
            cell = tableView.dequeueReusableCell(withIdentifier: "detailCell") as! FTTableViewCell
            cell.label1.text = FTDataManager.getTBL(phrase: "Highlights")
            cell.label2.text = currentTour.tourHighlight
            
             return cell
        }
        
        else if(indexPath.section == 3){
            cell = tableView.dequeueReusableCell(withIdentifier: "detailCell") as! FTTableViewCell
            cell.label1.text =  FTDataManager.getTBL(phrase: "Guide")
            cell.label2.text = currentTour.tourGuide
            
             return cell
        }
        else if(indexPath.section == 4){
            cell = tableView.dequeueReusableCell(withIdentifier: "detailCell") as! FTTableViewCell
            cell.label1.text = FTDataManager.getTBL(phrase: "Duration")
            cell.label2.text = currentTour.tourDuration
            
            return cell
        }
        
        else if(indexPath.section == 5){
            cell = tableView.dequeueReusableCell(withIdentifier: "rateCell") as! FTTableViewCell
            
            
            let currentRate = currentTour.tourRates[indexPath.row]
            cell.paddedlabel1.sizeToFit()
            cell.paddedlabel1.text = String(format: "$%.2f %@\n%@", currentRate.rateCost * FTDataManager.currencyValue, FTDataManager.currencyCode, currentRate.ratePostNote)
            if(currentRate.ratePreNote.isEmpty){
                cell.label2.text  = currentRate.rateType
            }
            else{
                cell.label2.text  = currentRate.ratePreNote + "\n" + currentRate.rateType
            }
            
            return cell
        }
            
        else{
   
            
            
            cell = tableView.dequeueReusableCell(withIdentifier: "videoCell") as! FTTableViewCell

            cell.ytView.loadVideoID(currentTour.tourVideo)
       //     cell.ytView.webView
            cell.ytView.playerVars = [
                "playsinline": "0",
                "controls": "1",
                "modestbranding": "0",
                "showinfo": "0",
                "loop": "0",
                "rel": "0"
                ] as [String: AnyObject]
            return cell
        }
        
        
        
        
    }
    
}

extension ToursDVC: UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imageCell", for: indexPath) as! FTCollectionViewCell
        let path = arrImages[indexPath.item]
        let url = URL(string:"\(FTDataManager.homeURL)images/tours/\(currentTour.tourID)/\(path)")
        
        cell.imgView1.kf.setImage(with: url)
        
        return cell
    }
    
    
}


