//
//  Business.swift
//  Feejee Traveller
//
//  Created by Vinay Prasad on 6/4/18.
//  Copyright © 2018 Vinay Prasad. All rights reserved.
//

import UIKit

struct Business {
    var id: String
    var bName: String
    var bDesc: String
    var bImage: String
    var bType: String
    var bVideo: String
    var bPhone: String
    var bEmail: String
    var bWebsite: String
    var bFacebook: String
    var bTwitter: String
    var bFeature: String
    var bFeatureName: String
    var bService: String
    var bHours: String
    var bAvailable: String
    var bListingType: String
    var bBranch: [Branch]
    var bVoucher: Voucher
}

extension Business{
    init(){
        self.id =  ""
        self.bName = ""
        self.bDesc = ""
        self.bImage = ""
        self.bType = ""
        self.bVideo = ""
        self.bPhone = ""
        self.bEmail = ""
        self.bWebsite = ""
        self.bFacebook = ""
        self.bTwitter = ""
        self.bFeature = ""
        self.bFeatureName = ""
        self.bService = ""
        self.bHours = ""
        self.bAvailable = ""
        self.bListingType = ""
        self.bBranch = [Branch]()
        self.bVoucher = Voucher.init()
    }
}
