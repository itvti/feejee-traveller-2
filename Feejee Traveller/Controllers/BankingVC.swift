//
//  BankingVC.swift
//  Feejee Traveller
//
//  Created by Vinay Prasad on 5/4/18.
//  Copyright © 2018 Vinay Prasad. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class BankingVC: UIViewController, UISearchBarDelegate, CLLocationManagerDelegate {
    @IBOutlet weak var tvBanks: UITableView!
    @IBOutlet weak var sbSearch: UISearchBar!
    @IBOutlet weak var mkMap : MKMapView!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var btnMenu: UIButton!
    @IBOutlet weak var btnConverter: UIButton!
    @IBOutlet weak var segView: UISegmentedControl!
    var arrPlaces : [Business] = [Business()]
    var arrBranches : [Branch] = [Branch()]
    var placeTypeID : String = ""
    var locationManager = CLLocationManager()
      var HUD = LottieHUD("spinner")
    override func viewDidLoad() {
        super.viewDidLoad()
        
        HUD.contentMode = .scaleAspectFill
        HUD.size = CGSize(width: 50, height: 50)
        
        segView.backgroundColor = UIColor(white: 1.0, alpha: 0.42)
        segView.setTitleTextAttributes([
            
            NSAttributedStringKey.foregroundColor: UIColor.white
            ], for: .selected)
        
        
    }
    override func viewDidAppear(_ animated: Bool) {
        HUD.showHUD()
        updateImage()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        HUD.stopHUD()
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {

    }
    @IBAction func btnMenuTouched(_ sender: Any) {
        if(tvBanks.isHidden){
            tvBanks.isHidden = false
        }
        else{
            tvBanks.isHidden = true
        }
    }
    @IBAction func btnConverterTouched(_ sender: Any) {
        // open converter view here
        
    }
    func updateImage(){
        self.title = FTDataManager.getTBL(phrase: "Banking").uppercased()
       // self.sbSearch.placeholder = FTDataManager.getTBL(phrase: "SearchBanking")
        switch FTDataManager.languageID {
        case "1":
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "english").withRenderingMode(.alwaysOriginal), style:.plain, target: self, action: #selector(changeLanguage))
            self.getData()
         //   self.tvBanks.reloadData()
        case "2":
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "chinese").withRenderingMode(.alwaysOriginal), style:.plain, target: self, action: #selector(changeLanguage))
            self.getData()
         //   self.tvBanks.reloadData()
        case "3":
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "japanese").withRenderingMode(.alwaysOriginal), style:.plain, target: self, action: #selector(changeLanguage))
            self.getData()
          //  self.tvBanks.reloadData()
        default:
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "english").withRenderingMode(.alwaysOriginal), style:.plain, target: self, action: #selector(changeLanguage))
        }
    }
    @IBAction func changeLanguage() {
        let enAction = UIAlertAction(title: FTDataManager.getTranslationByLanguage(phrase: "English", langID: FTDataManager.languageID),
                                     style: .default) { (action) in
                                        FTDataManager.saveLanguage(langID: "1")
                                        self.updateImage()
                                        
        }
        let zhAction = UIAlertAction(title: FTDataManager.getTranslationByLanguage(phrase: "Chinese", langID: FTDataManager.languageID),
                                     style: .default) { (action) in
                                        FTDataManager.saveLanguage(langID: "2")
                                        self.updateImage()
        }
        let jpAction = UIAlertAction(title: FTDataManager.getTranslationByLanguage(phrase: "Japanese", langID: FTDataManager.languageID),
                                     style: .default) { (action) in
                                        FTDataManager.saveLanguage(langID: "3")
                                        self.updateImage()
                                        
                                        
        }
        let cancelAction = UIAlertAction(title: FTDataManager.getTBL(phrase: "Cancel"),
                                         style: .cancel) { (action) in
                                            // Respond to user selection of the action
        }
        
        var alert = UIAlertController(title: FTDataManager.getTBL(phrase: "SelectLanguage"),
                                      message: "",
                                      preferredStyle: .actionSheet)
        if(UIDevice.current.userInterfaceIdiom == .pad){
            alert = UIAlertController(title: FTDataManager.getTBL(phrase: "SelectLanguage"),
                                      message: "",
                                      preferredStyle:  .alert)
        }
        
        alert.addAction(enAction)
        alert.addAction(zhAction)
        alert.addAction(jpAction)
        
        alert.addAction(cancelAction)
        
        // On iPad, action sheets must be presented from a popover.
        //alert.popoverPresentationController?.barButtonItem = self.btnLanguage
        
        self.present(alert, animated: true) {
            // The alert was presented
        }
    }
    @IBAction func myLocationButtonTapped(_ sender: Any) {
        
        mkMap.delegate = self
        mkMap.showsUserLocation = true
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.delegate = self
        
        //Check for Location Services
        if (CLLocationManager.locationServicesEnabled()) {
            locationManager = CLLocationManager()
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.requestAlwaysAuthorization()
            locationManager.requestWhenInUseAuthorization()
        }
        locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.startUpdatingLocation()
        }
        mkMap.setUserTrackingMode(.follow, animated: true)
        
    }
    @IBAction func segViewChanged(_ sender: Any) {
        switch (segView.selectedSegmentIndex) {
        case 0:
            mkMap.mapType = .standard
        case 1:
            mkMap.mapType = .satellite
        default:
            mkMap.mapType = .hybrid
        }
    }
    func getData(){
        arrPlaces = FTDataManager.getPlacesDataByLanguageAndType(langID: FTDataManager.languageID, typeID: placeTypeID)
        
        for place in arrPlaces{
            for branch in place.bBranch{
                arrBranches.append(branch)
            }
        }
        
        arrBranches.remove(at: 0)
        loadMapPins()
    }
    func loadMapPins(){
        
        // for each branch, create a pin and add to map
        for branch in arrBranches {
            let annotation = MKPointAnnotation()
            annotation.title = branch.bName + "-" + branch.bLocation
            annotation.subtitle = branch.bAddress
            annotation.coordinate = CLLocationCoordinate2D(latitude: Double(branch.bLatitude)!, longitude: Double(branch.bLongitude)!)
            mkMap.addAnnotation(annotation)
        }
        
      //   for all pins, get its coordinate and then center the map to show all pins
        var zoomRect = MKMapRectNull
        for  annotation in mkMap.annotations {
            let annotationPoint = MKMapPointForCoordinate(annotation.coordinate)
            let pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0.0, 0.0)
            if (MKMapRectIsNull(zoomRect)) {
                zoomRect = pointRect
            } else {
                zoomRect = MKMapRectUnion(zoomRect, pointRect)
            }
        }
        mkMap .setVisibleMapRect(zoomRect, animated: true)
        
        HUD.stopHUD()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}
extension BankingVC: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
           // zoom to pin here
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 10
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
}
extension BankingVC: UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       return arrBranches.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "branchCell") as! FTTableViewCell
        
        let branch = arrBranches[indexPath.row]
        //formatting
       
        cell.label1.text = branch.bName  + (branch.bPhone.isEmpty ? "" : " - " + branch.bPhone)
        cell.label2.text = branch.bAddress
        
        return cell
    }

}

extension BankingVC: MKMapViewDelegate
{
    func mapView(_ mapView: MKMapView, viewFor annotation: MKAnnotation) -> MKAnnotationView?
    {
        let annotationView = MKPinAnnotationView(annotation: annotation, reuseIdentifier: "annotationView")
        
        if(annotation .isKind(of: MKUserLocation.self)){
          return nil
        }
        annotationView.canShowCallout = true
        let button = UIButton(frame: CGRect(x: 0, y: 0, width: 100, height: 50))

        button.setTitle(FTDataManager.getTBL(phrase: "Directions"), for: .normal)
        button.titleLabel?.font =  UIFont(name: "Avenir", size: 13)
        button.setTitleColor(UIColor.blue, for: .normal)

        annotationView.rightCalloutAccessoryView = button
        
        return annotationView
    }
    
    func mapView(_ mapView: MKMapView, annotationView view: MKAnnotationView, calloutAccessoryControlTapped control: UIControl){
        guard let annotation = view.annotation else
        {
            return
        }
        // TODO add google maps
        let urlString = "http://maps.apple.com/?sll=\(annotation.coordinate.latitude),\(annotation.coordinate.longitude)"
        guard let url = URL(string: urlString) else
        {
            return
        }
        
        UIApplication.shared.openURL(url)
    }
}



