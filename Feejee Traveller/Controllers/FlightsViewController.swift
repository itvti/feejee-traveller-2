//
//  FlightsViewController.swift
//  Feejee Traveller
//
//  Created by Vinay Prasad on 3/4/18.
//  Copyright © 2018 Vinay Prasad. All rights reserved.
//

import UIKit
import SwiftyJSON
import WebKit
import LinearProgressBarMaterial

class FlightsViewController: UIViewController, WKNavigationDelegate {
    
    @IBOutlet  var tvContacts : UITableView?
    @IBOutlet var cvBookings : UICollectionView?
    @IBOutlet  var cvFlights : UICollectionView?
    @IBOutlet  var cvContacts: UICollectionView?
    @IBOutlet  var segFlightPlace : UISegmentedControl?
    @IBOutlet  var segFlightType : UISegmentedControl?
    @IBOutlet var segActionType: UISegmentedControl?
    var pvView: WKWebView!
    let linearBar: LinearProgressBar = LinearProgressBar()
    let apiKey = "1fb2fe7e065fc4779fe278072622b8892945f0a9"
    let userName = "EroniPuamau"
    
    var flightArrivalData : [Flights] = [] // 0 0
    var flightDepartureData : [Flights] = []  // 0 1
     var HUD = LottieHUD("spinner")
    var flightLocalArrivalData : [Flights] = [] // 1 0
    var flightLocalDepartureData : [Flights] = [] // 1 1
    
    var arrAirlines : [Airline] = [Airline]()
    var arrBookings : [Airline] = [Airline]()
    
    override func loadView() {
        super.loadView()
        let webConfiguration = WKWebViewConfiguration()
        pvView = WKWebView(frame: .zero, configuration: webConfiguration)
        pvView.frame  = CGRect(x: 0, y: 180, width: UIScreen.main.bounds.width, height:  (UIScreen.main.bounds.height - 180))
        pvView.isHidden = true
        pvView.navigationDelegate = self
        view.addSubview(pvView)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        HUD.contentMode = .scaleAspectFill
        HUD.size = CGSize(width: 50, height: 50)
        if(self.view.frame.size.width < 414.0){
            let font = UIFont.init(name: "AvenirNext-Medium", size: 11.0)
            segFlightPlace?.setTitleTextAttributes([NSAttributedStringKey.font: font],
                                                  for: .normal)
            segFlightType?.setTitleTextAttributes([NSAttributedStringKey.font: font],
                                                  for: .normal)
            segActionType?.setTitleTextAttributes([NSAttributedStringKey.font: font],
                                                  for: .normal)
        }
        segFlightPlace?.backgroundColor = UIColor(white: 1.0, alpha: 0.42)
        segFlightPlace?.setTitleTextAttributes([
            
            NSAttributedStringKey.foregroundColor: UIColor.white
            ], for: .selected)
        
        segFlightType?.backgroundColor = UIColor(white: 1.0, alpha: 0.42)
        segFlightType?.setTitleTextAttributes([
            
            NSAttributedStringKey.foregroundColor: UIColor.white
            ], for: .selected)
        
        segActionType?.backgroundColor = UIColor(white: 1.0, alpha: 0.42)
        segActionType?.setTitleTextAttributes([
            
            NSAttributedStringKey.foregroundColor: UIColor.white
            ], for: .selected)
        
        cvFlights?.dataSource = self
        cvFlights?.delegate = self
        
        updateText()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        
        
        DispatchQueue.global(qos: .userInitiated).async {
        
     
            self.getData()
            self.sortAirlineData()
            DispatchQueue.main.async {
                 self.updateImage()
                 self.showFlights()
            }
        }
        
        

    }
    func updateText(){
        self.title = FTDataManager.getTBL(phrase: "Flights").uppercased()
        segActionType?.setTitle(FTDataManager.getTBL(phrase: "Flights").uppercased(), forSegmentAt: 0)
        segActionType?.setTitle(FTDataManager.getTBL(phrase: "BookFlight").uppercased(), forSegmentAt: 1)
        segActionType?.setTitle(FTDataManager.getTBL(phrase: "AirlineContacts").uppercased(), forSegmentAt: 2)
//
        segFlightType?.setTitle(FTDataManager.getTBL(phrase: "Arrival").uppercased(), forSegmentAt: 0)
        segFlightType?.setTitle(FTDataManager.getTBL(phrase: "Departures").uppercased(), forSegmentAt: 1)

        segFlightPlace?.setTitle(FTDataManager.getTBL(phrase: "International").uppercased(), forSegmentAt: 0)
        segFlightPlace?.setTitle(FTDataManager.getTBL(phrase: "Domestic").uppercased(), forSegmentAt: 1)
    }
    func getData(){
        self.HUD.showHUD()
        self.setMaximumLimits()
        self.getFlightArrivalData()
        self.getFlightDepartureData()
        self.getFlightEnrouteData()
        self.getFlightScheduledData()
        
        var temp = flightArrivalData
        
        flightArrivalData = temp.sorted(by: {  $0.actualArrivalTime < $1.actualArrivalTime })
    }
    func showFlights(){
        
        cvFlights?.isHidden = false
        cvFlights?.reloadData()
        HUD.stopHUD()
//        cvFlights.scrollToItem(at: IndexPath(item: 0, section: 0), at: .top, animated: true)
    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        HUD.showHUD()
       
    }
   
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        HUD.stopHUD()
     
    }
    func updateImage(){
        self.updateText()
        switch FTDataManager.languageID {
        case "1":
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "english").withRenderingMode(.alwaysOriginal), style:.plain, target: self, action: #selector(changeLanguage))
            //self.getData()
            self.segActionTypeChanged(segActionType)
        case "2":
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "chinese").withRenderingMode(.alwaysOriginal), style:.plain, target: self, action: #selector(changeLanguage))
            // self.getData()
            self.segActionTypeChanged(segActionType)
        case "3":
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "japanese").withRenderingMode(.alwaysOriginal), style:.plain, target: self, action: #selector(changeLanguage))
            //self.getData()
            self.segActionTypeChanged(segActionType)
        default:
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "english").withRenderingMode(.alwaysOriginal), style:.plain, target: self, action: #selector(changeLanguage))
        }
    }
    @IBAction func changeLanguage() {
        let enAction = UIAlertAction(title: FTDataManager.getTranslationByLanguage(phrase: "English", langID: FTDataManager.languageID),
                                     style: .default) { (action) in
                                        FTDataManager.saveLanguage(langID: "1")
                                        self.updateImage()
                                        
        }
        let zhAction = UIAlertAction(title: FTDataManager.getTranslationByLanguage(phrase: "Chinese", langID: FTDataManager.languageID),
                                     style: .default) { (action) in
                                        FTDataManager.saveLanguage(langID: "2")
                                        self.updateImage()
        }
        let jpAction = UIAlertAction(title: FTDataManager.getTranslationByLanguage(phrase: "Japanese", langID: FTDataManager.languageID),
                                     style: .default) { (action) in
                                        FTDataManager.saveLanguage(langID: "3")
                                        self.updateImage()
                                        
                                        
        }
        let cancelAction = UIAlertAction(title: FTDataManager.getTBL(phrase: "Cancel"),
                                         style: .cancel) { (action) in
                                            // Respond to user selection of the action
        }
        
        var alert = UIAlertController(title: FTDataManager.getTBL(phrase: "SelectLanguage"),
                                      message: "",
                                      preferredStyle: .actionSheet)
        if(UIDevice.current.userInterfaceIdiom == .pad){
            alert = UIAlertController(title: FTDataManager.getTBL(phrase: "SelectLanguage"),
                                      message: "",
                                      preferredStyle:  .alert)
        }
        
        alert.addAction(enAction)
        alert.addAction(zhAction)
        alert.addAction(jpAction)
        
        alert.addAction(cancelAction)
        
        // On iPad, action sheets must be presented from a popover.
        //alert.popoverPresentationController?.barButtonItem = self.btnLanguage
        
        self.present(alert, animated: true) {
            // The alert was presented
        }
    }
    
    func sortAirlineData(){
        FTDataManager.saveAirline()
        let tempAirlines =  FTDataManager.getAirlinesData(langID: FTDataManager.languageID)
        for contact in tempAirlines{
            if(contact.airlineContact.count > 0){
                arrAirlines.append(contact)
            }
        }
        
        for booking in tempAirlines{
            if(booking.airlineWebsite != ""){
                arrBookings.append(booking)
            }
        }
        
    }
    @IBAction func segFlightTypeChanged(_ sender: Any) {
        showFlights()
    }
    @IBAction func segFlightPlaceChanged(_ sender: Any) {
        showFlights()
    }
    @IBAction func segActionTypeChanged(_ sender: Any) {
        // if 0 then flights
        if(segActionType?.selectedSegmentIndex == 0){
            
            cvBookings?.isHidden = true
            pvView.isHidden = true
            cvContacts?.isHidden = true
            
            segFlightType?.isHidden = false
            segFlightPlace?.isHidden = false
            
            showFlights()
        }
        // if 1 then booking a flight
        if(segActionType?.selectedSegmentIndex == 1){
            segFlightType?.isHidden = true
            segFlightPlace?.isHidden = true
            cvBookings?.allowsMultipleSelection = false
            cvBookings?.delegate = self
            cvBookings?.dataSource = self
            
            
            cvBookings?.isHidden = false
            pvView.navigationDelegate = self
            pvView.isHidden = false
            cvFlights?.isHidden = true
            cvContacts?.isHidden = true
            
            cvBookings?.reloadData()
        }
        // if 2 then contacts
        if(segActionType?.selectedSegmentIndex == 2){
            
            cvFlights?.isHidden = true
            cvBookings?.isHidden = true
            pvView.isHidden = true
            cvContacts?.isHidden = false
            cvContacts?.delegate = self
            cvContacts?.dataSource = self
            cvContacts?.reloadData()
            segFlightType?.isHidden = true
            segFlightPlace?.isHidden = true
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
       HUD.stopHUD()
     
    }
    func setMaximumLimits(){
        do{
            let dataurl = URL(string: "http://\(userName):\(apiKey)@flightxml.flightaware.com/json/FlightXML2/SetMaximumResultSize?max_size=100")
            let data =  try Data(contentsOf: dataurl!)
            let json =  try JSON(data: data)
            
        }catch{}
    }
    func getFlightArrivalData(){
        // international arrivals
        // get ENROUTE to Nadi
        // get ARRIVED to Nadi
        // if origin is Nausori then its domestic arrival
        
        do{
            let dataurl = URL(string: "http://\(userName):\(apiKey)@flightxml.flightaware.com/json/FlightXML2/Arrived?airport=NFFN&howMany=100&filter=airline")
            
            let data =  try Data(contentsOf: dataurl!)
            let json =  try JSON(data: data)
            
            let flights = json["ArrivedResult"]["arrivals"]
            
            for flight in flights{
                //if origin is Nausori - NFNA then add to local data
                let date = Date(timeIntervalSince1970: flight.1["actualarrivaltime"].doubleValue)
                let nowDate = Date()
                let diff = nowDate.timeIntervalSince(date) / 60
     
                if(diff <= 180){
                    if(flight.1["origin"] == "NFNA" || flight.1["origin"] == "NFNS" || flight.1["origin"] == "NFKD"){
                        flightLocalArrivalData.append(Flights.init(ident: flight.1["ident"].stringValue, type: flight.1["aircrafttype"].stringValue, arrivaltime: flight.1["actualarrivaltime"].stringValue, deptTime: flight.1["actualdeparturetime"].stringValue,  origin: flight.1["origin"].stringValue, destination: flight.1["destination"].stringValue, originName: flight.1["originName"].stringValue, originCity: flight.1["originCity"].stringValue, destName:flight.1["destinationName"].stringValue, destCity:flight.1["destinationCity"].stringValue))
                    }
                    else{
                        flightArrivalData.append(Flights.init(ident: flight.1["ident"].stringValue, type: flight.1["aircrafttype"].stringValue, arrivaltime: flight.1["actualarrivaltime"].stringValue, deptTime: flight.1["actualdeparturetime"].stringValue,  origin: flight.1["origin"].stringValue, destination: flight.1["destination"].stringValue, originName: flight.1["originName"].stringValue, originCity: flight.1["originCity"].stringValue, destName:flight.1["destinationName"].stringValue, destCity:flight.1["destinationCity"].stringValue))
                    }
                }
                //else
            }
            
        }
        catch{}
    }
    func getFlightDepartureData(){
        do{
            let dataurl = URL(string: "http://\(userName):\(apiKey)@flightxml.flightaware.com/json/FlightXML2/Departed?airport=NFFN&howMany=100&filter=airline")
            
            let data =  try Data(contentsOf: dataurl!)
            let json =  try JSON(data: data)
            
            let flights = json["DepartedResult"]["departures"]
            
            for flight in flights{
                let date = Date(timeIntervalSince1970: flight.1["estimatedarrivaltime"].doubleValue)
                let nowDate = Date()
                let diff = nowDate.timeIntervalSince(date) / 60
                    
                if(diff <= 180){
                    if(flight.1["destination"] == "NFNA" || flight.1["destination"] == "NFNS" || flight.1["destination"] == "NFKD"){
                        flightLocalDepartureData.append(Flights.init(ident: flight.1["ident"].stringValue, type: flight.1["aircrafttype"].stringValue, arrivaltime: flight.1["estimatedarrivaltime"].stringValue, deptTime: flight.1["actualdeparturetime"].stringValue,  origin: flight.1["origin"].stringValue, destination: flight.1["destination"].stringValue, originName: flight.1["originName"].stringValue, originCity: flight.1["originCity"].stringValue, destName:flight.1["destinationName"].stringValue, destCity:flight.1["destinationCity"].stringValue))}
                    else{
                        flightDepartureData.append(Flights.init(ident: flight.1["ident"].stringValue, type: flight.1["aircrafttype"].stringValue, arrivaltime: flight.1["estimatedarrivaltime"].stringValue, deptTime: flight.1["actualdeparturetime"].stringValue,  origin: flight.1["origin"].stringValue, destination: flight.1["destination"].stringValue, originName: flight.1["originName"].stringValue, originCity: flight.1["originCity"].stringValue, destName:flight.1["destinationName"].stringValue, destCity:flight.1["destinationCity"].stringValue))
                    }
                }
            }
        }
        catch{}
    }
    func getFlightEnrouteData(){
        do{
            let dataurl = URL(string: "http://\(userName):\(apiKey)@flightxml.flightaware.com/json/FlightXML2/Enroute?airport=NFFN&howMany=100&filter=airline")
            let data =  try Data(contentsOf: dataurl!)
            let json =  try JSON(data: data)
        
            let flights = json["EnrouteResult"]["enroute"]
            
            for flight in flights{
                let date = Date(timeIntervalSince1970: flight.1["estimatedarrivaltime"].doubleValue)
                let nowDate = Date()
                let diff = nowDate.timeIntervalSince(date) / 60
        
                if(diff <= 180){
                    if(flight.1["origin"] == "NFNA" || flight.1["origin"] == "NFNS" || flight.1["origin"] == "NFKD"){
                        flightLocalArrivalData.append(Flights.init(ident: flight.1["ident"].stringValue, type: flight.1["aircrafttype"].stringValue, arrivaltime: flight.1["estimatedarrivaltime"].stringValue, deptTime: flight.1["filed_departuretime"].stringValue,  origin: flight.1["origin"].stringValue, destination: flight.1["destination"].stringValue, originName: flight.1["originName"].stringValue, originCity: flight.1["originCity"].stringValue, destName:flight.1["destinationName"].stringValue, destCity:flight.1["destinationCity"].stringValue))
                    }
                    else{
                        flightArrivalData.append(Flights.init(ident: flight.1["ident"].stringValue, type: flight.1["aircrafttype"].stringValue, arrivaltime: flight.1["estimatedarrivaltime"].stringValue, deptTime: flight.1["filed_departuretime"].stringValue,  origin: flight.1["origin"].stringValue, destination: flight.1["destination"].stringValue, originName: flight.1["originName"].stringValue, originCity: flight.1["originCity"].stringValue, destName:flight.1["destinationName"].stringValue, destCity:flight.1["destinationCity"].stringValue))
                    }
                }
            }
            
        }
        catch{}
    }
    func getFlightScheduledData(){
        do{
            let dataurl = URL(string: "http://\(userName):\(apiKey)@flightxml.flightaware.com/json/FlightXML2/Scheduled?airport=NFFN&howMany=100&filter=airline")
            
            let data =  try Data(contentsOf: dataurl!)
            let json =  try JSON(data: data)
            
            let flights = json["ScheduledResult"]["scheduled"]
            
            for flight in flights{
                let date = Date(timeIntervalSince1970: flight.1["estimatedarrivaltime"].doubleValue)
                let nowDate = Date()
                let diff = nowDate.timeIntervalSince(date) / 60
                
                if(diff <= 180){
                    if(flight.1["destination"] == "NFNA" || flight.1["destination"] == "NFNS" || flight.1["destination"] == "NFKD"){
                        flightLocalDepartureData.append(Flights.init(ident: flight.1["ident"].stringValue, type: flight.1["aircrafttype"].stringValue, arrivaltime: flight.1["estimatedarrivaltime"].stringValue, deptTime: flight.1["filed_departuretime"].stringValue,  origin: flight.1["origin"].stringValue, destination: flight.1["destination"].stringValue, originName: flight.1["originName"].stringValue, originCity: flight.1["originCity"].stringValue, destName:flight.1["destinationName"].stringValue, destCity:flight.1["destinationCity"].stringValue))}
                        
                    else{
                        flightDepartureData.append(Flights.init(ident: flight.1["ident"].stringValue, type: flight.1["aircrafttype"].stringValue, arrivaltime: flight.1["estimatedarrivaltime"].stringValue, deptTime: flight.1["filed_departuretime"].stringValue,  origin: flight.1["origin"].stringValue, destination: flight.1["destination"].stringValue, originName: flight.1["originName"].stringValue, originCity: flight.1["originCity"].stringValue, destName:flight.1["destinationName"].stringValue, destCity:flight.1["destinationCity"].stringValue))
                    }
                }
            }
        }
        catch{}
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    @objc private func call(_ sender: UIButton) {
        let indexPath = cvContacts?.indexPath(for: sender.superview?.superview as! FTCollectionViewCell)
        
        let airline = arrAirlines[(indexPath?.item)!]
        let contact = airline.airlineContact[sender.tag].replacingOccurrences(of: " ", with: "")
        
        if let phoneCallURL = URL(string: "tel://\(contact)") {
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL)) {
                if #available(iOS 10.0, *) {
                    application.open(phoneCallURL, options: [:], completionHandler: nil)
                } else {
                    // Fallback on earlier versions
                }
            }
            else{
                // show cannot call error
            }
        }
    }
    func getAircraftNameForIden(id: String)->String{
        switch id {
        case "FJA":
            return "Fiji Airways"
        case "ACI":
            return "Aircalin"
        case "ANZ":
            return "Air New Zealand"
        case "FJI":
            return "Fiji Airways"
        case "JST":
            return "Jet Star"
        case "KAL":
            return "Korean Airlines"
        case "RON":
            return "Nauru Airlines"
        case "VOZ":
            return "Virgin Australia"
        case "VA":
            return "Virgin Australia"
        default:
            return id
        }
    }
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}



extension FlightsViewController: UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if(collectionView == cvBookings){
            let cell = collectionView .cellForItem(at: indexPath) as! FTCollectionViewCell
                cell.label1.backgroundColor = #colorLiteral(red: 0.1098039216, green: 0.7333333333, blue: 0.7058823529, alpha: 1)
                cell.label1.textColor = UIColor.white
            
                let airline = arrBookings[indexPath.item]
                let url: NSURL = NSURL(string: airline.airlineWebsite)!
            
                DispatchQueue.main.async {
                    self.pvView.stopLoading()
                    self.pvView.contentMode = UIViewContentMode.scaleAspectFit
                    self.pvView.load(URLRequest(url: url as URL))
                    self.HUD.stopHUD()
                  
                }
            }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        let cell = collectionView .cellForItem(at: indexPath) as! FTCollectionViewCell
        cell.label1.backgroundColor = UIColor.white
        cell.label1.textColor = UIColor.black
    }
}
extension FlightsViewController: UICollectionViewDataSource{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(collectionView == cvFlights){
            if(segFlightPlace?.selectedSegmentIndex == 0 && segFlightType?.selectedSegmentIndex == 0){ return flightArrivalData.count}
            else if(segFlightPlace?.selectedSegmentIndex == 0 && segFlightType?.selectedSegmentIndex == 1){ return flightDepartureData.count }
            else if(segFlightPlace?.selectedSegmentIndex == 1 && segFlightType?.selectedSegmentIndex == 0){ return flightLocalArrivalData.count}
            else{ return flightLocalDepartureData.count  }
        }
        else if(collectionView == cvBookings){
            return arrBookings.count
        }
        else{
            return arrAirlines.count
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if(collectionView == cvFlights){
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "flightCell", for: indexPath) as! FTCollectionViewCell
            var flightData = Flights()
            let timestamp = Date().timeIntervalSince1970
            

            if(segFlightPlace?.selectedSegmentIndex == 0 && segFlightType?.selectedSegmentIndex == 0){
                flightData = flightArrivalData[indexPath.item]

                if(Double(flightData.actualArrivalTime)! - timestamp < 0){
                     cell.label4.text = "On Time"
                }
                else{
                    cell.label4.text = "Landed"
                }
                
            }
            else if(segFlightPlace?.selectedSegmentIndex == 0 && segFlightType?.selectedSegmentIndex == 1){
                flightData = flightDepartureData[indexPath.item]
                
            }
            else if(segFlightPlace?.selectedSegmentIndex == 1 && segFlightType?.selectedSegmentIndex == 0){
                flightData = flightLocalArrivalData[indexPath.item]
                
            }
            else{
                flightData = flightLocalDepartureData[indexPath.item]
                
            }
         
            cell.label1.text = getAircraftNameForIden(id: String(flightData.ident.prefix(3)))
            if(!flightData.aircraftType.isEmpty){
                cell.label3.text = "Aircraft: " + flightData.aircraftType
            }
            cell.label2.text = flightData.ident
            //calculate status here and update color
         
    
            cell.label5.text = flightData.originCity
            cell.label6.text = flightData.destinationCity
            
            let date1 = Date(timeIntervalSince1970: Double(flightData.actualDepartureTime)!)
            let date2 = Date(timeIntervalSince1970: Double(flightData.actualArrivalTime)!)
            
            let dateFormatterPrint = DateFormatter()
            dateFormatterPrint.dateFormat = "EEE hh:mm a"
            
            cell.label7.text = dateFormatterPrint.string(from: date1)
            cell.label8.text = dateFormatterPrint.string(from: date2)
            
            cell.label9.text = FTDataManager.getTBL(phrase: "Departure")
            cell.label10.text = FTDataManager.getTBL(phrase: "Arrival")
            
            let imgName = flightData.ident.prefix(3)
            cell.imgView1.image = UIImage(named: "\(imgName).png")
            return cell
        }
        else if(collectionView == cvBookings){
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "bookingCell", for: indexPath) as! FTCollectionViewCell
            let booking = arrBookings[indexPath.item]
            cell.layer.borderColor = UIColor.clear.cgColor
            cell.layer.borderWidth = 1.0
            cell.layer.cornerRadius = 4.0
            cell.layer.masksToBounds = true
            
            cell.label1.text = booking.airlineName
            
            if(cell.isSelected){
                cell.label1.backgroundColor = #colorLiteral(red: 0.157289058, green: 0.6577236056, blue: 0.5027878881, alpha: 1)
                cell.label1.textColor = UIColor.white
            }
            else{
                cell.label1.backgroundColor = UIColor.white
                cell.label1.textColor = UIColor.black
            }
            
            return cell
        }
        else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "contactCell", for: indexPath) as! FTCollectionViewCell
            let airline = arrAirlines[indexPath.item]
            cell.imgView1.image = UIImage(named: "\(airline.airlineCode).png")
           
            cell.btn1.setTitle("\(airline.airlineContact[0]) 📞", for: .normal)
            cell.btn1.addTarget(self, action: #selector(call(_:)), for: .touchUpInside)
            if(airline.airlineContact.count > 1){
                cell.btn2.setTitle("\(airline.airlineContact[1]) 📞", for: .normal)
                cell.btn2.addTarget(self, action: #selector(call(_:)), for: .touchUpInside)
            }
            else{
                cell.btn2.setTitle("", for: .normal)
            }
            return cell
        }
    }
}

extension FlightsViewController: UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if(collectionView == cvFlights){
            if(UIDevice.current.userInterfaceIdiom == .pad){
                let width = (cvFlights!.frame.size.width - 20) / 3
                return CGSize(width:width, height: width * 0.524)
            }
            else{
                let width = (cvFlights!.frame.size.width - 20)
                return CGSize(width:width, height: width * 0.524)
            }
            
        }
        else if(collectionView == cvContacts){
            if(UIDevice.current.userInterfaceIdiom == .pad){
                let width = (cvFlights!.frame.size.width - 20) / 3
                return CGSize(width:width, height: 80)
            }
            else{
                let width = (cvFlights!.frame.size.width - 20)
                return CGSize(width:width, height: 80)
            }
            
        }
        else{
            return CGSize(width: 130, height:32)
        }
    }
}


