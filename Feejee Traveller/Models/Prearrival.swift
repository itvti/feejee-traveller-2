//
//  Prearrival.swift
//  Feejee Traveller
//
//  Created by Vinay Prasad on 9/5/18.
//  Copyright © 2018 Vinay Prasad. All rights reserved.
//

import UIKit

struct Prearrival {
    var preID: String
    var preTitle: String
    var preDescription: String
    var preLangID: String
    
    
}

extension Prearrival{
    init(){
        self.preID = ""
        self.preTitle = ""
        self.preDescription = ""
        self.preLangID = ""
    }
}
