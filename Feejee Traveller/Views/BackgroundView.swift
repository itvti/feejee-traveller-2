//
//  BackgroundView.swift
//  Feejee Traveller
//
//  Created by Vinay Prasad on 28/3/18.
//  Copyright © 2018 Vinay Prasad. All rights reserved.
//

import UIKit

class BackgroundView: UIImageView {

    
    override init(image: UIImage?) {
        super.init(image: image)
       
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
  
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func layoutSubviews() {
       // self.image = #imageLiteral(resourceName: "home")
       // self.image = #imageLiteral(resourceName: "wallpaper-2")
        if(UIDevice.current.userInterfaceIdiom == .pad){

              self.image =  #imageLiteral(resourceName: "bg_ipad")
        }
        else{
             self.image = #imageLiteral(resourceName: "bg_iphone")
        }
      
        self.alpha = 1
        self.contentMode = .scaleAspectFill
        
        let view = UIImageView(frame: CGRect(x: 0, y: 0, width: self.frame.size.width, height: self.frame.size.height))
        view.backgroundColor = UIColor(hex: "ebebebe")
        view.alpha = 0.33
        
        self.addSubview(view)
    }
    
  
}
