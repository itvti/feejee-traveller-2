//  Tour.swift
//  Feejee Traveller
//
//  Created toury Vinay Prasad on 11/4/18.
//  Copyright © 2018 Vinay Prasad. All rights reserved.


import UIKit

struct Tour {
    var tourID: String
    var tourName: String
    var tourDesc: String
    var tourHighlight: String
    var tourDuration: String
    var tourImage: String
    var tourGuide: String
    var tourVideo: String
    var tourType: String
    var tourPhone: String
    var tourEmail: String
    var tourWebsite: String
    var tourFormURL: String
    var tourFeatureName: String
    var tourService: String
    var tourHours: String
    var tourPrice: Double
    var tourDiscount: Double
    var tourTripAdvisorRatings: String
    var tourAvailable: String
    var tourLanguage: String
    var tourRates: [Rates]
}

extension Tour{
    init(){
        self.tourID =  ""
        self.tourName = ""
        self.tourDesc = ""
        self.tourHighlight = ""
        self.tourDuration = ""
        self.tourImage = ""
        self.tourGuide = ""
        self.tourVideo = ""
        self.tourType = ""
        self.tourPhone = ""
        self.tourEmail = ""
        self.tourWebsite = ""
        self.tourFormURL = ""
        self.tourFeatureName = ""
        self.tourService = ""
        self.tourHours = ""
        self.tourPrice = 0.0
        self.tourDiscount = 0.0
        self.tourTripAdvisorRatings = ""
        self.tourAvailable = ""
        self.tourLanguage = ""
        self.tourRates = [Rates]()
    }
}
