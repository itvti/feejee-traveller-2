 //
//  EmergencyVC.swift
//  Feejee Traveller
//
//  Created by Vinay Prasad on 5/4/18.
//  Copyright © 2018 Vinay Prasad. All rights reserved.
//

import UIKit
import SwiftyJSON
 
class EmergencyVC: UIViewController {
    @IBOutlet weak var tvDetails : UITableView!
    @IBOutlet weak var cvEmergency : UICollectionView!
    @IBOutlet weak var segEmergencyType : UISegmentedControl!
    
    var arrEmergency = [JSON]()
    var arrDoctor = [JSON]()
    var arrPharmacy = [JSON]()
    override func viewDidLoad() {
        super.viewDidLoad()
        segEmergencyType.isHidden = true
        cvEmergency.dataSource = self
        cvEmergency.delegate = self
       // tvDetails.tableFooterView = UIView()
        segEmergencyType.backgroundColor = UIColor(white: 1.0, alpha: 0.42)
        segEmergencyType.setTitleTextAttributes([
            
            NSAttributedStringKey.foregroundColor: UIColor.white
            ], for: .selected)
       // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        arrEmergency = FTDataManager.getEmergencyData()
        updateImage()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func updateImage(){
        self.title = FTDataManager.getTBL(phrase: "Emergency").uppercased()
        
        self.segEmergencyType.setTitle(FTDataManager.getTBL(phrase: "Emergency").uppercased(), forSegmentAt: 0)
        self.segEmergencyType.setTitle(FTDataManager.getTBL(phrase: "Doctors").uppercased(), forSegmentAt: 1)
        self.segEmergencyType.setTitle(FTDataManager.getTBL(phrase: "Pharmacy").uppercased(), forSegmentAt: 2)
        self.segEmergencyType.isHidden = false
        switch FTDataManager.languageID {
        case "1":
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "english").withRenderingMode(.alwaysOriginal), style:.plain, target: self, action: #selector(changeLanguage))
            self.cvEmergency.reloadData()
        case "2":
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "chinese").withRenderingMode(.alwaysOriginal), style:.plain, target: self, action: #selector(changeLanguage))
          
            self.cvEmergency.reloadData()
        case "3":
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "japanese").withRenderingMode(.alwaysOriginal), style:.plain, target: self, action: #selector(changeLanguage))

            self.cvEmergency.reloadData()
        default:
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "english").withRenderingMode(.alwaysOriginal), style:.plain, target: self, action: #selector(changeLanguage))
        }
    }
    @IBAction func changeLanguage() {
        let enAction = UIAlertAction(title: FTDataManager.getTranslationByLanguage(phrase: "English", langID: FTDataManager.languageID),
                                     style: .default) { (action) in
                                        FTDataManager.saveLanguage(langID: "1")
                                        self.updateImage()
                                        
        }
        let zhAction = UIAlertAction(title: FTDataManager.getTranslationByLanguage(phrase: "Chinese", langID: FTDataManager.languageID),
                                     style: .default) { (action) in
                                        FTDataManager.saveLanguage(langID: "2")
                                        self.updateImage()
        }
        let jpAction = UIAlertAction(title: FTDataManager.getTranslationByLanguage(phrase: "Japanese", langID: FTDataManager.languageID),
                                     style: .default) { (action) in
                                        FTDataManager.saveLanguage(langID: "3")
                                        self.updateImage()
                                        
                                        
        }
        let cancelAction = UIAlertAction(title: FTDataManager.getTBL(phrase: "Cancel"),
                                         style: .cancel) { (action) in
                                            // Respond to user selection of the action
        }
        
        var alert = UIAlertController(title: FTDataManager.getTBL(phrase: "SelectLanguage"),
                                      message: "",
                                      preferredStyle: .actionSheet)
        if(UIDevice.current.userInterfaceIdiom == .pad){
            alert = UIAlertController(title: FTDataManager.getTBL(phrase: "SelectLanguage"),
                                      message: "",
                                      preferredStyle:  .alert)
        }
        
        alert.addAction(enAction)
        alert.addAction(zhAction)
        alert.addAction(jpAction)
        
        alert.addAction(cancelAction)
        
        // On iPad, action sheets must be presented from a popover.
        //alert.popoverPresentationController?.barButtonItem = self.btnLanguage
        
        self.present(alert, animated: true) {
            // The alert was presented
        }
    }
    
    
    @IBAction func segEmergencyType(_ sender: Any) {
        if(segEmergencyType.selectedSegmentIndex == 0){
            arrEmergency = FTDataManager.getEmergencyData()
        }
        if(segEmergencyType.selectedSegmentIndex == 1){
            arrDoctor = FTDataManager.getDoctorData()
        }
        if(segEmergencyType.selectedSegmentIndex == 2){
            arrPharmacy = FTDataManager.getPharmacyData()
        }
        
        cvEmergency.reloadData()
        let indexPath = IndexPath(item: 0, section: 0)
        
        cvEmergency.scrollToItem(at: indexPath, at: .top, animated: true)
    }
    
//    func getEmergencyData(){
//        do {
//            let jsonURL = Bundle.main.url(forResource: "emergency", withExtension: "json", subdirectory: nil, localization: nil)
//            let data = try Data(contentsOf: jsonURL!)
//
//            let json =  try JSON(data: data)
//            arrEmergency = JSON(json["emergency"]).array!
//
//            cvEmergency.reloadData()
//
//        }
//        catch {}
//    }
//    func getDoctorData(){
//        do {
//            let jsonURL = Bundle.main.url(forResource: "doctors", withExtension: "json", subdirectory: nil, localization: nil)
//            let data = try Data(contentsOf: jsonURL!)
//
//            let json =  try JSON(data: data)
//            arrDoctor = JSON(json["doctors"]).array!
//
//            cvEmergency.reloadData()
//
//        }
//        catch {}
//    }
//    func getPharmacyData(){
//        do {
//            let jsonURL = Bundle.main.url(forResource: "pharmacies", withExtension: "json", subdirectory: nil, localization: nil)
//            let data = try Data(contentsOf: jsonURL!)
//
//            let json =  try JSON(data: data)
//            arrPharmacy = JSON(json["pharmacies"]).array!
//
//            cvEmergency.reloadData()
//
//        }
//        catch {}
//    }
    @objc private func call(_ sender: UIButton) {
        if let phoneCallURL = URL(string: "tel://\(sender.tag)") {
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL)) {
                if #available(iOS 10.0, *) {
                    application.open(phoneCallURL, options: [:], completionHandler: nil)
                } else {
                    // Fallback on earlier versions
                }
            }
        }
    }


}
 extension EmergencyVC: UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        let header = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: "headerCell", for: indexPath) as! CollectionHeaderView
       
        if(segEmergencyType.selectedSegmentIndex == 1) {
            header.titleLabel.text =  "\(arrDoctor[indexPath.section]["location"].stringValue) - \(arrDoctor[indexPath.section]["name"].stringValue)".uppercased()
        }
        else if(segEmergencyType.selectedSegmentIndex == 2){
            header.titleLabel.text =  "\(arrPharmacy[indexPath.section]["location"].stringValue) - \(arrPharmacy[indexPath.section]["name"].stringValue)".uppercased()
        }
        else{
            header.titleLabel.text =    arrEmergency[indexPath.section]["location"].stringValue.uppercased()
            
            
        }
    
        return header
        
        
    }
 }
 extension EmergencyVC: UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(segEmergencyType.selectedSegmentIndex == 0){ return 1}
        else if(segEmergencyType.selectedSegmentIndex == 1) {
            let address = arrDoctor[section]["address"].stringValue
            return address.components(separatedBy: "|").count
        }
        else if(segEmergencyType.selectedSegmentIndex == 2){
            let id = arrPharmacy[section]["id"].stringValue
            return id.components(separatedBy: "|").count
        }
        else{
            return 0
        }
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if(segEmergencyType.selectedSegmentIndex == 0){ return arrEmergency.count}
        else if(segEmergencyType.selectedSegmentIndex == 1){ return arrDoctor.count}
        else if(segEmergencyType.selectedSegmentIndex == 2){ return arrPharmacy.count}
        else{ return 0}
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if(segEmergencyType.selectedSegmentIndex == 0){
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "emergencyCell", for: indexPath) as! FTCollectionViewCell
            
            let contacts = arrEmergency[indexPath.section]["contacts"].stringValue
            cell.btn1.setTitle(contacts.components(separatedBy: "|")[0], for: .normal)
            cell.btn2.setTitle(contacts.components(separatedBy: "|")[1], for: .normal)
            cell.btn3.setTitle(contacts.components(separatedBy: "|")[2], for: .normal)
            cell.btn4.setTitle(contacts.components(separatedBy: "|")[3], for: .normal)
            cell.btn1.addTarget(self, action: #selector(call(_:)), for: .touchUpInside)
            cell.btn2.addTarget(self, action: #selector(call(_:)), for: .touchUpInside)
            cell.btn3.addTarget(self, action: #selector(call(_:)), for: .touchUpInside)
            cell.btn4.addTarget(self, action: #selector(call(_:)), for: .touchUpInside)
            
            cell.btn1.tag = Int(contacts.components(separatedBy: "|")[0])!
            cell.btn2.tag = Int(contacts.components(separatedBy: "|")[1])!
            cell.btn3.tag = Int(contacts.components(separatedBy: "|")[2])!
            cell.btn4.tag = Int(contacts.components(separatedBy: "|")[3])!
            
            cell.label1.text = FTDataManager.getTBL(phrase: "Fire")
            cell.label2.text = FTDataManager.getTBL(phrase: "Police")
            cell.label3.text = FTDataManager.getTBL(phrase: "Ambulance")
            cell.label4.text = FTDataManager.getTBL(phrase: "Hospital")
            return cell
        }
        else{
             let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "contactCell", for: indexPath) as! FTCollectionViewCell
            
            if(segEmergencyType.selectedSegmentIndex == 1){
                cell.label1.text = arrDoctor[indexPath.section]["address"].stringValue.components(separatedBy: "|")[indexPath.item]
                cell.label2.text = arrDoctor[indexPath.section]["contact_type"].stringValue.components(separatedBy: "|")[indexPath.item]
                cell.btn1.tag = Int(arrDoctor[indexPath.section]["number"].stringValue.components(separatedBy: "|")[indexPath.item])!
                cell.btn1.setTitle(arrDoctor[indexPath.section]["number"].stringValue.components(separatedBy: "|")[indexPath.item], for: .normal)
                cell.btn1.addTarget(self, action: #selector(call(_:)), for: .touchUpInside)
                return cell
            }
            else{
                    cell.label1.text = arrPharmacy[indexPath.section]["address"].stringValue.components(separatedBy: "|")[indexPath.item]
                    cell.label2.text = arrPharmacy[indexPath.section]["contact_type"].stringValue.components(separatedBy: "|")[indexPath.item]
                    cell.btn1.tag = Int(arrPharmacy[indexPath.section]["number"].stringValue.components(separatedBy: "|")[indexPath.item])!
                    cell.btn1.setTitle(arrPharmacy[indexPath.section]["number"].stringValue.components(separatedBy: "|")[indexPath.item], for: .normal)
                    cell.btn1.addTarget(self, action: #selector(call(_:)), for: .touchUpInside)
                    return cell
            }
        }
    }
    
    
 }
 
 extension EmergencyVC: UICollectionViewDelegateFlowLayout{
 
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (cvEmergency.frame.size.width - 20 )
        if(UIDevice.current.userInterfaceIdiom == .pad){
            if(segEmergencyType.selectedSegmentIndex == 0){
                
                return CGSize(width: width / 3 , height: 140 )
            }
            else{
                
                return CGSize(width: width / 3, height: 90)
            }
        }
        else{
            if(segEmergencyType.selectedSegmentIndex == 0){
                
                return CGSize(width: width  , height: width  * 0.558 )
            }
            else{
                
                return CGSize(width: width , height: 90)
            }
        }
    }
 }

