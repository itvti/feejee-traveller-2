//
//  TravelGuideVC.swift
//  Feejee Traveller
//
//  Created by Vinay Prasad on 5/4/18.
//  Copyright © 2018 Vinay Prasad. All rights reserved.
//

import UIKit
import SwiftyJSON

class TravelGuideVC: UIViewController {

    @IBOutlet weak var tvOptions: UITableView!
    @IBOutlet weak var cvOptions: UICollectionView!
    let options = ["TravellerTips", "AboutFeejee", "GettingAroundFeejee", "FeejeeVideos", "Tales", "Banking", "Emergency", "Communications"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       
        
        //  FTDataManager.getTourData()
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        if(UIDevice.current.userInterfaceIdiom == .phone){
            tvOptions.delegate = self
            tvOptions.dataSource = self
        }
        else{
            cvOptions.delegate = self
            cvOptions.dataSource = self
        }
        self.updateImage()
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func getData(){
        if(UIDevice.current.userInterfaceIdiom == .phone){
           
            tvOptions.reloadData()
        }
        else{
  
            cvOptions.reloadData()
        }
    }
    
    func updateImage(){
        self.title = FTDataManager.getTBL(phrase: "TravelGuide").uppercased()
        switch FTDataManager.languageID {
        case "1":
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "english").withRenderingMode(.alwaysOriginal), style:.plain, target: self, action: #selector(changeLanguage))
            self.getData()
           // self.tvOptions.reloadData()
        case "2":
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "chinese").withRenderingMode(.alwaysOriginal), style:.plain, target: self, action: #selector(changeLanguage))
            self.getData()
          //  self.tvOptions.reloadData()
        case "3":
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "japanese").withRenderingMode(.alwaysOriginal), style:.plain, target: self, action: #selector(changeLanguage))
            self.getData()
          //  self.tvOptions.reloadData()
        default:
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "english").withRenderingMode(.alwaysOriginal), style:.plain, target: self, action: #selector(changeLanguage))
        }
    }
    @IBAction func changeLanguage() {
        let enAction = UIAlertAction(title: FTDataManager.getTranslationByLanguage(phrase: "English", langID: FTDataManager.languageID),
                                     style: .default) { (action) in
                                        FTDataManager.saveLanguage(langID: "1")
                                        self.updateImage()
                                        
        }
        let zhAction = UIAlertAction(title: FTDataManager.getTranslationByLanguage(phrase: "Chinese", langID: FTDataManager.languageID),
                                     style: .default) { (action) in
                                        FTDataManager.saveLanguage(langID: "2")
                                        self.updateImage()
        }
        let jpAction = UIAlertAction(title: FTDataManager.getTranslationByLanguage(phrase: "Japanese", langID: FTDataManager.languageID),
                                     style: .default) { (action) in
                                        FTDataManager.saveLanguage(langID: "3")
                                        self.updateImage()
                                        
                                        
        }
        let cancelAction = UIAlertAction(title: FTDataManager.getTBL(phrase: "Cancel"),
                                         style: .cancel) { (action) in
                                            // Respond to user selection of the action
        }
        
        var alert = UIAlertController(title: FTDataManager.getTBL(phrase: "SelectLanguage"),
                                      message: "",
                                      preferredStyle: .actionSheet)
        if(UIDevice.current.userInterfaceIdiom == .pad){
            alert = UIAlertController(title: FTDataManager.getTBL(phrase: "SelectLanguage"),
                                      message: "",
                                      preferredStyle:  .alert)
        }
        
        alert.addAction(enAction)
        alert.addAction(zhAction)
        alert.addAction(jpAction)
        
        alert.addAction(cancelAction)
        
        // On iPad, action sheets must be presented from a popover.
        //alert.popoverPresentationController?.barButtonItem = self.btnLanguage
        
        self.present(alert, animated: true) {
            // The alert was presented
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
}
extension TravelGuideVC: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var storyboard = UIStoryboard(name: "Main", bundle: nil)
        var controller = storyboard.instantiateInitialViewController()
        
        if(indexPath.section == 0){
            let tipsVC = storyboard.instantiateViewController(withIdentifier: "tipsVC") as? TipsVC
            let backItem = UIBarButtonItem()
            backItem.title = ""
            navigationItem.backBarButtonItem = backItem
            self.navigationController?.pushViewController(tipsVC!, animated: true)
        }
        if(indexPath.section == 1){
            let aboutVC = storyboard.instantiateViewController(withIdentifier: "payVC") as? PaymentViewController
            if(FTDataManager.languageID == "1"){
                 aboutVC?.webURL =  "https://www.fiji.travel/"
            }
            else if(FTDataManager.languageID == "2"){
                aboutVC?.webURL =  "https://www.fiji.travel/cn"
            }
            else if(FTDataManager.languageID == "3"){
                aboutVC?.webURL =  "https://www.fiji.travel/jp"
            }
           
            let backItem = UIBarButtonItem()
            backItem.title = ""
            aboutVC?.titleTag = FTDataManager.getTBL(phrase: "AboutFeejee")
            navigationItem.backBarButtonItem = backItem
            self.navigationController?.pushViewController(aboutVC!, animated: true)
            
        }
        if(indexPath.section == 2){
            //controller = storyboard.instantiateViewController(withIdentifier: "aroundVC") as? GettingAroundVC
            let aboutVC = storyboard.instantiateViewController(withIdentifier: "payVC") as? PaymentViewController
            do{
                let dataurl = URL(string:"\(FTDataManager.homeURL)/calls_v2_ios/info.php")

                let data =  try Data(contentsOf: dataurl!)
       
                let json =  try JSON(data: data)
                let info = json.array
                
        
                let backItem = UIBarButtonItem()
                backItem.title = ""
                aboutVC?.titleTag =  FTDataManager.getTBL(phrase: "GettingAroundFeejee")
                
                var url = ""
                for i in info!{
                    if(i["language_lang_id"].stringValue == FTDataManager.languageID){
                        url = i["infor_link"].stringValue
                  
                    }
                }
              
                if(url.isEmpty){
                    aboutVC?.webURL2 =  Bundle.main.url(forResource: "GettingAroundFiji", withExtension: "pdf", subdirectory: nil, localization: nil)
                }
                else{
                    aboutVC?.webURL = url
                }
   
                navigationItem.backBarButtonItem = backItem
                self.navigationController?.pushViewController(aboutVC!, animated: true)
                
            }catch{}
            
        }
        if(indexPath.section == 3){
            let videosVC = storyboard.instantiateViewController(withIdentifier: "videosVC") as? VideosVC
            
            let backItem = UIBarButtonItem()
            backItem.title = ""
            navigationItem.backBarButtonItem = backItem
            self.navigationController?.pushViewController(videosVC!, animated: true)
            
            
            
        }
        if(indexPath.section == 4){
            let talesVC = storyboard.instantiateViewController(withIdentifier: "talesVC") as? TalesVC

            let backItem = UIBarButtonItem()
            backItem.title = ""
            navigationItem.backBarButtonItem = backItem
            self.navigationController?.pushViewController(talesVC!, animated: true)
            
            
        }
        if(indexPath.section == 5){
            
            let bankVC = storyboard.instantiateViewController(withIdentifier: "bankingVC") as? BankingVC
           
            bankVC?.placeTypeID = "1"
            
            let backItem = UIBarButtonItem()
            backItem.title = ""
            navigationItem.backBarButtonItem = backItem
            self.navigationController?.pushViewController(bankVC!, animated: true)
            
            
        }
        if(indexPath.section == 6){
            controller = storyboard.instantiateViewController(withIdentifier: "emergencyVC") as? EmergencyVC
            
            let backItem = UIBarButtonItem()
            backItem.title = ""
            navigationItem.backBarButtonItem = backItem
            self.navigationController?.pushViewController(controller!, animated: true)
            
        }
        if(indexPath.section == 7){
            let vf = FTDataManager.getPlacesDataByLanguageAndID(langID: FTDataManager.languageID, businessID: "426")[0]
            
            if(UIDevice.current.userInterfaceIdiom == .pad){
                storyboard = UIStoryboard(name: "Main_iPad", bundle: nil)
            }
        
            
            let switchVC = storyboard.instantiateViewController(withIdentifier: "switchVC") as? CommunicationsSwitchViewController
            let backItem = UIBarButtonItem()
            backItem.title = ""
            navigationItem.backBarButtonItem = backItem
            self.navigationController?.pushViewController(switchVC!, animated: true)
        }
        
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 10
    }

    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
}
extension TravelGuideVC: UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return options.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "guideCell") as! FTTableViewCell
        
        cell.label1.text =  FTDataManager.getTBL(phrase: options[indexPath.section]).uppercased()
        cell.imgView1.image = UIImage(named: options[indexPath.section])!
        return cell
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    
    
}

extension TravelGuideVC: UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return options.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier:  "guideCell", for: indexPath) as! FTCollectionViewCell

        cell.label1.text = FTDataManager.getTBL(phrase: options[indexPath.item]).uppercased()
        cell.imgView1.image = UIImage(named: options[indexPath.item])!
        return cell
        
    }
    
    
}

extension TravelGuideVC: UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        var storyboard = UIStoryboard(name: "Main", bundle: nil)
        var controller = storyboard.instantiateInitialViewController()
        
        if(indexPath.item == 0){
            let tipsVC = storyboard.instantiateViewController(withIdentifier: "tipsVC") as? TipsVC
            let backItem = UIBarButtonItem()
            backItem.title = ""
            navigationItem.backBarButtonItem = backItem
            self.navigationController?.pushViewController(tipsVC!, animated: true)
        }
        if(indexPath.item == 1){
            let aboutVC = storyboard.instantiateViewController(withIdentifier: "payVC") as? PaymentViewController
            if(FTDataManager.languageID == "1"){
                aboutVC?.webURL =  "https://www.fiji.travel/"
            }
            else if(FTDataManager.languageID == "2"){
                aboutVC?.webURL =  "https://www.fiji.travel/cn"
            }
            else if(FTDataManager.languageID == "3"){
                aboutVC?.webURL =  "https://www.fiji.travel/jp"
            }
            
            aboutVC?.titleTag = FTDataManager.getTBL(phrase: "AboutFeejee")

            let backItem = UIBarButtonItem()
            backItem.title = ""
            
            navigationItem.backBarButtonItem = backItem
            self.navigationController?.pushViewController(aboutVC!, animated: true)
            
        }
        if(indexPath.item == 2){
            let aboutVC = storyboard.instantiateViewController(withIdentifier: "payVC") as? PaymentViewController
            do{
                let dataurl = URL(string:"\(FTDataManager.homeURL)/calls_v2_ios/info.php")
                
                let data =  try Data(contentsOf: dataurl!)
                
                let json =  try JSON(data: data)
                let info = json.array
                
                
                let backItem = UIBarButtonItem()
                backItem.title = ""
                aboutVC?.titleTag = FTDataManager.getTBL(phrase: "GettingAroundFeejee")
                
                var url = ""
                for i in info!{
                    if(i["language_lang_id"].stringValue == FTDataManager.languageID){
                        url = i["infor_link"].stringValue
                        
                    }
                }
                
                if(url.isEmpty){
                    aboutVC?.webURL2 =  Bundle.main.url(forResource: "GettingAroundFiji", withExtension: "pdf", subdirectory: nil, localization: nil)
                }
                else{
                    aboutVC?.webURL = url
                }
                
                navigationItem.backBarButtonItem = backItem
                self.navigationController?.pushViewController(aboutVC!, animated: true)
                
            }catch{}
            
        }
        if(indexPath.item == 3){
            let videosVC = storyboard.instantiateViewController(withIdentifier: "videosVC") as? VideosVC
            
            let backItem = UIBarButtonItem()
            backItem.title = ""
            navigationItem.backBarButtonItem = backItem
            self.navigationController?.pushViewController(videosVC!, animated: true)
        }
        if(indexPath.item == 4){
            let talesVC = storyboard.instantiateViewController(withIdentifier: "talesVC") as? TalesVC
            
            let backItem = UIBarButtonItem()
            backItem.title = ""
            navigationItem.backBarButtonItem = backItem
            self.navigationController?.pushViewController(talesVC!, animated: true)
            
            
        }
        if(indexPath.item == 5){
            
            let bankVC = storyboard.instantiateViewController(withIdentifier: "bankingVC") as? BankingVC
            
            bankVC?.placeTypeID = "1"
            
            let backItem = UIBarButtonItem()
            backItem.title = ""
            navigationItem.backBarButtonItem = backItem
            self.navigationController?.pushViewController(bankVC!, animated: true)
            
            
        }
        if(indexPath.item == 6){
            controller = storyboard.instantiateViewController(withIdentifier: "emergencyVC") as? EmergencyVC
            
            let backItem = UIBarButtonItem()
            backItem.title = ""
            navigationItem.backBarButtonItem = backItem
            self.navigationController?.pushViewController(controller!, animated: true)
            
        }
        if(indexPath.item == 7){
            storyboard = UIStoryboard(name: "Main_iPad", bundle: nil)

            
            let switchVC = storyboard.instantiateViewController(withIdentifier: "switchVC") as? CommunicationsSwitchViewController
           // vfVC?.currentPlace = vf
            let backItem = UIBarButtonItem()
            backItem.title = ""
            navigationItem.backBarButtonItem = backItem
            self.navigationController?.pushViewController(switchVC!, animated: true)
        }
    }
}
extension TravelGuideVC: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (cvOptions.frame.size.width - 30 ) / 4
        
        
        return CGSize(width: width , height: width * 0.75)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {

        let cellHeight = ((cvOptions.frame.size.width - 30 ) / 4 ) * 0.75
        let height = cvOptions.frame.size.height - (cellHeight * 2)
        
        return UIEdgeInsets(top: height / 3, left: 0, bottom: height / 3, right: 0)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        let cellHeight = ((cvOptions.frame.size.width - 30 ) / 4 ) * 0.75
        let height = cvOptions.frame.size.height - (cellHeight * 2)
        
        
        return height / 3
    }
  
}



