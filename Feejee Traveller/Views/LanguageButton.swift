//
//  LanguageButton.swift
//  Feejee Traveller
//
//  Created by Vinay Prasad on 13/4/18.
//  Copyright © 2018 Vinay Prasad. All rights reserved.
//

import UIKit

class LanguageButton: UIButton {
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    override func layoutSubviews() {
        // self.image = #imageLiteral(resourceName: "home")
        self.imageView?.image = #imageLiteral(resourceName: "english")
   //     LanguageButton.setImage(<#T##UIButton#>)
    }
}
