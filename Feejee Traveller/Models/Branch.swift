//
//  Branch.swift
//  Feejee Traveller
//
//  Created by Vinay Prasad on 12/4/18.
//  Copyright © 2018 Vinay Prasad. All rights reserved.
//

import UIKit

struct Branch {
    var id: String
    var bName: String
    var bLocation: String
    var bAddress: String
    var bLongitude: String
    var bLatitude: String
    var bImage: String
    var bType: String
    var bVideo: String
    var bPhone: String
    var bEmail: String
    var bWebsite: String
    var bService: String
    var bHours: String
    var bAvailable: String
}

extension Branch {
    init(){
        self.id =  ""
        self.bName = ""
        self.bLocation = ""
        self.bAddress = ""
        self.bImage = ""
        self.bType = ""
        self.bVideo = ""
        self.bPhone = ""
        self.bEmail = ""
        self.bWebsite = ""
        self.bLatitude = ""
        self.bLongitude = ""
        self.bLocation = ""
        self.bService = ""
        self.bHours = ""
        self.bAvailable = ""
    }
}
