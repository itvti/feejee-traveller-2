//
//  Event.swift
//  Feejee Traveller
//
//  Created by Vinay Prasad on 24/5/18.
//  Copyright © 2018 Vinay Prasad. All rights reserved.
//

import UIKit

struct Event {
    var eventID: String
    var eventName: String
    var eventImage: String
    var eventStartDate: String
    var eventEndDate: String
    var eventLangID: String
}

extension Event{
    init(){
        self.eventID = ""
        self.eventName = ""
        self.eventImage = ""
        self.eventStartDate = ""
        self.eventEndDate = ""
        self.eventLangID = ""
    }
}
