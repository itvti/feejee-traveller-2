//
//  RentalsDVC.swift
//  Feejee Traveller
//
//  Created by Vinay Prasad on 3/4/18.
//  Copyright © 2018 Vinay Prasad. All rights reserved.
//

import UIKit
import Alamofire
import ImageSlideshow

class RentalsDVC: UIViewController {

    @IBOutlet weak var tvDetails: UITableView!
    @IBOutlet weak var cvGallery: UICollectionView!
    @IBOutlet weak var btnBookNow: UIButton!
    
    var currentRental = Rental()
    var arrImages = [String]()
    var HUD = LottieHUD("spinner")
    
    var sections = 3
    override func viewDidLoad() {
        super.viewDidLoad()
        HUD.contentMode = .scaleAspectFill
        HUD.size = CGSize(width: 50, height: 50)
        tvDetails.delegate = self
        tvDetails.dataSource = self

        tvDetails.estimatedRowHeight = 44.0
        tvDetails.rowHeight = UITableViewAutomaticDimension
        tvDetails.tableFooterView = UIView()
        
        

        HUD.showHUD()
        self.updateImage()
        self.getGalleryImages()
        
        tvDetails.reloadData()
        HUD.stopHUD()
        // Do any additional setup after loading the view.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func getGalleryImages(){
        do{
            let imagesURL = URL(string:"\(FTDataManager.homeURL)images/rentals.php?id=\(currentRental.rentalID)")
            let strURL = try String(contentsOf: imagesURL!)
            FTDataManager.logStats(id: currentRental.rentalID, table: "rental", type: "view")
            arrImages = strURL.trimmingCharacters(in: .whitespacesAndNewlines).components(separatedBy: "*")
            
            arrImages.removeLast()
            
            if(arrImages.count > 0 && UIDevice.current.userInterfaceIdiom == .pad){
                cvGallery.dataSource = self
                //cvGallery.delegate = self
                cvGallery.reloadData()
            }
        }
        catch{}
    }
    func getData(){
        currentRental = FTDataManager.getRentalDataByIDAndLanguage(rentalID: currentRental.rentalID, langID: FTDataManager.languageID)
        btnBookNow.setTitle(FTDataManager.getTBL(phrase: "BookNow").uppercased(), for: .normal)
        self.title = currentRental.rentalName.uppercased()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
       
        self.navigationController?.isNavigationBarHidden = false
    }
    func updateImage(){
        if(currentRental.rentalVideo.count > 0){
            sections = sections + 1
        }
        let share = UIBarButtonItem(barButtonSystemItem: .action, target: self, action: #selector(shareTapped))
        switch FTDataManager.languageID {
        case "1":
            let change = UIBarButtonItem(image: #imageLiteral(resourceName: "english").withRenderingMode(.alwaysOriginal), style:.plain, target: self, action: #selector(changeLanguage))
            
            navigationItem.rightBarButtonItems = [share, change]
            self.getData()
            self.tvDetails.reloadData()
        case "2":
            let change = UIBarButtonItem(image: #imageLiteral(resourceName: "chinese").withRenderingMode(.alwaysOriginal), style:.plain, target: self, action: #selector(changeLanguage))
            navigationItem.rightBarButtonItems = [share, change]
            self.getData()
            self.tvDetails.reloadData()
        case "3":
            let change = UIBarButtonItem(image: #imageLiteral(resourceName: "japanese").withRenderingMode(.alwaysOriginal), style:.plain, target: self, action: #selector(changeLanguage))
            navigationItem.rightBarButtonItems = [share, change]
            self.getData()
            self.tvDetails.reloadData()
        default:
            let change = UIBarButtonItem(image: #imageLiteral(resourceName: "english").withRenderingMode(.alwaysOriginal), style:.plain, target: self, action: #selector(changeLanguage))
            navigationItem.rightBarButtonItems = [share, change]
        }
    }
    
    @objc func shareTapped(_ sender: Any){
        if let name = NSURL(string: "http://share.feejeetraveller.com/rental.php?id=\(currentRental.rentalID)&lang_id=\(FTDataManager.languageID)") {
            let objectsToShare = [name]
            let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
            
            
            if(UIDevice.current.userInterfaceIdiom == .pad){
                activityVC.modalPresentationStyle = UIModalPresentationStyle.popover
                activityVC.popoverPresentationController?.barButtonItem = sender as? UIBarButtonItem
                self.present(activityVC, animated: true, completion: nil)
            }
            else{
                self.present(activityVC, animated: true, completion: nil)
            }
        }
        else
        {
            // show alert for not available
        }
    }
    @IBAction func changeLanguage() {
        let enAction = UIAlertAction(title: FTDataManager.getTBL(phrase: "English"),
                                     style: .default) { (action) in
                                        FTDataManager.saveLanguage(langID: "1")
                                        self.updateImage()
                                        
        }
        let zhAction = UIAlertAction(title: FTDataManager.getTBL(phrase: "Chinese"),
                                     style: .default) { (action) in
                                        FTDataManager.saveLanguage(langID: "2")
                                        self.updateImage()
        }
        let jpAction = UIAlertAction(title: FTDataManager.getTBL(phrase: "Japanese"),
                                     style: .default) { (action) in
                                        FTDataManager.saveLanguage(langID: "3")
                                        self.updateImage()
                                        
                                        
        }
        let cancelAction = UIAlertAction(title: FTDataManager.getTBL(phrase: "Cancel"),
                                         style: .cancel) { (action) in
                                            // Respond to user selection of the action
        }
        
        var alert = UIAlertController(title: FTDataManager.getTranslationByLanguage(phrase: "SelectLanguage", langID: FTDataManager.languageID),
                                      message: "",
                                      preferredStyle: .actionSheet)
        if(UIDevice.current.userInterfaceIdiom == .pad){
            alert = UIAlertController(title: FTDataManager.getTranslationByLanguage(phrase: "SelectLanguage", langID: FTDataManager.languageID),
                                      message: "",
                                      preferredStyle:  .alert)
        }
        
        alert.addAction(enAction)
        alert.addAction(zhAction)
        alert.addAction(jpAction)
        
        alert.addAction(cancelAction)
        
        // On iPad, action sheets must be presented from a popover.
        //alert.popoverPresentationController?.barButtonItem = self.btnLanguage
        
        self.present(alert, animated: true) {
            // The alert was presented
        }
    }
    
    @IBAction func btnBookNowTouched(_ sender: Any) {
        FTDataManager.logStats(id: currentRental.rentalID, table: "rental", type: "book")
        if(!currentRental.rentalFormURL.isEmpty){
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "payVC") as? PaymentViewController
            controller?.webURL = currentRental.rentalFormURL.replacingOccurrences(of: " ", with: "")
            

            let backItem = UIBarButtonItem()
            backItem.title = ""
            
            navigationItem.backBarButtonItem = backItem
            self.navigationController?.pushViewController(controller!, animated: true)
        }
    }
    

}

extension RentalsDVC: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(indexPath.section == 0){
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "placesVC") as? PlacesViewController
            let backItem = UIBarButtonItem()
            backItem.title = ""
            navigationItem.backBarButtonItem = backItem
            self.navigationController?.pushViewController(controller!, animated: true)
        }
        
    }
    
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return 10
//    }
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        let headerView = UIView()
//        headerView.backgroundColor = UIColor.clear
//        return headerView
//    }
    
}
extension RentalsDVC: UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return sections
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(section == 0){
            if(UIDevice.current.userInterfaceIdiom == .pad){
                return 0
            }
            else {return 1}
        }
        else {return 1 }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "detailCell") as! FTTableViewCell
        
        if(indexPath.section == 0){
            cell = tableView.dequeueReusableCell(withIdentifier: "imageCell") as! FTTableViewCell
            cell.layer.cornerRadius = 0.0
       
            cell.slideShow.pageControl.currentPageIndicatorTintColor = UIColor.black
            cell.slideShow.pageControl.pageIndicatorTintColor = UIColor.lightGray
            
            var imgURL = [AlamofireSource]()
            for url in arrImages{
            
                let image = url.addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed)
                    print(image!)
                imgURL.append(AlamofireSource(urlString:"\(FTDataManager.homeURL)images/rentals/\(currentRental.rentalID)/\(String(describing: image))")!)
                
            }
            
            cell.slideShow.setImageInputs(imgURL)
            
            return cell
        }
        
        else if(indexPath.section == 1){
                cell = tableView.dequeueReusableCell(withIdentifier: "detailCell") as! FTTableViewCell
                cell.label1.text = currentRental.rentalModel //FTDataManager.getTBL(phrase: "Description")
                cell.label2.text = String(format: "%@ : $%.2f %@", FTDataManager.getTBL(phrase: "DailyRate"), currentRental.rentalPrice * FTDataManager.currencyValue * (1 - currentRental.rentalDiscount), FTDataManager.currencyCode)
                cell.label2.textAlignment = .center
                cell.label1.textAlignment = .center
            
                return cell
            }
        else if(indexPath.section == 2){
                cell = tableView.dequeueReusableCell(withIdentifier: "detailCell") as! FTTableViewCell
                cell.label1.text =  FTDataManager.getTBL(phrase: "Features")
                cell.label2.text = currentRental.rentalFeatures
                
                return cell
            }

        
        else{
            
    
            cell = tableView.dequeueReusableCell(withIdentifier: "videoCell") as! FTTableViewCell
        
            cell.ytView.loadVideoID(currentRental.rentalVideo)
            
            
            return cell
        }
    }
//        func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//            if(indexPath.section == 0 &&  UIDevice.current.userInterfaceIdiom == .pad){){return 1}
//            else{ return tvDetails.estimatedRowHeight}
//        }
}

extension RentalsDVC: UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imageCell", for: indexPath) as! FTCollectionViewCell
        let path = arrImages[indexPath.item]
        let url = URL(string:"\(FTDataManager.homeURL)images/rentals/\(currentRental.rentalID)/\(path)")
        
        cell.imgView1.kf.setImage(with: url)
        
        return cell
    }
    
    
}

