//
//  Flights.swift
//  Feejee Traveller
//
//  Created by Vinay Prasad on 4/4/18.
//  Copyright © 2018 Vinay Prasad. All rights reserved.
//

import UIKit

struct Flights {
    var ident: String
    var aircraftType: String
    var actualArrivalTime: String
    var actualDepartureTime: String
    //var actualDepartureTime: String
    var origin: String
    var destination: String
    var originName: String
    var originCity: String
    var destinationName: String
    var destinationCity: String

}


extension Flights{
    init(){
        self.ident = ""
        self.aircraftType = ""
        self.actualDepartureTime = ""
        self.actualArrivalTime = ""
        self.origin = ""
        self.destination = ""
        self.originName = ""
        self.originCity = ""
        self.destinationName = ""
        self.destinationCity = ""
    }
    
    init(ident: String, type: String, arrivaltime: String, deptTime: String, origin: String, destination: String, originName : String, originCity:  String, destName: String, destCity: String){
        self.ident =  ident
        self.aircraftType = type
        self.actualArrivalTime = arrivaltime
        self.actualDepartureTime = deptTime
        self.origin = origin
        self.destination = destination
        self.originName = originName
        self.originCity = originCity
        self.destinationName = destName
        self.destinationCity = destCity
    }
}
