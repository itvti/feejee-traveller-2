//
//  Rates.swift
//  Feejee Traveller
//
//  Created by Vinay Prasad on 11/4/18.
//  Copyright © 2018 Vinay Prasad. All rights reserved.
//

import UIKit

struct Rates {
    var rateID: String
    var ratePickUp: String
    var ratePreNote: String
    var ratePostNote: String
    var rateType: String
    var rateCost: Double
    var rateDisplay: String
    var rateSpecial: String
    var rateLanguage: String
}

extension Rates{
    init(){
        self.rateID = ""
        self.ratePickUp = ""
        self.ratePreNote = ""
        self.ratePostNote = ""
        self.rateType = ""
        self.rateCost = 0.0
        self.rateDisplay = ""
        self.rateSpecial = ""
        self.rateLanguage = ""
    }
}
