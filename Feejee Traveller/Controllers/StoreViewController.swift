//
//  StoreViewController.swift
//  Feejee Traveller
//
//  Created by Vinay Prasad on 3/4/18.
//  Copyright © 2018 Vinay Prasad. All rights reserved.
//

import UIKit
import WebKit
import SwiftyJSON
import LinearProgressBarMaterial
import Lottie
import Presentr

class StoreViewController: UIViewController, WKNavigationDelegate, WKUIDelegate {
 
    @IBOutlet weak var cvOptions: UICollectionView!
    @IBOutlet weak var segStoreOption: UISegmentedControl!
    @IBOutlet weak var btnLanguage: UIBarButtonItem!
    @IBOutlet weak var cvWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var cvTripAdLogoConstraint: NSLayoutConstraint!
    @IBOutlet weak var cvTopSpacingConstraint: NSLayoutConstraint!
    @IBOutlet weak var lblTripAdv: UILabel!
    @IBOutlet weak var lblTripAdviPad: UILabel!
    @IBOutlet weak var imgTripAdviPad: UIImageView!
    @IBOutlet weak var imgTripAdv: UIImageView!
    @IBOutlet weak var backButton: UIBarButtonItem!
    @IBOutlet weak var forwardButton: UIBarButtonItem!
    @IBOutlet weak var reloadButton: UIBarButtonItem!
    @IBOutlet weak var navToolBar: UIToolbar!
    
    var wbvHotel: WKWebView!
    //var pvView: WKWebView!
    @IBOutlet weak var vwTripAdvisor: UIView!
    @IBOutlet weak var vwTripAdvisoriPad: UIView!
    @IBOutlet weak var vwTripAdvisoriPadWidth: NSLayoutConstraint!
    @IBOutlet weak var vwTripAdvisoriPadHeight: NSLayoutConstraint!
    
    var HUD = LottieHUD("spinner")
    
   // var HUD: LOTAnimationView = LOTAnimationView(name: "spinner");
    var arrTours : [Tour] = [Tour()]
    var arrRentals : [Rental] = [Rental()]
    var arrTransfers : [Transfer] = [Transfer()]
    
    override func loadView() {

        super.loadView()
        
        let webConfiguration = WKWebViewConfiguration()
        wbvHotel = WKWebView(frame: .zero, configuration: webConfiguration)
        wbvHotel.frame  = CGRect(x: 0, y: 140, width: UIScreen.main.bounds.width, height:  (UIScreen.main.bounds.height - 184))
        wbvHotel.navigationDelegate = self
        wbvHotel.uiDelegate = self
        wbvHotel.isHidden = true
        view.insertSubview(wbvHotel, belowSubview: segStoreOption)
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        updateImage()
        HUD.contentMode = .scaleAspectFill
        //HUD.
        HUD.size = CGSize(width: 50, height: 50)

        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imgTripAdvTapped))
        imgTripAdv.isUserInteractionEnabled = true
        imgTripAdv.addGestureRecognizer(tapGestureRecognizer)
        

        imgTripAdviPad.isUserInteractionEnabled = true
        imgTripAdviPad.addGestureRecognizer(tapGestureRecognizer)
        
        segStoreOption.backgroundColor = UIColor(white: 1.0, alpha: 0.88)
       
        if(self.view.frame.size.width < 414.0){
            let font = UIFont.init(name: "AvenirNext-Medium", size: 11.0)
            segStoreOption.setTitleTextAttributes([NSAttributedStringKey.font: font!],
                                                  for: .normal)
        }
       
        segStoreOption.setTitleTextAttributes([
            
            NSAttributedStringKey.foregroundColor: UIColor.white
            ], for: .selected)

        segStoreOption.setTitleTextAttributes([
    
            NSAttributedStringKey.foregroundColor: FTDataManager.appColor
        ], for: .normal)

        self.cvOptions.delegate = self
        self.cvOptions.dataSource = self
        vwTripAdvisoriPadWidth.constant = 540
        
        if(UIDevice.current.userInterfaceIdiom == .pad){
            cvTopSpacingConstraint.constant = 124
        }
        else{
            cvTopSpacingConstraint.constant = 160
            
        }
        
        self.viewWillLayoutSubviews()
        DispatchQueue.global(qos: .userInitiated).async {
            self.HUD.showHUD()
          
            FTDataManager.saveHotels()
            FTDataManager.saveTransfers()
            FTDataManager.saveShop()
            
            DispatchQueue.main.async {
                if(FTDataManager.checkUpdates()){
                          self.updateImage()
                          self.HUD.stopHUD()
                    }
            }
        
        }
        
        showTripAdvisor(state: true)
   
   
    }
    func updateViews(){
        self.title = FTDataManager.getTBL(phrase: "FeejeeStore").uppercased()
        self.segStoreOption.setTitle(FTDataManager.getTBL(phrase: "Tours").uppercased(), forSegmentAt: 0)
        self.segStoreOption.setTitle(FTDataManager.getTBL(phrase: "Rentals").uppercased(), forSegmentAt: 1)
        self.segStoreOption.setTitle(FTDataManager.getTBL(phrase: "Transfers").uppercased(), forSegmentAt: 2)
        self.segStoreOption.setTitle(FTDataManager.getTBL(phrase: "Hotels").uppercased(), forSegmentAt: 3)
        self.segStoreOption.setTitle(FTDataManager.getTBL(phrase: "Shopping").uppercased(), forSegmentAt: 4)
        
        lblTripAdv.text = FTDataManager.getTBL(phrase: "ViewReviews").uppercased()
        lblTripAdviPad.text = FTDataManager.getTBL(phrase: "ViewReviews").uppercased()
        
        cvOptions.isHidden = false
        cvOptions.reloadData()
    }
    
    func updateImage(){
        switch FTDataManager.languageID {
        case "1":
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "english").withRenderingMode(.alwaysOriginal), style:.plain, target: self, action: #selector(changeLanguage))
            self.getData()
            self.updateViews()
         
           
        case "2":
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "chinese").withRenderingMode(.alwaysOriginal), style:.plain, target: self, action: #selector(changeLanguage))
            self.getData()
            self.updateViews()
           
        case "3":
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "japanese").withRenderingMode(.alwaysOriginal), style:.plain, target: self, action: #selector(changeLanguage))
            self.getData()
            self.updateViews()
         
        default:
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "english").withRenderingMode(.alwaysOriginal), style:.plain, target: self, action: #selector(changeLanguage))
        }
    }
    @IBAction func changeLanguage() {
        let enAction = UIAlertAction(title: FTDataManager.getTranslationByLanguage(phrase: "English", langID: FTDataManager.languageID),
                                     style: .default) { (action) in
                                        FTDataManager.saveLanguage(langID: "1")
                                        self.updateImage()
                                        
        }
        let zhAction = UIAlertAction(title: FTDataManager.getTranslationByLanguage(phrase: "Chinese", langID: FTDataManager.languageID),
                                     style: .default) { (action) in
                                        FTDataManager.saveLanguage(langID: "2")
                                        self.updateImage()
        }
        let jpAction = UIAlertAction(title: FTDataManager.getTranslationByLanguage(phrase: "Japanese", langID: FTDataManager.languageID),
                                     style: .default) { (action) in
                                        FTDataManager.saveLanguage(langID: "3")
                                        self.updateImage()
                                        
                                        
        }
        let cancelAction = UIAlertAction(title: FTDataManager.getTBL(phrase: "Cancel"),
                                         style: .cancel) { (action) in
                                            // Respond to user selection of the action
        }
        
        var alert = UIAlertController(title: FTDataManager.getTranslationByLanguage(phrase: "SelectLanguage", langID: FTDataManager.languageID),
                                      message: "",
                                      preferredStyle: .actionSheet)
        if(UIDevice.current.userInterfaceIdiom == .pad){
            alert = UIAlertController(title: FTDataManager.getTranslationByLanguage(phrase: "SelectLanguage", langID: FTDataManager.languageID),
                                      message: "",
                                      preferredStyle:  .alert)
        }
        
        alert.addAction(enAction)
        alert.addAction(zhAction)
        alert.addAction(jpAction)
        alert.addAction(cancelAction)
        
        // On iPad, action sheets must be presented from a popover.
        //alert.popoverPresentationController?.barButtonItem = self.btnLanguage
        
        self.present(alert, animated: true) {
            // The alert was presented
        }
    }
   
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    
    }
    func saveData(){
        //FTDataManager.saveTourData()
      //  FTDataManager.saveRentalData()
        FTDataManager.saveTransfers()
        FTDataManager.saveHotels()
        FTDataManager.saveShop()
    }
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
       //  HUD.showHUD()
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
          //  HUD.showHUD()
       
    }
    
    func webView(_ webView: WKWebView, createWebViewWith configuration: WKWebViewConfiguration, for navigationAction: WKNavigationAction, windowFeatures: WKWindowFeatures) -> WKWebView? {

        if(navigationAction.targetFrame == nil || !navigationAction.targetFrame!.isMainFrame){
                  webView.load(navigationAction.request)
        }

        return nil
    }
//    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
//
//
//    }
    @IBAction func back(sender: UIBarButtonItem) {
        let os = ProcessInfo().operatingSystemVersion
        if(os.majorVersion > 10){
            if (self.wbvHotel.canGoBack) {
                self.wbvHotel.goBack()
            }
        }
        else{
            if (self.wbvHotel.canGoBack) {
                self.wbvHotel.goBack()
            }
        }
    }
    
    @IBAction func forward(sender: UIBarButtonItem) {
        let os = ProcessInfo().operatingSystemVersion
        if(os.majorVersion > 10){
            if (self.wbvHotel.canGoForward) {
                self.wbvHotel.goForward()
            }
        }
        else{
            if (self.wbvHotel.canGoForward) {
                self.wbvHotel.goForward()
            }
        }
    }
    
    @IBAction func stop(sender: UIBarButtonItem) {
        let os = ProcessInfo().operatingSystemVersion
        if(os.majorVersion > 10){
            wbvHotel.stopLoading()
        }
        else{
            wbvHotel.stopLoading()
        }
        
        
    }
    
    @IBAction func reload(sender: UIBarButtonItem) {
        let os = ProcessInfo().operatingSystemVersion
        if(os.majorVersion > 10){
            wbvHotel.reload()
        }
        else{
            wbvHotel.reload()
        }
        
    }
    override func viewWillDisappear(_ animated: Bool) {
            HUD.stopHUD()
       
    }
    func getData(){
        self.arrTours = FTDataManager.getTourDataByLanguage(langID: FTDataManager.languageID)
        self.arrRentals = FTDataManager.getRentalDataByLanguage(langID: FTDataManager.languageID)
        self.arrTransfers = FTDataManager.getTransfersData(langID: "1")
        
//        let temp = arrTours
//        arrTours = temp.sorted(by: {  $0.tourName < $1.tourName })
//
//        let temp2 = arrRentals
//        arrRentals = temp2.sorted(by: {  $0.rentalName < $1.rentalName })
//
//        let temp3 = arrTransfers
//        arrTransfers = temp3.sorted(by: {  $0.transferName < $1.transferName })
//
        self.saveData()
        
        
    }
    func showTripAdvisor(state: Bool){
        if(state){
            if(UIDevice.current.userInterfaceIdiom == .pad){
                vwTripAdvisor.isHidden = true
                vwTripAdvisoriPad.isHidden = false
                cvTripAdLogoConstraint.constant = 148
                vwTripAdvisoriPadHeight.constant = 148
            }
            else{
                vwTripAdvisor.isHidden = false
                vwTripAdvisoriPad.isHidden = true
                cvTripAdLogoConstraint.constant = 148
                vwTripAdvisoriPadHeight.constant = 148
            }
        }
        else{
            cvTripAdLogoConstraint.constant = 0
            vwTripAdvisoriPadHeight.constant = 0
            vwTripAdvisoriPad.isHidden = true
            vwTripAdvisor.isHidden = true
        }
        
         self.viewWillLayoutSubviews()
       
    }
    @IBAction func segStoreOptionChanged(_ sender: Any) {
        if(segStoreOption.selectedSegmentIndex == 3){
            navToolBar.isHidden = false
            wbvHotel.isHidden = false
            wbvHotel.navigationDelegate = self
            cvOptions.isHidden = true
            
            showTripAdvisor(state: false)
            self.viewWillLayoutSubviews()
            // load website
            let url: NSURL = NSURL(string: FTDataManager.getHotelData(langID: FTDataManager.languageID))!
            wbvHotel.contentMode = UIViewContentMode.scaleAspectFit
            wbvHotel.load(URLRequest(url: url as URL))
        }
        else if(segStoreOption.selectedSegmentIndex == 4){
            navToolBar.isHidden = false
            wbvHotel.isHidden = false
            wbvHotel.navigationDelegate = self
            cvOptions.isHidden = true
            
            showTripAdvisor(state: false)
            self.viewWillLayoutSubviews()
            // load website
             let url: NSURL = NSURL(string: FTDataManager.getShopData(langID: FTDataManager.languageID))!
            wbvHotel.contentMode = UIViewContentMode.scaleAspectFit
            wbvHotel.load(URLRequest(url: url as URL))
        }
        else{
            wbvHotel.isHidden = true
            navToolBar.isHidden = true
            wbvHotel.stopLoading()
            HUD.stopHUD()
            cvOptions.isHidden = false
            cvOptions.reloadData()

            showTripAdvisor(state: true)

            wbvHotel.isHidden = true
            wbvHotel.stopLoading()
        }
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer){
        let label = tapGestureRecognizer.view as! UIImageView
        var indexPath = IndexPath.init(row: 0, section: 0)
        
        indexPath = cvOptions.indexPath(for:  label.superview?.superview?.superview as! FTCollectionViewCell)!
        
        let tour = arrTours[indexPath.item]
        
        if(tour.tourTripAdvisorRatings != "0" && tour.tourTripAdvisorRatings != "" ){
            let tripAdvisorRatings =  tour.tourTripAdvisorRatings.components(separatedBy: "||")
            let url = tripAdvisorRatings[1]
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            
            let popWebVC = (storyboard.instantiateViewController(withIdentifier: "popWebVC") as? PopUpWebVC)!
            popWebVC.webURL = url
            popWebVC.titleTag = tour.tourName
            let width = ModalSize.custom(size: Float(UIScreen.main.bounds.width))
            let height = ModalSize.custom(size:  Float(UIScreen.main.bounds.height))
            let center = ModalCenterPosition.customOrigin(origin: CGPoint(x: 0, y:0))
            let customType2 = PresentationType.custom(width: width, height: height, center: center)
            
            let presenter = Presentr(presentationType: customType2)
            presenter.roundCorners = false
            presenter.backgroundColor = .black
            presenter.backgroundOpacity = 0.66
            presenter.dismissOnSwipe = false
            presenter.dismissAnimated = true
            
            customPresentViewController(presenter, viewController: popWebVC, animated: true, completion: nil)
        }
    }
    
    @objc func imgTripAdvTapped(){
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "payVC") as? PaymentViewController
        controller?.webURL =  "http://www.tripadvisor.com"
        
        let backItem = UIBarButtonItem()
        backItem.title = ""
        
        navigationItem.backBarButtonItem = backItem
        self.navigationController?.pushViewController(controller!, animated: true)
    }
    
    @objc func btnBookTransferTapped(_ sender: UIButton){
        var indexPath = IndexPath.init(row: 0, section: 0)

        indexPath = cvOptions.indexPath(for:  sender.superview?.superview as! FTCollectionViewCell)!

        let transfer = arrTransfers[(indexPath.item)]
        

        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let transfersVC = storyboard.instantiateViewController(withIdentifier: "payVC") as? PaymentViewController
//
        if(!transfer.transferLink.isEmpty){
            transfersVC?.webURL = transfer.transferLink
            FTDataManager.logStats(id: transfer.transferID, table: "transfer", type: "view")

            let backItem = UIBarButtonItem()
            backItem.title = ""

            navigationItem.backBarButtonItem = backItem
            self.navigationController?.pushViewController(transfersVC!, animated: true)
        }
//        else{
//            transfersVC?.webURL = "https://www.fijiviptransfers.com/"
//            FTDataManager.logStats(id: transfer.transferID, table: "transfer", type: "view")
//
//            let backItem = UIBarButtonItem()
//            backItem.title = ""
//
//            navigationItem.backBarButtonItem = backItem
//            self.navigationController?.pushViewController(transfersVC!, animated: true)
//
     //   }
     
    }
    @objc func btnBookRentalTapped(tapGestureRecognizer: UITapGestureRecognizer){
        let label = tapGestureRecognizer.view as! UILabel
        var indexPath = IndexPath.init(row: 0, section: 0)
        
        indexPath = cvOptions.indexPath(for:  label.superview?.superview?.superview as! FTCollectionViewCell)!

       let transfer = arrRentals[(indexPath.item)]
//        if(!transfer.rentalFormURL.isEmpty){
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            let transfersVC = storyboard.instantiateViewController(withIdentifier: "payVC") as? PaymentViewController
//            transfersVC?.webURL = transfer.rentalFormURL
//
//            let backItem = UIBarButtonItem()
//            backItem.title = ""
//
//            navigationItem.backBarButtonItem = backItem
//            self.navigationController?.pushViewController(transfersVC!, animated: true)
//        }
//        else{
            var name = "Main"
            
            if(UIDevice.current.userInterfaceIdiom == .pad){
                name = "Main_iPad"
            }
            let storyboard = UIStoryboard(name: name, bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "rentalsDVC") as? RentalsDVC
            controller?.currentRental = transfer
            let backItem = UIBarButtonItem()
            backItem.title = ""
            navigationItem.backBarButtonItem = backItem
            self.navigationController?.pushViewController(controller!, animated: true)
      //  }

    }
    @objc func openRatingsPage(tapGestureRecognizer: UITapGestureRecognizer){
        let label = tapGestureRecognizer.view as! UILabel
        var indexPath = IndexPath.init(row: 0, section: 0)
        
        indexPath = cvOptions.indexPath(for:  label.superview?.superview?.superview as! FTCollectionViewCell)!
        
        let tour = arrTours[indexPath.item]
        
        if(tour.tourTripAdvisorRatings != "0" && tour.tourTripAdvisorRatings != "" ){
            let tripAdvisorRatings =  tour.tourTripAdvisorRatings.components(separatedBy: "||")
            let url = tripAdvisorRatings[1]
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            
            let popWebVC = (storyboard.instantiateViewController(withIdentifier: "popWebVC") as? PopUpWebVC)!
            popWebVC.webURL = url
            popWebVC.titleTag = tour.tourName
            let width = ModalSize.custom(size: Float(UIScreen.main.bounds.width))
            let height = ModalSize.custom(size:  Float(UIScreen.main.bounds.height))
            let center = ModalCenterPosition.customOrigin(origin: CGPoint(x: 0, y:0))
            let customType2 = PresentationType.custom(width: width, height: height, center: center)
            
            let presenter = Presentr(presentationType: customType2)
            presenter.roundCorners = false
            presenter.backgroundColor = .black
            presenter.backgroundOpacity = 0.66
            presenter.dismissOnSwipe = false
            presenter.dismissAnimated = true
            
            customPresentViewController(presenter, viewController: popWebVC, animated: true, completion: nil)
        }
        
        
    }
    @objc func btnBookTourTapped(tapGestureRecognizer: UITapGestureRecognizer){
        let label = tapGestureRecognizer.view as! UILabel
        var indexPath = IndexPath.init(row: 0, section: 0)
        
        indexPath = cvOptions.indexPath(for:  label.superview?.superview?.superview as! FTCollectionViewCell)!
        
        let tour = arrTours[(indexPath.item)]
        
        
        
        
//        if(!tour.tourFormURL.isEmpty){
//            let storyboard = UIStoryboard(name: "Main", bundle: nil)
//            let bookVC = storyboard.instantiateViewController(withIdentifier: "payVC") as? PaymentViewController
//            bookVC?.webURL = tour.tourFormURL
//
//            let backItem = UIBarButtonItem()
//            backItem.title = ""
//
//            navigationItem.backBarButtonItem = backItem
//            self.navigationController?.pushViewController(bookVC!, animated: true)
//        }
//        else{
            var name = "Main"
            
            if(UIDevice.current.userInterfaceIdiom == .pad){
                name = "Main_iPad"
            }
            let storyboard = UIStoryboard(name: name, bundle: nil)

            let controller = storyboard.instantiateViewController(withIdentifier: "toursDVC") as? ToursDVC
            controller?.currentTour = tour
            let backItem = UIBarButtonItem()
            backItem.title = ""
            navigationItem.backBarButtonItem = backItem
            self.navigationController?.pushViewController(controller!, animated: true)
      //  }
  
    }

}
extension StoreViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if(segStoreOption.selectedSegmentIndex == 0){
            
            var name = "Main"
            
            if(UIDevice.current.userInterfaceIdiom == .pad){
                name = "Main_iPad"
            }
            let storyboard = UIStoryboard(name: name, bundle: nil)
            
            
            
            let controller = storyboard.instantiateViewController(withIdentifier: "toursDVC") as? ToursDVC
            controller?.currentTour = arrTours[indexPath.item]
            let backItem = UIBarButtonItem()
            backItem.title = ""
            navigationItem.backBarButtonItem = backItem
            
            self.navigationController?.pushViewController(controller!, animated: true)
        }
        else if(segStoreOption.selectedSegmentIndex == 1){
            var name = "Main"
            
            if(UIDevice.current.userInterfaceIdiom == .pad){
                name = "Main_iPad"
            }
            let storyboard = UIStoryboard(name: name, bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "rentalsDVC") as? RentalsDVC
            controller?.currentRental = arrRentals[indexPath.item]
            let backItem = UIBarButtonItem()
            backItem.title = ""
            navigationItem.backBarButtonItem = backItem
            self.navigationController?.pushViewController(controller!, animated: true)
        }
        else{
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let transfersVC = storyboard.instantiateViewController(withIdentifier: "payVC") as? PaymentViewController
            
            let transfer = arrTransfers[indexPath.item]
            transfersVC?.webURL = transfer.transferLink
            
            let backItem = UIBarButtonItem()
            backItem.title = ""
            
            navigationItem.backBarButtonItem = backItem
            self.navigationController?.pushViewController(transfersVC!, animated: true)
        }
    }
}

extension StoreViewController: UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if(segStoreOption.selectedSegmentIndex == 0){ return arrTours.count}
        else if(segStoreOption.selectedSegmentIndex == 1){ return arrRentals.count}
        else if(segStoreOption.selectedSegmentIndex == 2){ return arrTransfers.count}
        else{ return 0}
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        if(segStoreOption.selectedSegmentIndex == 0){ return 1}
        else if(segStoreOption.selectedSegmentIndex == 1){ return 1}
        else if(segStoreOption.selectedSegmentIndex == 2){ return 1}
        else { return 0 }
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if(segStoreOption.selectedSegmentIndex == 0){
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "tourCell", for: indexPath) as! FTCollectionViewCell
            
            let tour = arrTours[indexPath.item]
            cell.label1.text = tour.tourName
  
            let url = URL(string: tour.tourImage)
            cell.vw1?.isHidden = false
            cell.vw1?.layer.cornerRadius = 8.0
            cell.vw1?.layer.borderWidth = 1.0
            cell.vw1?.layer.borderColor = UIColor.clear.cgColor
            cell.vw1?.layer.masksToBounds = true
            cell.imgView1.kf.setImage(with: url)
            cell.imgView3.isHidden = true
            cell.imgView4.isHidden = true
            
            
            if(tour.tourTripAdvisorRatings != "0" && tour.tourTripAdvisorRatings != "" ){
                DispatchQueue.global(qos: .userInitiated).async {
                let tripAdvisorRatings =  tour.tourTripAdvisorRatings.components(separatedBy: "||")
                let ratingURL = URL(string: tripAdvisorRatings[0])
                let numReviews = tripAdvisorRatings[2]
              
                let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.imageTapped(tapGestureRecognizer:)))

                let book = UITapGestureRecognizer(target: self, action: #selector(self.openRatingsPage(tapGestureRecognizer:)))
              
                    
                     DispatchQueue.main.async {
                        cell.label6.isUserInteractionEnabled = true
                        cell.label6.addGestureRecognizer(book)
                        cell.label6.text = "\(numReviews) \(FTDataManager.getTBL(phrase: "Reviews"))"
                        cell.label6.underline()
                        cell.label5.text = FTDataManager.getTBL(phrase: "TripAdvisorRating")
                        cell.imgView2.isHidden = false
                        cell.imgView2.kf.setImage(with: ratingURL)
                        cell.imgView2.isUserInteractionEnabled = true
                        cell.imgView2.addGestureRecognizer(tapGestureRecognizer)
                    }
                }
                
               
    
            }
            else{
                cell.label5.text = ""
                cell.label6.text = ""
                cell.imgView2.image = nil
                cell.imgView2.isHidden = true
    
            }
            
            
            
            if(tour.tourDiscount.isZero){
                
                cell.label2.text = String(format: "$%.0f %@",ceil(tour.tourPrice  * FTDataManager.currencyValue),FTDataManager.currencyCode)
                cell.label3.text = ""
            }
            else{
                let off = FTDataManager.getTBL(phrase: "DiscountOff")
                let save = FTDataManager.getTBL(phrase: "DiscountSave")
                cell.label2.text = String(format: "$%.0f %@",ceil(tour.tourPrice * (1-tour.tourDiscount) * FTDataManager.currencyValue),FTDataManager.currencyCode)
                cell.label3.text = String(format: "%.0f%% \(off).\(save) $%.0f", tour.tourDiscount * 100, tour.tourPrice * tour.tourDiscount)
            }
            cell.label4.text = FTDataManager.getTBL(phrase: "BookNow").uppercased()
            let book = UITapGestureRecognizer(target: self, action: #selector(btnBookTourTapped(tapGestureRecognizer:)))
            cell.label4.isUserInteractionEnabled = true
            cell.label4.addGestureRecognizer(book)
            return cell
        }
        else if(segStoreOption.selectedSegmentIndex == 1){
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "rentalCell", for: indexPath) as! FTCollectionViewCell
            
            let rental = arrRentals[indexPath.item]
            cell.label1.text = rental.rentalName
 
            if(rental.rentalDiscount.isZero){
                cell.label2.text = String(format: "$%.0f %@",ceil(rental.rentalPrice  * FTDataManager.currencyValue),FTDataManager.currencyCode)
                cell.label3.text = ""
            }
            else{
                let off = FTDataManager.getTBL(phrase: "DiscountOff")
                let save = FTDataManager.getTBL(phrase: "DiscountSave")
                cell.label2.text = String(format: "$%.0f %@",ceil(rental.rentalPrice * (1 - rental.rentalDiscount) * FTDataManager.currencyValue), FTDataManager.currencyCode)
                
                cell.label3.text = String(format: "%.0f%% \(off).\(save) $%.0f", rental.rentalDiscount * 100, rental.rentalPrice * rental.rentalDiscount)
            }
            var url = URL(string: rental.rentalImage)
            
            cell.imgView1.kf.setImage(with: url)
            
            if(rental.rentalLogo.count > 0){
                cell.imgView3.isHidden = false
                cell.imgView4.isHidden = false
                url = URL(string: "http://www.feejeetraveller.com/tba/images/rentals/\(rental.rentalLogo)")
                cell.imgView4.kf.setImage(with: url)
            }
            cell.label4.text = FTDataManager.getTBL(phrase: "BookNow").uppercased()
            let book = UITapGestureRecognizer(target: self, action: #selector(btnBookRentalTapped(tapGestureRecognizer:)))
            cell.label4.isUserInteractionEnabled = true
            cell.label4.addGestureRecognizer(book)
            
            return cell
            
        }
        else if(segStoreOption.selectedSegmentIndex == 2){
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "transferCell", for: indexPath) as! FTCollectionViewCell
            
            let transfer = arrTransfers[indexPath.item]
            
            let url = URL(string: FTDataManager.homeURL + "/images/transfers/" + transfer.transferImage )
            
            let logoUrl = URL(string: FTDataManager.homeURL + "/images/transfers/" + transfer.transferLogo )
            cell.imgView1.kf.setImage(with: url)
            cell.imgView2.kf.setImage(with: logoUrl)
            
            cell.btn1.setTitle(FTDataManager.getTBL(phrase: "BookNow").uppercased(), for: .normal)
            
            cell.btn1.addTarget(self, action: #selector(btnBookTransferTapped(_:)), for: .touchUpInside)
          //  cell.imgView3.isHidden = true
         //   cell.imgView4.isHidden = true
            return cell
        }
        else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "tourCell", for: indexPath) as! FTCollectionViewCell
            
            let tour = arrTours[indexPath.row]
            cell.label1.text = tour.tourName
            
            let url = URL(string: tour.tourImage)
            cell.imgView1.kf.setImage(with: url)
           // cell.imgView3.isHidden = true
           // cell.imgView4.isHidden = true
            return cell
        }
    }
    
    
}
extension StoreViewController: UICollectionViewDelegateFlowLayout{

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        var width = (cvOptions.frame.size.width - 20 ) / 3
        
        if(UIDevice.current.userInterfaceIdiom == .phone){
            width =  (cvOptions.frame.size.width - 20 )
         
            return CGSize(width: width , height: width * 0.67 )
        }
        else{
            width =  (cvOptions.frame.size.width - 20 ) / 3
         
            return CGSize(width: width , height: width * 0.65 )
        }
        
    }
}

