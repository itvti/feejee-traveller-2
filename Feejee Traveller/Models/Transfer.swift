//
//  Transfer.swift
//  Feejee Traveller
//
//  Created by Vinay Prasad on 11/5/18.
//  Copyright © 2018 Vinay Prasad. All rights reserved.
//

import UIKit

struct Transfer {
    var transferID: String
    var transferName: String
    var transferDesc: String
    var transferLink: String
    var transferImage: String
    var transferLogo: String
    var transferType: String
    var transferTypeID: String
    var transferLangID: String
}

extension Transfer{
    init(){
        self.transferID = ""
        self.transferName = ""
        self.transferDesc = ""
        self.transferLink = ""
        self.transferImage = ""
        self.transferLogo = ""
        self.transferType = ""
        self.transferTypeID = ""
        self.transferLangID = ""
    }
}
