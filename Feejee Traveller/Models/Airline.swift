//
//  Airline.swift
//  Feejee Traveller
//
//  Created by Vinay Prasad on 10/5/18.
//  Copyright © 2018 Vinay Prasad. All rights reserved.
//

import UIKit

struct Airline {
    var airlineID: String
    var airlineName: String
    var airlineLogo: String
    var airlineWebsite: String
    var airlineImage: String
    var airlineCode: String
    var airlineFlightCode: String
    var airlineContact: [String]
}

extension Airline{
    init(){
        self.airlineID = ""
        self.airlineName = ""
        self.airlineLogo = ""
        self.airlineWebsite = ""
        self.airlineImage = ""
        self.airlineCode = ""
        self.airlineFlightCode = ""
        self.airlineContact = [String]()
    }
}
