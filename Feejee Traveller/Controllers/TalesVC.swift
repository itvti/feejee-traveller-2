//
//  TalesVC.swift
//  Feejee Traveller
//
//  Created by Vinay Prasad on 5/4/18.
//  Copyright © 2018 Vinay Prasad. All rights reserved.
//

import UIKit

class TalesVC: UIViewController {
    @IBOutlet weak var tvTips: UITableView!
    @IBOutlet weak var cvTips: UICollectionView!
    var arrStories: [Stories] = [Stories()]
    var HUD = LottieHUD("spinner")
    override func viewDidLoad() {
        super.viewDidLoad()
        HUD.contentMode = .scaleAspectFill
        HUD.size = CGSize(width: 50, height: 50)
        cvTips.dataSource = self
        cvTips.delegate = self
        
        updateImage()
        // Do any additional setup after loading the view.
    }

    func getData(){
        FTDataManager.saveStories()
        arrStories = FTDataManager.getStoriesData(langID: FTDataManager.languageID)
        self.cvTips.isHidden = false
        self.cvTips.reloadData()
        
         HUD.stopHUD()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func updateImage(){
        HUD.showHUD()
        self.title = FTDataManager.getTBL(phrase: "Tales").uppercased()
        switch FTDataManager.languageID {
        case "1":
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "english").withRenderingMode(.alwaysOriginal), style:.plain, target: self, action: #selector(changeLanguage))
            self.getData()
          
        case "2":
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "chinese").withRenderingMode(.alwaysOriginal), style:.plain, target: self, action: #selector(changeLanguage))
            self.getData()
         
        case "3":
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "japanese").withRenderingMode(.alwaysOriginal), style:.plain, target: self, action: #selector(changeLanguage))
            self.getData()

        default:
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "english").withRenderingMode(.alwaysOriginal), style:.plain, target: self, action: #selector(changeLanguage))
        }
       
    }
    @IBAction func changeLanguage() {
        let enAction = UIAlertAction(title: FTDataManager.getTranslationByLanguage(phrase: "English", langID: FTDataManager.languageID),
                                     style: .default) { (action) in
                                        FTDataManager.saveLanguage(langID: "1")
                                        self.updateImage()
                                        
        }
        let zhAction = UIAlertAction(title: FTDataManager.getTranslationByLanguage(phrase: "Chinese", langID: FTDataManager.languageID),
                                     style: .default) { (action) in
                                        FTDataManager.saveLanguage(langID: "2")
                                        self.updateImage()
        }
        let jpAction = UIAlertAction(title: FTDataManager.getTranslationByLanguage(phrase: "Japanese", langID: FTDataManager.languageID),
                                     style: .default) { (action) in
                                        FTDataManager.saveLanguage(langID: "3")
                                        self.updateImage()
                                        
                                        
        }
        let cancelAction = UIAlertAction(title: FTDataManager.getTBL(phrase: "Cancel"),
                                         style: .cancel) { (action) in
                                            // Respond to user selection of the action
        }
        
        var alert = UIAlertController(title: FTDataManager.getTBL(phrase: "SelectLanguage"),
                                      message: "",
                                      preferredStyle: .actionSheet)
        if(UIDevice.current.userInterfaceIdiom == .pad){
            alert = UIAlertController(title: FTDataManager.getTBL(phrase: "SelectLanguage"),
                                      message: "",
                                      preferredStyle:  .alert)
        }
        
        alert.addAction(enAction)
        alert.addAction(zhAction)
        alert.addAction(jpAction)
        
        alert.addAction(cancelAction)
        
        // On iPad, action sheets must be presented from a popover.
        //alert.popoverPresentationController?.barButtonItem = self.btnLanguage
        
        self.present(alert, animated: true) {
            // The alert was presented
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension TalesVC: UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "payVC") as? PaymentViewController
        
        let story = arrStories[indexPath.item]
        
        controller?.webURL = story.storyLink
        
        
        let backItem = UIBarButtonItem()
        backItem.title = ""
        navigationItem.backBarButtonItem = backItem
        self.navigationController?.pushViewController(controller!, animated: true)
    }
}
extension TalesVC: UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrStories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "talesCell", for: indexPath) as! FTCollectionViewCell
        // let cell = tableView.dequeueReusableCell(withIdentifier: "talesCell") as! FTTableViewCell
        let story = arrStories[indexPath.item]
        cell.label1.text = story.storyTitle.uppercased()
        
        let url = URL(string: story.storyImage)
        
        cell.imgView1.kf.setImage(with: url)
        
        return cell
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
}

extension TalesVC: UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if(UIDevice.current.userInterfaceIdiom == .pad){
            let width = (cvTips.frame.size.width - 20 ) / 3
            return CGSize(width: width , height: width * 0.65 )
        }
        else{
            let width = (cvTips.frame.size.width - 20 )
            return CGSize(width: width , height: width * 0.558 )
        }
    }
}




