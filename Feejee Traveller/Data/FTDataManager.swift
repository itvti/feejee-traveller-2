//
//  FTDataManager.swift
//  Feejee Traveller
//
//  Created by Vinay Prasad on 28/3/18.
//  Copyright © 2018 Vinay Prasad. All rights reserved.
//

import UIKit
import SwiftyJSON
import Firebase

final class FTDataManager {
    static var languageID = "1"
    static var langCode = "en"
    static let homeURL = "http://www.feejeetraveller.com/tba/"
    static var currencyValue = 0.51
    static var currencyCode = "USD"
    static var appColor = UIColor(hex: "1CBBB4")
    static func saveLanguage(langID: String){
        
        if(langID == "1"){
            Messaging.messaging().subscribe(toTopic: "eninfo")
            Messaging.messaging().unsubscribe(fromTopic: "zhinfo")
            Messaging.messaging().unsubscribe(fromTopic: "jpinfo")
        }
        if(langID == "2"){
            Messaging.messaging().subscribe(toTopic: "zhinfo")
            Messaging.messaging().unsubscribe(fromTopic: "eninfo")
            Messaging.messaging().unsubscribe(fromTopic: "jpinfo")
        }
        if(langID == "3"){
            Messaging.messaging().subscribe(toTopic: "jpinfo")
            Messaging.messaging().unsubscribe(fromTopic: "zhinfo")
            Messaging.messaging().unsubscribe(fromTopic: "eninfo")
        }
        
        
        self.languageID = langID
        var data : Data!
        let body : JSON = JSON(["language" : langID])
        
        do{
            let fileManager = FileManager.default
            let url =  try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            let jsonURL = url.appendingPathComponent("settings.json")
            
            try data =  body.rawData()
            try data?.write(to: jsonURL)
        }
        catch{}
        
    }
    
    static func saveWeather(){
            do{
                let dataurl = URL(string:"https://api.openweathermap.org/data/2.5/group?id=2202064,2198148&appid=05f157317f22f8ba19c3f73efa040a59&units=metric")
                let fileManager = FileManager.default
                let url =  try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
                let jsonURL = url.appendingPathComponent("weather.json")
                
                let data =  try Data(contentsOf: dataurl!)
        
                try data.write(to: jsonURL)
        }
        catch{}
    }
    static func checkUpdates3(){
        do{
            let dataurl = URL(string:"http://feejeetraveller.com/tba/calls_v2_ios/checkCalls.php")

            let data =  try Data(contentsOf: dataurl!)
            let json =  try JSON(data: data)

            let updates = JSON(json["updates"]).array
            
            let tour = updates![0]["tour"].stringValue
            let rental = updates![1]["rental"].stringValue
           // let business = updates![2]["business"].stringValue
            let featured = updates![3]["featured"].stringValue
           

            if(tour != getUpdateDateFor(table: "tour")){

                saveTourData()
                saveUpdateData()
            }
            if(rental != getUpdateDateFor(table: "rental")){
                saveRentalData()
                saveUpdateData()
            }
            
//            if(business != getUpdateDateFor(table: "business")){
//                savePlacesData()
//                saveUpdateData()
//            }
            if(featured != getUpdateDateFor(table: "featured")){
                saveFeaturedData()
                saveUpdateData()
            }

       
            
        }
        catch{}
        
    }
    
    static func checkUpdates()->Bool{
        do{
            let dataurl = URL(string:"http://feejeetraveller.com/tba/calls_v2_ios/checkCalls.php")
            
            let data =  try Data(contentsOf: dataurl!)
            let json =  try JSON(data: data)
            
            let updates = JSON(json["updates"]).array
            
            let tour = updates![0]["tour"].stringValue
            let rental = updates![1]["rental"].stringValue
            // let business = updates![2]["business"].stringValue
            let featured = updates![3]["featured"].stringValue
            
            var isUpdated = false
            if(tour != getUpdateDateFor(table: "tour")){
                
                saveTourData()
                saveUpdateData()
                
                isUpdated = true
            }
            if(rental != getUpdateDateFor(table: "rental")){
                saveRentalData()
                saveUpdateData()
                
                isUpdated = true
            }
            
            if(featured != getUpdateDateFor(table: "featured")){
                saveFeaturedData()
                saveUpdateData()
                
                isUpdated = true
            }
            
            return isUpdated
            
        }
        catch{   return false}
        
    }
    
    static func checkUpdates2(){
        let headers = [
            "cache-control": "no-cache",
            ]
        
        let request = NSMutableURLRequest(url: NSURL(string: "http://feejeetraveller.com/tba/calls_v2_ios/checkCalls.php")! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                //   print(error)
                //self.showOfflineAlert()
            } else {
                let httpResponse = response as? HTTPURLResponse
                // print(httpResponse)
                
                let responseData = data as? Data
                
                do{
                   // let tour =  try JSON(data: responseData!)
                    
                    let json =  try JSON(data: responseData!)
                    
                    let updates = JSON(json["updates"]).array
                    
                    let tour = updates![0]["tour"].stringValue
                    let rental = updates![1]["rental"].stringValue
                    // let business = updates![2]["business"].stringValue
                    let featured = updates![3]["featured"].stringValue
                    
                    
                    if(tour != getUpdateDateFor(table: "tour")){
                        
                        saveTourData()
                        saveUpdateData()
                    }
                    if(rental != getUpdateDateFor(table: "rental")){
                        saveRentalData()
                        saveUpdateData()
                    }
                    
                    if(featured != getUpdateDateFor(table: "featured")){
                        saveFeaturedData()
                        saveUpdateData()
                    }
                }
                catch{}
                
            }
        })
        
        dataTask.resume()
    }
    static func checkBusinessUpdates(){
        do{
            let dataurl = URL(string:"\(homeURL)calls_v2_ios/checkCalls.php")
            
            let data =  try Data(contentsOf: dataurl!)
            let json =  try JSON(data: data)
            
            let updates = JSON(json["updates"]).array
        
            let business = updates![2]["business"].stringValue
 
            if(business != getUpdateDateFor(table: "business")){
                savePlacesData()
                saveUpdateData()
            }
        }
        catch{}
        
    }
    static func saveCurrency(){
        do{
            let dataurl = URL(string:"\(homeURL)calls_v2_ios/currency.php")
        let fileManager = FileManager.default
        let url =  try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
       
        let jsonURL = url.appendingPathComponent("currency.json")
        
        let data =  try Data(contentsOf: dataurl!)
        
        try data.write(to: jsonURL)
        }
        catch{}
    }
    static func saveCurrency2(){
        let headers = [
            "cache-control": "no-cache",
            ]
        
        let request = NSMutableURLRequest(url: NSURL(string: "http://feejeetraveller.com/tba/calls_v2_ios/currency.php")! as URL,
                                          cachePolicy: .useProtocolCachePolicy,
                                          timeoutInterval: 10.0)
        request.httpMethod = "GET"
        request.allHTTPHeaderFields = headers
        
        let session = URLSession.shared
        let dataTask = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
            if (error != nil) {
                //   print(error)
                //self.showOfflineAlert()
            } else {
                let httpResponse = response as? HTTPURLResponse
                // print(httpResponse)
                
                let responseData = data as? Data
                
                do{
                    // let tour =  try JSON(data: responseData!)
                    
                    let json =  try JSON(data: responseData!)
                    
                    let values = json.array
                    self.currencyValue = values![0]["currency_rate"].doubleValue
                    self.currencyCode = values![0]["currency_code"].stringValue
                   
                }
                catch{}
                
            }
        })
        
        dataTask.resume()
    }
    static func getUpdateDateFor(table: String)->String{
        do{
            let fileManager = FileManager.default
            
            let url =  try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            let jsonURL = url.appendingPathComponent("updates.json")
            let data = try Data(contentsOf: jsonURL)
            let json =  try JSON(data: data)
            
            let updates  = JSON(json["updates"]).array
            
            
            if(updates != nil){
                for update in updates!{
                    return update[table].stringValue
                }
            }
           
        }
        catch{}
        
        return ""
    }
    static func getLanguage(){
        do {
            let fileManager = FileManager.default
            
            let url =  try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            let jsonURL = url.appendingPathComponent("settings.json")
            let data = try Data(contentsOf: jsonURL)
            
            let json =  try JSON(data: data)
     
            
            self.languageID = json["language"].stringValue
            
        }catch {}
        

    }
    static func getCurrency(){
        do {
            let fileManager = FileManager.default
            
            let url =  try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            let jsonURL = url.appendingPathComponent("currency.json")
            let data = try Data(contentsOf: jsonURL)
            
            let json =  try JSON(data: data)
            let values = json.array
            
          
            self.currencyValue = values![0]["currency_rate"].doubleValue
            self.currencyCode = values![0]["currency_code"].stringValue
            
        }catch {}
        
        
    }
    static func getWeather(city: String)->String{
        do {
            let fileManager = FileManager.default

            let url =  try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            let jsonURL = url.appendingPathComponent("weather.json")
          
            let data = try Data(contentsOf: jsonURL)
            
            let json =  try JSON(data: data)
            let list = JSON(json["list"]).array
            
            
            for value in list!{
                if(value["name"].stringValue == city){
                    let weather = ceil(JSON(value["main"]["temp"]).doubleValue)
                    let name = JSON(value["name"]).stringValue
                    let date = Date()
                    // "Jul 23, 2014, 11:01 AM" <-- looks local without seconds. But:
                    
                    let formatter = DateFormatter()
                    //formatter.timeZone = TimeZone(abbreviation: "GMT+12")
                    formatter.timeZone = TimeZone(identifier: "Pacific/Fiji")
                 
                    formatter.dateFormat = "hh:mm a"
                
                    // "2014-07-23 11:01:35 -0700" <-- same date, local, but with seconds
                    
                    let utcTimeZoneStr = formatter.string(from: date)

                    
                    return String(format: "%@\n%@ - %.0f℃" ,utcTimeZoneStr, name, weather)
                }
            }
            
        }catch {}
        
        return ""
        
    }
    static func getWeatherIcon(city: String)->String{
        do {
            let fileManager = FileManager.default
            
            let url =  try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            let jsonURL = url.appendingPathComponent("weather.json")
            
            let data = try Data(contentsOf: jsonURL)
            
            let json =  try JSON(data: data)
            let list = JSON(json["list"]).array
            
            
            for value in list!{
                if(value["name"].stringValue == city){
                    let weather = JSON(value["weather"]).array
                    
                
           
                    if((weather!.count) > 0){
                        return weather![0]["icon"].stringValue
                       
                    }
                    else{
                        return "01d"
                    }
                
                }
            }
            
        }catch {}
        
        return ""
        
    }
    static func saveUpdateData(){
        do{
            let dataurl = URL(string:"\(homeURL)calls_v2_ios/checkCalls.php")
            let fileManager = FileManager.default
            let url =  try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
    
            let jsonURL = url.appendingPathComponent("updates.json")
            
            let data =  try Data(contentsOf: dataurl!)
            
            try data.write(to: jsonURL)
            
        }
        catch{}
    }
    static func saveTourData(){
        do{
            let dataurl = URL(string:"\(homeURL)calls_v2_ios/tours.php")
            let fileManager = FileManager.default
            let url =  try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            let jsonURL = url.appendingPathComponent("tours.json")
            
            let data =  try Data(contentsOf: dataurl!)
            
            try data.write(to: jsonURL)
        }
        catch{}
    }
    static func saveTranslationData(){
        do{
            let dataurl = URL(string:"\(homeURL)calls_v2_ios/translations.php")
            let fileManager = FileManager.default
            let url =  try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            let jsonURL = url.appendingPathComponent("translations.json")
            
            let data =  try Data(contentsOf: dataurl!)
            try data.write(to: jsonURL)
        }
        catch{}
    }
    static func saveRentalData(){
        do{
            let dataurl = URL(string:"\(homeURL)calls_v2_ios/rentals.php")
            let fileManager = FileManager.default
            let url =  try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            let jsonURL = url.appendingPathComponent("rentals.json")
            
            let data =  try Data(contentsOf: dataurl!)
            try data.write(to: jsonURL)
        }
        catch{}
    }
    static func savePlacesData(){
        do{
            let dataurl = URL(string:"\(homeURL)calls_v2_ios/business.php")
            let fileManager = FileManager.default
            let url =  try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            let jsonURL = url.appendingPathComponent("places.json")
            
            let data =  try Data(contentsOf: dataurl!)
            
            try data.write(to: jsonURL)
        }
        catch{}
    }
    static func saveFeaturedData(){
        do{
            let dataurl = URL(string:"\(homeURL)calls_v2_ios/featured3.php")
            
            let fileManager = FileManager.default
            let url =  try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            let jsonURL = url.appendingPathComponent("featured.json")
        
            let data =  try Data(contentsOf: dataurl!)
       
            try data.write(to: jsonURL)
        }
        catch{}
    }
    static func getFeaturedData()-> [JSON]{
        do {
            let fileManager = FileManager.default
            
            let url =  try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            let jsonURL = url.appendingPathComponent("featured.json")
            let data = try Data(contentsOf: jsonURL)
            
            let json =  try JSON(data: data)
            let values = json.array
            
            return values!
            
            
        }catch {}
        
        return [JSON.null]
    }
    
    static func getEmergencyData()-> [JSON]{
        do {
            let fileManager = FileManager.default
            
            let url =  try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            let jsonURL = url.appendingPathComponent("emergency.json")
            let data = try Data(contentsOf: jsonURL)
            
            let json =  try JSON(data: data)
            let values = JSON(json["emergency"]).array!
            
            return values
            
        }
        catch {}
        return [JSON.null]
    }
    static func getDoctorData()-> [JSON]{
        do {
            let fileManager = FileManager.default
            
            let url =  try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            let jsonURL = url.appendingPathComponent("doctors.json")
            let data = try Data(contentsOf: jsonURL)
            
            let json =  try JSON(data: data)
            let values  = JSON(json["doctors"]).array!
            
            return values
            
        }
        catch {}
        return [JSON.null]
    }
    static func getPharmacyData()-> [JSON]{
        do {
            let fileManager = FileManager.default
            
            let url =  try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            let jsonURL = url.appendingPathComponent("pharmacies.json")
            let data = try Data(contentsOf: jsonURL)
            
            let json =  try JSON(data: data)
            let values  = JSON(json["pharmacies"]).array!
            
            return values
            
        }
        catch {}
        return [JSON.null]
    }
    static func getTourData()-> [JSON]{
        do {
            let fileManager = FileManager.default
            
            let url =  try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            let jsonURL = url.appendingPathComponent("tours.json")
            let data = try Data(contentsOf: jsonURL)
            
            let json =  try JSON(data: data)
            let tours = json.array
            
            return tours!
    
        }catch {}
        
        return [JSON.null]
    }
    static func getTourDataByLanguage(langID: String)-> [Tour]{
        do {
            let fileManager = FileManager.default
            let url =  try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            let jsonURL = url.appendingPathComponent("tours.json")

            let data = try Data(contentsOf: jsonURL)
           // let data = try Data(contentsOf: jsonURL)
        
            let json =  try JSON(data: data)
            let tours = json.array

            var allTours = [Tour]()
            
            for tour in tours!{
           
                
                if(tour["language_lang_id"].stringValue == langID){
       
                    let rates = tour["rates"].array
                    var allRates = [Rates]()
                    for rate in rates!{
                        if(rate["language_lang_id"].stringValue == langID){
                        allRates.append(Rates(rateID: rate["trates_id"].stringValue, ratePickUp: rate["trates_pickup"].stringValue, ratePreNote: rate["trates_prenote"].stringValue, ratePostNote: rate["trates_postnote"].stringValue, rateType: rate["trates_type"].stringValue, rateCost: rate["trates_cost"].doubleValue, rateDisplay: rate["trates_display"].stringValue, rateSpecial: rate["trates_special"].stringValue, rateLanguage: rate["trates_id"].stringValue))
                        }
                    }
                    
                    allTours.append(Tour(tourID: tour["tourmulti_id"].stringValue, tourName: tour["tour_name"].stringValue, tourDesc: tour["tour_description"].stringValue, tourHighlight: tour["tour_highlight"].stringValue, tourDuration: tour["tour_duration"].stringValue, tourImage: tour["tour_image"].stringValue, tourGuide: tour["tour_guide"].stringValue, tourVideo: tour["tour_video"].stringValue, tourType: tour["language_lang_id"].stringValue, tourPhone: tour["language_lang_id"].stringValue, tourEmail: tour["language_lang_id"].stringValue, tourWebsite:  tour["language_lang_id"].stringValue, tourFormURL: tour["tour_formurl"].stringValue, tourFeatureName: tour["language_lang_id"].stringValue, tourService: tour["language_lang_id"].stringValue, tourHours: tour["language_lang_id"].stringValue, tourPrice: tour["tour_price"].doubleValue, tourDiscount: tour["tour_discount"].doubleValue , tourTripAdvisorRatings: tour["tripadvisorid"].stringValue, tourAvailable: tour["language_lang_id"].stringValue, tourLanguage: tour["language_lang_id"].stringValue, tourRates: allRates))
                    
                  
                }
            }
            return allTours
            
        }catch {}
        
        return [Tour ()]
    }
    static func getTourDataByIDAndLanguage(tourID: String, langID: String)-> Tour{
        do {
            let fileManager = FileManager.default
            let url =  try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            let jsonURL = url.appendingPathComponent("tours.json")
            
            let data =  try Data(contentsOf: jsonURL)
            
            let json =  try JSON(data: data)
            let tours = json.array
            
            for tour in tours!{
                if(tour["language_lang_id"].stringValue == langID && tour["tourmulti_id"].stringValue == tourID){
                    let rates = tour["rates"].array
                    var allRates = [Rates]()
                    for rate in rates!{
                        if(rate["language_lang_id"].stringValue == langID){
                            allRates.append(Rates(rateID: rate["trates_id"].stringValue, ratePickUp: rate["trates_pickup"].stringValue, ratePreNote: rate["trates_prenote"].stringValue, ratePostNote: rate["trates_postnote"].stringValue, rateType: rate["trates_type"].stringValue, rateCost: rate["trates_cost"].doubleValue, rateDisplay: rate["trates_display"].stringValue, rateSpecial: rate["trates_special"].stringValue, rateLanguage: rate["trates_id"].stringValue))
                        }
                    }
                    
                    return Tour(tourID: tour["tourmulti_id"].stringValue, tourName: tour["tour_name"].stringValue, tourDesc: tour["tour_description"].stringValue, tourHighlight: tour["tour_highlight"].stringValue, tourDuration: tour["tour_duration"].stringValue, tourImage: tour["tour_image"].stringValue, tourGuide: tour["tour_guide"].stringValue, tourVideo: tour["tour_video"].stringValue, tourType: tour["language_lang_id"].stringValue, tourPhone: tour["language_lang_id"].stringValue, tourEmail: tour["language_lang_id"].stringValue, tourWebsite:  tour["language_lang_id"].stringValue, tourFormURL: tour["tour_formurl"].stringValue, tourFeatureName: tour["language_lang_id"].stringValue, tourService: tour["language_lang_id"].stringValue, tourHours: tour["language_lang_id"].stringValue, tourPrice: tour["tour_price"].doubleValue, tourDiscount: tour["tour_discount"].doubleValue , tourTripAdvisorRatings: tour["tripadvisorid"].stringValue, tourAvailable: tour["language_lang_id"].stringValue, tourLanguage: tour["language_lang_id"].stringValue, tourRates: allRates)
                }
            }
    
            
        }catch {}
        
        return getTourDataByIDAndLanguage(tourID: tourID, langID: "1")
    }
    
    static func getTranslationByLanguage(phrase: String, langID: String)-> String{
        do {
            let fileManager = FileManager.default
            
            let url =  try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            let jsonURL = url.appendingPathComponent("translations.json")
            let data = try Data(contentsOf: jsonURL)
            
            let json =  try JSON(data: data)
            
            
            let translations = json.array
   
            for translation in translations!{
                if(translation["language_id"].stringValue == langID && translation["phrase"].stringValue == phrase) {
                    return translation["phraseTranslation"].stringValue
                }
            }
        }catch {}
        
        return ""
    }
    static func getTBL(phrase: String)-> String{
        do {
            let fileManager = FileManager.default
            
            let url =  try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            let jsonURL = url.appendingPathComponent("translations.json")
            
            let data = try Data(contentsOf: jsonURL)
            let json =  try JSON(data: data)
            
            
            let translations = json.array
            
            for translation in translations!{
                if(translation["language_id"].stringValue == FTDataManager.languageID && translation["phrase"].stringValue == phrase) {
                    return translation["phraseTranslation"].stringValue
                }
            }
        }catch {}
        
        return ""
    }
    static func getRentalDataByLanguage(langID: String)-> [Rental]{
        do {
            let fileManager = FileManager.default
            
            let url =  try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            let jsonURL = url.appendingPathComponent("rentals.json")
            let data = try Data(contentsOf: jsonURL)
            
            let json =  try JSON(data: data)
            let rentals = json.array
            
            var allRentals = [Rental]()
            
            for rental in rentals!{
                if(rental["language_lang_id"].stringValue == langID){
                    allRentals.append(Rental(rentalID: rental["rentalmulti_id"].stringValue, rentalName: rental["rental_name"].stringValue, rentalModel: rental["rental_model"].stringValue, rentalFeatures: rental["rental_features"].stringValue, rentalDeal: rental["rental_deal"].stringValue, rentalImage: rental["rental_hero"].stringValue, rentalPrice: rental["rental_price"].doubleValue, rentalDiscount: rental["rental_discount"].doubleValue, rentalExtraPrice: rental["rental_extraprice"].doubleValue, rentalFormURL: rental["rental_bookingform"].stringValue, rentalVideo: rental["rental_video"].stringValue, rentalAvailable: rental["rental_isavailable"].stringValue, rentalLanguage: rental["language_lang_id"].stringValue, rentalLogo: rental["rcompany_image"].stringValue))
                }
            }
            return allRentals
            
        }catch {}
        
        return [Rental()]
    }
    
    static func getRentalDataByIDAndLanguage(rentalID: String, langID: String)-> Rental{
        do {
            let fileManager = FileManager.default
            
            let url =  try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            let jsonURL = url.appendingPathComponent("rentals.json")

            let data = try Data(contentsOf: jsonURL)
            let json =  try JSON(data: data)
            let rentals = json.array
    
            for rental in rentals!{
                if(rental["language_lang_id"].stringValue == langID && rental["rentalmulti_id"].stringValue == rentalID  ){
                    return Rental(rentalID: rental["rentalmulti_id"].stringValue, rentalName: rental["rental_name"].stringValue, rentalModel: rental["rental_model"].stringValue, rentalFeatures: rental["rental_features"].stringValue, rentalDeal: rental["rental_deal"].stringValue, rentalImage: rental["rental_hero"].stringValue, rentalPrice: rental["rental_price"].doubleValue,  rentalDiscount: rental["rental_discount"].doubleValue, rentalExtraPrice: rental["rental_extraprice"].doubleValue, rentalFormURL: rental["rental_bookingform"].stringValue, rentalVideo: rental["rental_video"].stringValue, rentalAvailable: rental["rental_isavailable"].stringValue, rentalLanguage: rental["language_lang_id"].stringValue, rentalLogo: rental["rcompany_image"].stringValue)
                    
                    
                }
            }
        
        }catch {}
        
        return getRentalDataByIDAndLanguage(rentalID: rentalID, langID: "1")
    }
    
    static func getPlacesDataByLanguage(langID: String)-> [Business]{
        do {
            let fileManager = FileManager.default
            
            let url =  try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            let jsonURL = url.appendingPathComponent("places.json")
            let data = try Data(contentsOf: jsonURL)
            
            let json =  try JSON(data: data)
            let places = json.array
            
            var allPlaces = [Business]()
            
            for place in places!{
                if(place["business"]["language_lang_id"].stringValue == langID){
                    let vouchers = place["vouchers"].array
                    
                    var voucher = Voucher.init()
                    for v in vouchers!{
                        if(v["language_lang_id"].stringValue == langID){
                            voucher = Voucher(id: v["id"].stringValue, voucherName: v["bv_name"].stringValue, voucherLink: v["bv_link"].stringValue, voucherStartDate: v["bv_startdate"].stringValue, voucherEndDate: v["bv_enddate"].stringValue)
                        }
                    }
                    
                    allPlaces.append(Business(id: place["business"]["businessmulti_id"].stringValue, bName: place["business"]["business_name"].stringValue, bDesc: place["business"]["business_description"].stringValue, bImage: place["business"]["business_image"].stringValue, bType: place["business"]["btype_multi_id"].stringValue, bVideo: place["business"]["business_video"].stringValue, bPhone: place["business"]["business_phone"].stringValue, bEmail: place["business"]["business_email"].stringValue, bWebsite: place["business"]["business_web"].stringValue, bFacebook: place["business"]["business_facebook"].stringValue, bTwitter: place["business"]["business_twitter"].stringValue,bFeature: place["business"]["business_feature"].stringValue, bFeatureName: place["business"]["business_featurename"].stringValue, bService: place["business"]["business_service"].stringValue, bHours: place["business"]["business_hours"].stringValue, bAvailable: place["business"]["business_available"].stringValue, bListingType: place["business"]["business_listingtype_list_id"].stringValue, bBranch: [Branch](), bVoucher: voucher))
                }
            }
            return allPlaces
            
        }catch {}
        
        return [Business()]
    }
    
    static func getPlacesDataByLanguageAndID(langID: String, businessID: String)-> [Business]{
        do {
        
            
            let fileManager = FileManager.default

            let url =  try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            let jsonURL = url.appendingPathComponent("places.json")
            let data = try Data(contentsOf: jsonURL)

            let json =  try JSON(data: data)
            let places = json.array
            
            var allPlaces = [Business]()
            
            for place in places!{
                if(place["business"]["language_lang_id"].stringValue == langID && place["business"]["businessmulti_id"].stringValue == businessID){
                    let vouchers = place["vouchers"].array

                    var voucher = Voucher.init()
                    for v in vouchers!{
                        if(v["language_lang_id"].stringValue == langID){
                            voucher = Voucher(id: v["id"].stringValue, voucherName: v["bv_name"].stringValue, voucherLink: v["bv_link"].stringValue, voucherStartDate: v["bv_startdate"].stringValue, voucherEndDate: v["bv_enddate"].stringValue)
                        }
                    }
                    
                    
                    let branches = place["branches"].array
                    
                    var allBranches = [Branch]()
                    for branch in branches!{
                        //if(branch["language_lang_id"].stringValue == langID){
                        allBranches.append(Branch(id: branch["branch_id"].stringValue, bName: place["business"]["business_name"].stringValue, bLocation: branch["branch_location"].stringValue, bAddress: branch["branch_address"].stringValue,  bLongitude: branch["branch_longitude"].stringValue, bLatitude: branch["branch_latitude"].stringValue, bImage: branch["branch_image"].stringValue, bType: branch["branch_type"].stringValue, bVideo: branch["branch_video"].stringValue, bPhone: branch["branch_phone"].stringValue, bEmail: branch["branch_email"].stringValue, bWebsite: branch["branch_website"].stringValue, bService: branch["branch_services"].stringValue, bHours: branch["branch_hours"].stringValue, bAvailable: branch["branch_available"].stringValue))
                      //  }
                    }
                    
                    let bran = allBranches.sorted(by: {  $1.bLocation < $0.bLocation })
                    
                    allPlaces.append(Business(id: place["business"]["businessmulti_id"].stringValue, bName: place["business"]["business_name"].stringValue, bDesc: place["business"]["business_description"].stringValue, bImage: place["business"]["business_image"].stringValue, bType: place["business"]["business_type_btypemulti_id"].stringValue, bVideo: place["business"]["business_video"].stringValue, bPhone: place["business"]["business_phone"].stringValue, bEmail: place["business"]["business_email"].stringValue, bWebsite: place["business"]["business_website"].stringValue, bFacebook: place["business"]["business_facebook"].stringValue, bTwitter: place["business"]["business_twitter"].stringValue, bFeature: place["business"]["business_feature"].stringValue, bFeatureName: place["business"]["business_featurename"].stringValue, bService: place["business"]["business_service"].stringValue, bHours: place["business"]["business_hours"].stringValue, bAvailable: place["business"]["business_available"].stringValue,bListingType: place["business"]["business_listingtype_list_id"].stringValue, bBranch: bran, bVoucher: voucher))
                    
                    return allPlaces
                }
                
                
            }
            
            
        }catch {}
        
        return [Business()]
    }
    
    static func getPlacesDataByLanguageAndType(langID: String, typeID: String)-> [Business]{
        do {
            let fileManager = FileManager.default

            let url =  try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            let jsonURL = url.appendingPathComponent("places.json")
            let data = try Data(contentsOf: jsonURL)
            
      
            let json =  try JSON(data: data)
            let places = json.array
            
            var allPlaces = [Business]()
            
            for place in places!{
                if(place["business"]["language_lang_id"].stringValue == langID && place["business"]["business_type_btypemulti_id"].stringValue == typeID){
                    
                    if(place["business"]["businessmulti_id"].stringValue == "239"){
    
                    }
                    let vouchers = place["vouchers"].array
                    
                    var voucher = Voucher.init()
                    for v in vouchers!{
                        if(v["language_lang_id"].stringValue == langID){
                            voucher = Voucher(id: v["id"].stringValue, voucherName: v["bv_name"].stringValue, voucherLink: v["bv_link"].stringValue, voucherStartDate: v["bv_startdate"].stringValue, voucherEndDate: v["bv_enddate"].stringValue)
                        }
                    }
                    
                    
                    let branches = place["branches"].array
                    var allBranches = [Branch]()
                    for branch in branches!{
                        allBranches.append(Branch(id: branch["branch_id"].stringValue, bName: place["business"]["business_name"].stringValue, bLocation: branch["branch_location"].stringValue, bAddress: branch["branch_address"].stringValue,  bLongitude: branch["branch_longitude"].stringValue, bLatitude: branch["branch_latitude"].stringValue, bImage: branch["branch_image"].stringValue, bType: branch["branch_type"].stringValue, bVideo: branch["branch_video"].stringValue, bPhone: branch["branch_phone"].stringValue, bEmail: branch["branch_email"].stringValue, bWebsite: branch["branch_website"].stringValue, bService: branch["branch_services"].stringValue, bHours: branch["branch_hours"].stringValue, bAvailable: branch["branch_available"].stringValue))
                    }
                    
                    allPlaces.append(Business(id: place["business"]["businessmulti_id"].stringValue, bName: place["business"]["business_name"].stringValue, bDesc: place["business"]["business_description"].stringValue, bImage: place["business"]["business_image"].stringValue, bType: place["business"]["business_type_btypemulti_id"].stringValue, bVideo: place["business"]["business_video"].stringValue, bPhone: place["business"]["business_phone"].stringValue, bEmail: place["business"]["business_email"].stringValue, bWebsite: place["business"]["business_web"].stringValue,bFacebook: place["business"]["business_facebook"].stringValue, bTwitter: place["business"]["business_twitter"].stringValue, bFeature: place["business"]["business_feature"].stringValue, bFeatureName: place["business"]["business_featurename"].stringValue, bService: place["business"]["business_service"].stringValue, bHours: place["business"]["business_hours"].stringValue, bAvailable: place["business"]["business_available"].stringValue,bListingType: place["business"]["business_listingtype_list_id"].stringValue, bBranch: allBranches, bVoucher: voucher))
                    
                    
                    
                }
            }
            return allPlaces
            
        }catch {}
        
        return [Business()]
    }
    static func getPlacesData()-> [Business]{
        do {
            let fileManager = FileManager.default
            
            let url =  try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            let jsonURL = url.appendingPathComponent("places.json")
            let data = try Data(contentsOf: jsonURL)
            
            let json =  try JSON(data: data)
            let places = json.array
            var allPlaces = [Business]()
            for place in places!{
                
                allPlaces.append(Business(id: place["businessmulti_id"].stringValue, bName: place["business_name"].stringValue, bDesc: place["business_description"].stringValue, bImage: place["business_image"].stringValue, bType: place["business_type"].stringValue, bVideo: place["business_video"].stringValue, bPhone: place["business_phone"].stringValue, bEmail: place["business_phone"].stringValue, bWebsite: place["business_web"].stringValue, bFacebook: place["business"]["business_facebook"].stringValue, bTwitter: place["business"]["business_twitter"].stringValue, bFeature: place["business_feature"].stringValue, bFeatureName: place["business_featurename"].stringValue, bService: place["business_service"].stringValue, bHours: place["business_hours"].stringValue, bAvailable: place["business_available"].stringValue, bListingType: place["business_listingtype_list_id"].stringValue,bBranch: [Branch](), bVoucher: Voucher()))
            }
            return allPlaces
            
            
        }catch {}
        
        return [Business()]
    }
    
    static func getEtiquetteData(langID: String)->[Etiquette]{
        do {
            let fileManager = FileManager.default
            
            let url =  try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            let jsonURL = url.appendingPathComponent("etiquette.json")
            let data = try Data(contentsOf: jsonURL)
            
            let json =  try JSON(data: data)
         
            
          
            let etqs = json.array
            var allEtq = [Etiquette]()
            
            for etq in etqs!{
                if(etq["language_lang_id"].stringValue == langID){
                allEtq.append(Etiquette(etiquetteID: etq["etiquette_id"].stringValue, etiquetteTitle: etq["etiquette_title"].stringValue, etiquetteDescription: etq["etiquette_description"].stringValue, etiquetteImage: etq[""].stringValue, etiquetteLangID: etq["language_lang_id"].stringValue))
                }
            }
            
            return allEtq
            
            
        }catch {}
        
        return [Etiquette()]
    }
    
    static func saveEtiquette(){
        do{
            let dataurl = URL(string:"\(homeURL)calls_v2_ios/etiquette.php")
            let fileManager = FileManager.default
            let url =  try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            let jsonURL = url.appendingPathComponent("etiquette.json")
            
            let data =  try Data(contentsOf: dataurl!)
            try data.write(to: jsonURL)
        }
        catch{}
    }
    static func getPrearivalData(langID: String)->[Prearrival]{
        do {
            let fileManager = FileManager.default

            let url =  try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            let jsonURL = url.appendingPathComponent("prearrival.json")

            let data = try Data(contentsOf: jsonURL)
            
            let json =  try JSON(data: data)
            let pres = json.array
            var allPres = [Prearrival]()
            for pre in pres!{
                if(pre["language_lang_id"].stringValue == langID){
                    allPres.append(Prearrival(preID: pre["pre_id"].stringValue, preTitle: pre["pre_title"].stringValue,preDescription: pre["pre_description"].stringValue, preLangID: pre["language_lang_id"].stringValue))
                }
            }
            return allPres
            
            
        }catch {}
        
        return [Prearrival()]
    }
    
    static func savePrearrival(){
        do{
            let dataurl = URL(string:"\(homeURL)calls_v2_ios/prearrival.php")
            let fileManager = FileManager.default
            let url =  try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            let jsonURL = url.appendingPathComponent("prearrival.json")
            
            let data =  try Data(contentsOf: dataurl!)
            try data.write(to: jsonURL)
        }
        catch{}
    }
    static func getPhrasesData(langID: String)->[Phrases]{
        do {

            let fileManager = FileManager.default
            
            let url =  try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            let jsonURL = url.appendingPathComponent("phrases.json")
            let data = try Data(contentsOf: jsonURL)
            
            let json =  try JSON(data: data)
            let phrases = json.array
            var allPhrases = [Phrases]()
            for phrase in phrases!{
                if(phrase["language_lang_id"].stringValue == langID){
                   allPhrases.append(Phrases(phraseID: phrase["td_id"].stringValue, phraseWord:  phrase["td_enword"].stringValue, phraseTranslation:  phrase["trword"].stringValue, phraseLangID:  phrase["language_lang_id"].stringValue))
                }
            }
            return allPhrases
            
            
        }catch {}
        
        return [Phrases()]
    }
    
    static func savePhrases(){
        do{
            let dataurl = URL(string:"\(homeURL)calls_v2_ios/phrases.php")
            let fileManager = FileManager.default
            let url =  try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            let jsonURL = url.appendingPathComponent("phrases.json")
            
            let data =  try Data(contentsOf: dataurl!)
            try data.write(to: jsonURL)
        }
        catch{}
    }
    static func getStoriesData(langID: String)->[Stories]{
        do {
            let fileManager = FileManager.default
            
            let url =  try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            let jsonURL = url.appendingPathComponent("stories.json")
            let data = try Data(contentsOf: jsonURL)
            
            let json =  try JSON(data: data)
            let stories = json.array
            var allStories = [Stories]()
            for story in stories!{
                if(story["language_lang_id"].stringValue == langID){
                   allStories.append(Stories(storyID: story["stories_id"].stringValue, storyTitle: story["stories_title"].stringValue, storyDescription: story["stories_description"].stringValue, storyImage: story["stories_image"].stringValue, storyLink: story["stories_link"].stringValue, storyLangID: story["language_lang_id"].stringValue))
                }
            }
            return allStories
            
            
        }catch {}
        
        return [Stories()]
    }
    
    static func saveStories(){
        do{
            let dataurl = URL(string:"\(homeURL)calls_v2_ios/stories.php")
            let fileManager = FileManager.default
            let url =  try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            let jsonURL = url.appendingPathComponent("stories.json")
            
            let data =  try Data(contentsOf: dataurl!)
            try data.write(to: jsonURL)
        }
        catch{}
    }
    static func getAirlinesData(langID: String)->[Airline]{
        do {
            let fileManager = FileManager.default
            
            let url =  try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            let jsonURL = url.appendingPathComponent("airlines.json")
            let data = try Data(contentsOf: jsonURL)
            
            let json =  try JSON(data: data)
            let airlines = json.array
            var allAirlines = [Airline]()
            for airline in airlines!{
                let contacts = airline["contacts"].array
                var allContacts = [String]()
                for contact in contacts!{
                   allContacts.append(String(contact["airline_contact"].stringValue))
                }
                
                allAirlines.append(Airline(airlineID: airline["airline"]["airline_id"].stringValue, airlineName: airline["airline"]["airline_name"].stringValue, airlineLogo: airline["airline"]["airline_logo"].stringValue, airlineWebsite: airline["airline"]["airline_website"].stringValue , airlineImage: airline["airline"]["airline_image"].stringValue, airlineCode: airline["airline"]["airline_code"].stringValue, airlineFlightCode: airline["airline"]["airline_flightcode"].stringValue, airlineContact: allContacts))
                
            }
            
            return allAirlines
            
            
        }catch {}
        
        return [Airline()]
    }
    
    static func saveAirline(){
        do{
            let dataurl = URL(string:"\(homeURL)calls_v2_ios/airlines.php")
            let fileManager = FileManager.default
            let url =  try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            let jsonURL = url.appendingPathComponent("airlines.json")
            
            let data =  try Data(contentsOf: dataurl!)
            try data.write(to: jsonURL)
        }
        catch{}
    }
    static func getTransfersData(langID: String)->[Transfer]{
        do {
            let fileManager = FileManager.default
            let url =  try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            let jsonURL = url.appendingPathComponent("transfers.json")

            let data = try Data(contentsOf: jsonURL)
            let json =  try JSON(data: data)
            let transfers = json.array
            var allTransfers = [Transfer]()
            for transfer in transfers!{
                if(transfer["language_lang_id"].stringValue == langID){
                    allTransfers.append(Transfer(transferID: transfer["transfermulti_id"].stringValue, transferName: transfer["transfer_name"].stringValue, transferDesc: transfer["transfer_description"].stringValue, transferLink: transfer["transfer_link"].stringValue, transferImage: transfer["transfer_image"].stringValue, transferLogo: transfer["transfer_logo"].stringValue, transferType: transfer["ttype_description"].stringValue, transferTypeID: transfer["transfer_type_ttype_id"].stringValue, transferLangID: transfer["language_lang_id"].stringValue))
                }
            }
            return allTransfers
            
            
        }catch {}
        
        return [Transfer()]
    }
    
    static func saveTransfers(){
        do{
            let dataurl = URL(string:"\(homeURL)calls_v2_ios/transfers.php")
            
            let fileManager = FileManager.default
            let url =  try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            let jsonURL = url.appendingPathComponent("transfers.json")
            
            let data =  try Data(contentsOf: dataurl!)
            try data.write(to: jsonURL)
        }
        catch{}
    }
    static func getEventsData(langID: String)->[Event]{
        do {
            let fileManager = FileManager.default
            
            let url =  try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            let jsonURL = url.appendingPathComponent("events.json")
            let data = try Data(contentsOf: jsonURL)
            
            let json =  try JSON(data: data)
            let events = json.array
            var allEvents = [Event]()
            for event in events!{
                if(event["language_lang_id"].stringValue == langID){
                   allEvents.append(Event(eventID: event["event_id"].stringValue, eventName: event["event_name"].stringValue, eventImage: event["event_image"].stringValue, eventStartDate: event["event_startdate"].stringValue, eventEndDate: event["event_enddate"].stringValue, eventLangID: event["language_lang_id"].stringValue))
                }
            }
            return allEvents
            
            
        }catch {}
        
        return [Event()]
    }
    
    static func saveEvents(){
        do{
            let dataurl = URL(string:"\(homeURL)calls_v2_ios/events.php")
            let fileManager = FileManager.default
            let url =  try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            let jsonURL = url.appendingPathComponent("events.json")
            
            let data =  try Data(contentsOf: dataurl!)
            try data.write(to: jsonURL)
        }
        catch{}
    }
    static func saveHotels(){
        do{
            let dataurl = URL(string:"\(homeURL)calls_v2_ios/hotels.php")
            let fileManager = FileManager.default
            let url =  try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            let jsonURL = url.appendingPathComponent("hotels.json")
            
            let data =  try Data(contentsOf: dataurl!)
            try data.write(to: jsonURL)
        }
        catch{}
    }
    static func getHotelData(langID: String)->String{
        do {
            let fileManager = FileManager.default
            
            let url =  try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            let jsonURL = url.appendingPathComponent("hotels.json")
            let data = try Data(contentsOf: jsonURL)
            
            let json =  try JSON(data: data)
            let hotels = json.array
            
            for hotel in hotels!{
                if(hotel["lang_id"].stringValue == langID){
                    return hotel["hotel_link"].stringValue
                }
            }
        } catch {}
        
        return ""
    }
    static func saveShop(){
        do{
            let dataurl = URL(string:"\(homeURL)calls_v2_ios/shop.php")
            let fileManager = FileManager.default
            let url =  try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            let jsonURL = url.appendingPathComponent("shop.json")
            
            let data =  try Data(contentsOf: dataurl!)
            try data.write(to: jsonURL)
        }
        catch{}
    }
    static func getShopData(langID: String)->String{
        do {
            let fileManager = FileManager.default
            
            let url =  try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            let jsonURL = url.appendingPathComponent("shop.json")
            let data = try Data(contentsOf: jsonURL)
            
            let json =  try JSON(data: data)
            let hotels = json.array
            
            for hotel in hotels!{
                if(hotel["lang_id"].stringValue == langID){
                    return hotel["shop_link"].stringValue
                }
            }
        } catch {}
        
        return ""
    }
    static func saveVideos(){
        do{
            let dataurl = URL(string:"\(homeURL)calls_v2_ios/videos.php")
            let fileManager = FileManager.default
            let url =  try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            let jsonURL = url.appendingPathComponent("videos.json")
            
            let data =  try Data(contentsOf: dataurl!)
            try data.write(to: jsonURL)
        }
        catch{}
    }
    static func getVideosData(langID: String)->[Video]{
        do {
            let fileManager = FileManager.default
            
            let url =  try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            let jsonURL = url.appendingPathComponent("videos.json")
            let data = try Data(contentsOf: jsonURL)
            
            let json =  try JSON(data: data)
            let videos = json.array
            var allVideos = [Video]()
            for video in videos!{
                if(video["language_lang_id"].stringValue == langID){
                    allVideos.append(Video(videoID: video["vids_id"].stringValue, videoName: video["vids_title"].stringValue, videoImage: video["vid_image_url"].stringValue, videoURL: video["vids_url"].stringValue, videoLangID: video["language_lang_id"].stringValue))
                }
            }
            return allVideos
            
            
        }catch {}
        
        return [Video()]
    }

   
    static func getBagData(){
        
    }
    static func getStoreData(){
        
    }
    static func getLegalData(langID: String)->[Prearrival]{
        do {
            let fileManager = FileManager.default
            
            let url =  try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            let jsonURL = url.appendingPathComponent("legal.json")
            let data = try Data(contentsOf: jsonURL)
            
            let json =  try JSON(data: data)
            let pres = json.array
            var allPres = [Prearrival]()
            for pre in pres!{
                if(pre["language_lang_id"].stringValue == langID){
                    allPres.append(Prearrival(preID: pre["legalmulti_id"].stringValue, preTitle: pre["legal_name"].stringValue,preDescription: pre["legal_description"].stringValue, preLangID: pre["language_lang_id"].stringValue))
                }
            }
            return allPres
            
            
        }catch {}
        
        return [Prearrival()]
    }
    
    static func saveLegal(){
        do{
            let dataurl = URL(string:"\(homeURL)calls_v2_ios/legal.php")
            let fileManager = FileManager.default
            let url =  try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            let jsonURL = url.appendingPathComponent("legal.json")
            
            let data =  try Data(contentsOf: dataurl!)
            try data.write(to: jsonURL)
        }
        catch{}
    }
    
    static func logStats(id: String, table: String, type: String){
        do{
            let dataurl = "\(homeURL)calls_v2_ios/addstats.php?table=\(table)&multiid=\(id)&type=\(type)&lang=\(FTDataManager.languageID)"

            
            let w = Wings()
            if let response = w.get(url: dataurl, headers: nil) {
                if let json = response.data{
                }
            }
        }
        catch{}
    }
}
