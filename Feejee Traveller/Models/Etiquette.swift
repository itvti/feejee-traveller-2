//
//  Etiquette.swift
//  Feejee Traveller
//
//  Created by Vinay Prasad on 8/5/18.
//  Copyright © 2018 Vinay Prasad. All rights reserved.
//

import UIKit

struct Etiquette {
    var etiquetteID: String
    var etiquetteTitle: String
    var etiquetteDescription: String
    var etiquetteImage: String
    var etiquetteLangID: String

    
}

extension Etiquette{
    init(){
        self.etiquetteID = ""
        self.etiquetteTitle = ""
        self.etiquetteDescription = ""
        self.etiquetteImage = ""
        self.etiquetteLangID = ""
    }
}

