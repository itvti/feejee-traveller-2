//
//  TipsVC.swift
//  Feejee Traveller
//
//  Created by Vinay Prasad on 5/4/18.
//  Copyright © 2018 Vinay Prasad. All rights reserved.
//

import UIKit

class TipsVC: UIViewController {
    @IBOutlet weak var tvTips: UITableView!
    @IBOutlet weak var segView: UISegmentedControl!
    
    @IBAction func segTipsChanged(_ sender: Any) {
        tvTips.reloadData()
        let indexPath = IndexPath(item: 0, section: 0)
        self.tvTips.scrollToRow(at: indexPath, at: .top, animated: true)
    }
    
    var arrEtq : [Etiquette] = [Etiquette()]
    var arrPhrases : [Phrases] = [Phrases()]
    var arrPre : [Prearrival] = [Prearrival()]
    var HUD = LottieHUD("spinner")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        HUD.contentMode = .scaleAspectFill
        HUD.size = CGSize(width: 50, height: 50)
        tvTips.isHidden = true
        tvTips.dataSource = self
        segView.backgroundColor = UIColor(white: 1.0, alpha: 0.42)
        segView.setTitleTextAttributes([
            
            NSAttributedStringKey.foregroundColor: UIColor.white
            ], for: .selected)
        
        tvTips.tableFooterView = UIView()
      
 
    }
    override func viewDidAppear(_ animated: Bool) {
        self.updateImage()
        
    }
    func getData(){
        self.title = FTDataManager.getTBL(phrase: "TravellerTips").uppercased()
        HUD.showHUD()
        DispatchQueue.global(qos: .userInitiated).async {
            FTDataManager.saveEtiquette()
            FTDataManager.savePhrases()
            FTDataManager.savePrearrival()
     
            DispatchQueue.main.async {
                self.segView.setTitle(FTDataManager.getTBL(phrase: "Etq"), forSegmentAt: 0)
                self.segView.setTitle(FTDataManager.getTBL(phrase: "Phrases"), forSegmentAt: 1)
                self.segView.setTitle(FTDataManager.getTBL(phrase: "Prearrival"), forSegmentAt: 2)
               
                self.arrEtq = FTDataManager.getEtiquetteData(langID: FTDataManager.languageID)
                self.arrPhrases = FTDataManager.getPhrasesData(langID: FTDataManager.languageID)
                self.arrPre = FTDataManager.getPrearivalData(langID: FTDataManager.languageID)
                
                self.HUD.stopHUD()
                self.tvTips.isHidden = false
                self.tvTips.reloadData()
            }
        }
        
        
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func updateImage(){
        switch FTDataManager.languageID {
        case "1":
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "english").withRenderingMode(.alwaysOriginal), style:.plain, target: self, action: #selector(changeLanguage))
            self.getData()
           
        case "2":
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "chinese").withRenderingMode(.alwaysOriginal), style:.plain, target: self, action: #selector(changeLanguage))
            self.getData()
          
        case "3":
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "japanese").withRenderingMode(.alwaysOriginal), style:.plain, target: self, action: #selector(changeLanguage))
            self.getData()
          
        default:
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "english").withRenderingMode(.alwaysOriginal), style:.plain, target: self, action: #selector(changeLanguage))
        }
    }
    @IBAction func changeLanguage() {
        let enAction = UIAlertAction(title: FTDataManager.getTranslationByLanguage(phrase: "English", langID: FTDataManager.languageID),
                                     style: .default) { (action) in
                                        FTDataManager.saveLanguage(langID: "1")
                                        self.updateImage()
                                        
        }
        let zhAction = UIAlertAction(title: FTDataManager.getTranslationByLanguage(phrase: "Chinese", langID: FTDataManager.languageID),
                                     style: .default) { (action) in
                                        FTDataManager.saveLanguage(langID: "2")
                                        self.updateImage()
        }
        let jpAction = UIAlertAction(title: FTDataManager.getTranslationByLanguage(phrase: "Japanese", langID: FTDataManager.languageID),
                                     style: .default) { (action) in
                                        FTDataManager.saveLanguage(langID: "3")
                                        self.updateImage()
                                        
                                        
        }
        let cancelAction = UIAlertAction(title: FTDataManager.getTBL(phrase: "Cancel"),
                                         style: .cancel) { (action) in
                                            // Respond to user selection of the action
        }
        
        var alert = UIAlertController(title: FTDataManager.getTBL(phrase: "SelectLanguage"),
                                      message: "",
                                      preferredStyle: .actionSheet)
        if(UIDevice.current.userInterfaceIdiom == .pad){
            alert = UIAlertController(title: FTDataManager.getTBL(phrase: "SelectLanguage"),
                                      message: "",
                                      preferredStyle:  .alert)
        }
        
        alert.addAction(enAction)
        alert.addAction(zhAction)
        alert.addAction(jpAction)
        
        alert.addAction(cancelAction)
        
        // On iPad, action sheets must be presented from a popover.
        //alert.popoverPresentationController?.barButtonItem = self.btnLanguage
        
        self.present(alert, animated: true) {
            // The alert was presented
        }
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension TipsVC: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "etqVC") as? PlacesViewController
        let backItem = UIBarButtonItem()
        backItem.title = ""
        navigationItem.backBarButtonItem = backItem
        self.navigationController?.pushViewController(controller!, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 10
    }
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
}
extension TipsVC: UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        if(segView.selectedSegmentIndex == 0){
            return arrEtq.count
        }
        else if(segView.selectedSegmentIndex == 1){
            return arrEtq.count
        }
        else{
            return arrPre.count
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "etqCell") as! FTTableViewCell
        
        if(segView.selectedSegmentIndex == 0){
            let etq = arrEtq[indexPath.section]
            cell.label1.text = etq.etiquetteTitle
            cell.label2.text = etq.etiquetteDescription
        }
        else if(segView.selectedSegmentIndex == 1){
            let phrase = arrPhrases[indexPath.section]
            cell.label1.text = phrase.phraseWord
            cell.label2.text = phrase.phraseTranslation
        }
        else{
            let pre = arrPre[indexPath.section]
            cell.label1.text = pre.preTitle
            cell.label2.text = pre.preDescription
        }
        
        return cell
    }
    
}
