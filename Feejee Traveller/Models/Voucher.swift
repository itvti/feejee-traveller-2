//
//  Voucher.swift
//  Feejee Traveller
//
//  Created by Vinay Prasad on 25/4/18.
//  Copyright © 2018 Vinay Prasad. All rights reserved.
//

import UIKit

struct Voucher {
    var id: String
    var voucherName: String
    var voucherLink: String
    var voucherStartDate: String
    var voucherEndDate: String
}


extension Voucher {
    init(){
        self.id = ""
        self.voucherLink = ""
        self.voucherName = ""
        self.voucherStartDate = ""
        self.voucherEndDate = ""
        }
}

