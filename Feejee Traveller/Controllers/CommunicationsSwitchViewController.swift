//
//  CommunicationsSwitchViewController.swift
//  Feejee Traveller
//
//  Created by Vinay Prasad on 6/9/18.
//  Copyright © 2018 Vinay Prasad. All rights reserved.
//

import UIKit

class CommunicationsSwitchViewController: UIViewController {
    @IBOutlet weak var cvOptions: UICollectionView!
    @IBOutlet weak var txIntro: UITextView!
    
    let optionsID = ["430", "426"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        // self.navigationController?.isNavigationBarHidden = false
        self.updateImage()
        
    }
    
    func getData(){
        self.title = FTDataManager.getTBL(phrase: "Communications").uppercased()
        txIntro.text = FTDataManager.getTBL(phrase: "CommunicationsIntro")
        
            cvOptions.delegate = self
            cvOptions.dataSource = self
            cvOptions.reloadData()
    }
    func updateImage(){
        switch FTDataManager.languageID {
        case "1":
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "english").withRenderingMode(.alwaysOriginal), style:.plain, target: self, action: #selector(changeLanguage))
            self.getData()
            
        case "2":
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "chinese").withRenderingMode(.alwaysOriginal), style:.plain, target: self, action: #selector(changeLanguage))
            self.getData()
            
        case "3":
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "japanese").withRenderingMode(.alwaysOriginal), style:.plain, target: self, action: #selector(changeLanguage))
            self.getData()
            
        default:
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "english").withRenderingMode(.alwaysOriginal), style:.plain, target: self, action: #selector(changeLanguage))
        }
    }
    @IBAction func changeLanguage() {
        let enAction = UIAlertAction(title: FTDataManager.getTranslationByLanguage(phrase: "English", langID: FTDataManager.languageID),
                                     style: .default) { (action) in
                                        FTDataManager.saveLanguage(langID: "1")
                                        self.updateImage()
                                        
        }
        let zhAction = UIAlertAction(title: FTDataManager.getTranslationByLanguage(phrase: "Chinese", langID: FTDataManager.languageID),
                                     style: .default) { (action) in
                                        FTDataManager.saveLanguage(langID: "2")
                                        self.updateImage()
        }
        let jpAction = UIAlertAction(title: FTDataManager.getTranslationByLanguage(phrase: "Japanese", langID: FTDataManager.languageID),
                                     style: .default) { (action) in
                                        FTDataManager.saveLanguage(langID: "3")
                                        self.updateImage()
                                        
                                        
        }
        let cancelAction = UIAlertAction(title: FTDataManager.getTBL(phrase: "Cancel"),
                                         style: .cancel) { (action) in
                                            // Respond to user selection of the action
        }
        
        var alert = UIAlertController(title: FTDataManager.getTranslationByLanguage(phrase: "SelectLanguage", langID: FTDataManager.languageID),
                                      message: "",
                                      preferredStyle: .actionSheet)
        if(UIDevice.current.userInterfaceIdiom == .pad){
            alert = UIAlertController(title: FTDataManager.getTranslationByLanguage(phrase: "SelectLanguage", langID: FTDataManager.languageID),
                                      message: "",
                                      preferredStyle:  .alert)
        }
        
        alert.addAction(enAction)
        alert.addAction(zhAction)
        alert.addAction(jpAction)
        
        alert.addAction(cancelAction)
        
        // On iPad, action sheets must be presented from a popover.
        //alert.popoverPresentationController?.barButtonItem = self.btnLanguage
        
        self.present(alert, animated: true) {
            // The alert was presented
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


extension CommunicationsSwitchViewController: UICollectionViewDelegate{
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        var storyboard = UIStoryboard(name: "Main_iPad", bundle: nil)
          if(UIDevice.current.userInterfaceIdiom == .phone){
            storyboard = UIStoryboard(name: "Main", bundle: nil)
        }
        let vf = FTDataManager.getPlacesDataByLanguageAndID(langID: FTDataManager.languageID, businessID: optionsID[indexPath.item])[0]
        let vfVC = storyboard.instantiateViewController(withIdentifier: "placesDVC") as? PlacesDetailViewController
        vfVC?.currentPlace = vf
        let backItem = UIBarButtonItem()
        backItem.title = ""
        navigationItem.backBarButtonItem = backItem
        self.navigationController?.pushViewController(vfVC!, animated: true)
        
    }
}

extension CommunicationsSwitchViewController: UICollectionViewDataSource{
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return optionsID.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "guideCell", for: indexPath) as! FTCollectionViewCell
        
       
        let vf = FTDataManager.getPlacesDataByLanguageAndID(langID: FTDataManager.languageID, businessID: optionsID[indexPath.item])[0]
        
        cell.label1.text = vf.bName
        let url = URL(string: vf.bImage)
        cell.imgView1.kf.setImage(with: url,  options: [.forceRefresh])
        
        return cell
    }
    
}

extension CommunicationsSwitchViewController: UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if(UIDevice.current.userInterfaceIdiom == .pad){
            let width = (cvOptions.frame.size.width - 40 ) / 2
            return CGSize(width: width , height: width * 0.75 )
        }
        else{
            let height = (cvOptions.frame.size.height - 40 ) / 2
        
            return CGSize(width: cvOptions.frame.size.width , height: height )
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        if(UIDevice.current.userInterfaceIdiom == .pad){
            let cellHeight = ((cvOptions.frame.size.width - 40 ) / 3 ) * 0.75
            let height = cvOptions.frame.size.height - (cellHeight * 2)
            
            return UIEdgeInsets(top: height / 3, left: 0, bottom: height / 3, right: 0)
        }
        else{
            let cellHeight = (cvOptions.frame.size.height - 40 ) / 2
            let height = cvOptions.frame.size.height - (cellHeight * 2)
            
            return UIEdgeInsets(top: height / 2, left: 0, bottom: height / 2, right: 0)
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        
        if(UIDevice.current.userInterfaceIdiom == .pad){
            let cellHeight = ((cvOptions.frame.size.width - 40 ) / 3 ) * 0.75
            let height = cvOptions.frame.size.height - (cellHeight * 2)
            return height / 3
            
        }
        else{
            let cellHeight = (cvOptions.frame.size.height - 40 ) / 2
            let height = cvOptions.frame.size.height - (cellHeight * 2)
            return height / 3
        }
    }
}

