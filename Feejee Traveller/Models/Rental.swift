//
//  Rental.swift
//  Feejee Traveller
//
//  Created by Vinay Prasad on 12/4/18.
//  Copyright © 2018 Vinay Prasad. All rights reserved.
//

import UIKit

struct Rental {
    var rentalID: String
    var rentalName: String
    var rentalModel: String
    var rentalFeatures: String
    var rentalDeal: String
    var rentalImage: String
    var rentalPrice: Double
    var rentalDiscount: Double
    var rentalExtraPrice: Double
    var rentalFormURL : String
    var rentalVideo: String
    var rentalAvailable: String
    var rentalLanguage: String
    var rentalLogo: String
}

extension Rental{
    init(){
        self.rentalID =  ""
        self.rentalName = ""
        self.rentalFeatures = ""
        self.rentalModel = ""
        self.rentalImage = ""
        self.rentalDeal = ""
        self.rentalVideo = ""
        self.rentalPrice = 0.0
        self.rentalDiscount = 0.0
        self.rentalExtraPrice = 0.0
        self.rentalFormURL = ""
        self.rentalAvailable = ""
        self.rentalLanguage = ""
        self.rentalLogo = ""
    }
}

