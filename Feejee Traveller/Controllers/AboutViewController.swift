//
//  AboutViewController.swift
//  Feejee Traveller
//
//  Created by Vinay Prasad on 11/4/18.
//  Copyright © 2018 Vinay Prasad. All rights reserved.
//

import UIKit
import MessageUI

class AboutViewController: UIViewController, MFMailComposeViewControllerDelegate {
    @IBOutlet weak var tvAbout: UITableView!
    @IBOutlet weak var lblVersion: UILabel!
    @IBOutlet weak var btnDeveloper: UIButton!
    @IBOutlet weak var btnLegal: UIButton!
    @IBOutlet weak var tvWidthConstant: NSLayoutConstraint!
    
    var about = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tvAbout.delegate = self
        tvAbout.dataSource = self
       
        
        tvAbout.estimatedRowHeight = 44.0
        tvAbout.rowHeight = UITableViewAutomaticDimension
        tvAbout.tableFooterView = UIView()
        
        let wd = self.view.frame.size.width - 30
        tvWidthConstant.constant = wd
        if(UIDevice.current.userInterfaceIdiom == .pad){
            tvWidthConstant.constant = wd * 0.7
        }
        self.view.layoutIfNeeded()
        // Do any additional setup after loading the view.
    }
    @IBAction func btnOpenFacebookTouched(_ sender: UIButton) {
        let fbURLWeb = URL(string: "https://www.facebook.com/feejeetraveller")
        let fbURLID =  URL(string: "fb://profile/946204472104523")
        
        if(UIApplication.shared.canOpenURL(fbURLID!)){
            // FB installed
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(fbURLID!, options: [:], completionHandler: nil)
            } else {
                // Fallback on earlier versions
            }
        } else {
            // FB is not installed, open in safari
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(fbURLWeb!, options: [:], completionHandler: nil)
            } else {
                // Fallback on earlier versions
            }
        }
    }
    @IBAction func btnDeveloperTouched(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "payVC") as? PaymentViewController
        controller?.webURL = "http://www.itvti.com.fj"
        
        
        let backItem = UIBarButtonItem()
        backItem.title = ""
        
        navigationItem.backBarButtonItem = backItem
        self.navigationController?.pushViewController(controller!, animated: true)
    }
    
    @IBAction func btnLegalTouched(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "legalVC") as? LegalVC
        
        
        
        let backItem = UIBarButtonItem()
        backItem.title = ""
        
        navigationItem.backBarButtonItem = backItem
        self.navigationController?.pushViewController(controller!, animated: true)
        
    }
    @IBAction func btnContactUsTouched(_ sender: UIButton) {
        //  let indexPath = tvDetails .indexPath(for: sender.superview?.superview as! UITableViewCell)
        
        // let email = currentPlace.bBranch[(indexPath?.row)!]
        //TODO add business phone num here
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients(["res@feejeetraveller.com"])
            mail.setMessageBody("<p>Bula!</p>", isHTML: true)
            
            present(mail, animated: true)
        } else {
            // show fail alert
            let alert = UIAlertController(title: "Alert", message: FTDataManager.getTBL(phrase: "NoEmail"), preferredStyle: UIAlertControllerStyle.alert)
            let okAction = UIAlertAction(title: FTDataManager.getTBL(phrase: "Cancel"),
                                             style: .default) { (action) in
                                                // Respond to user selection of the action
            } 
            alert.addAction(okAction)
            self.present(alert, animated: true, completion: nil)
            //
        }
    }
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        self.dismiss(animated: true, completion: nil)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
        
        
        self.updateImage()
        
        if(UIDevice.current.userInterfaceIdiom == .pad){
            
        }
        
    }
    

    
    func updateImage(){
        self.title = FTDataManager.getTBL(phrase: "About").uppercased()
        btnDeveloper.setTitle(FTDataManager.getTBL(phrase: "Developer"), for: .normal)
        
        btnLegal.setTitle(FTDataManager.getTBL(phrase: "Legal"), for: .normal)
        lblVersion.text = FTDataManager.getTBL(phrase: "Version") + " 2.2 (998.25)"
        
        switch FTDataManager.languageID {
        case "1":
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "english").withRenderingMode(.alwaysOriginal), style:.plain, target: self, action: #selector(changeLanguage))
        
            self.tvAbout.reloadData()
            
        case "2":
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "chinese").withRenderingMode(.alwaysOriginal), style:.plain, target: self, action: #selector(changeLanguage))
          
            self.tvAbout.reloadData()
        case "3":
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "japanese").withRenderingMode(.alwaysOriginal), style:.plain, target: self, action: #selector(changeLanguage))
         
            self.tvAbout.reloadData()
        default:
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "english").withRenderingMode(.alwaysOriginal), style:.plain, target: self, action: #selector(changeLanguage))
        }
    }
    @IBAction func changeLanguage() {
        let enAction = UIAlertAction(title: FTDataManager.getTranslationByLanguage(phrase: "English", langID: FTDataManager.languageID),
                                     style: .default) { (action) in
                                        FTDataManager.saveLanguage(langID: "1")
                                        self.updateImage()
                                        
        }
        let zhAction = UIAlertAction(title: FTDataManager.getTranslationByLanguage(phrase: "Chinese", langID: FTDataManager.languageID),
                                     style: .default) { (action) in
                                        FTDataManager.saveLanguage(langID: "2")
                                        self.updateImage()
        }
        let jpAction = UIAlertAction(title: FTDataManager.getTranslationByLanguage(phrase: "Japanese", langID: FTDataManager.languageID),
                                     style: .default) { (action) in
                                        FTDataManager.saveLanguage(langID: "3")
                                        self.updateImage()
                                        
                                        
        }
        let cancelAction = UIAlertAction(title: FTDataManager.getTBL(phrase: "Cancel"),
                                         style: .cancel) { (action) in
                                            // Respond to user selection of the action
        }
        
        var alert = UIAlertController(title: FTDataManager.getTBL(phrase: "SelectLanguage"),
                                      message: "",
                                      preferredStyle: .actionSheet)
        if(UIDevice.current.userInterfaceIdiom == .pad){
            alert = UIAlertController(title: FTDataManager.getTBL(phrase: "SelectLanguage"),
                                      message: "",
                                      preferredStyle:  .alert)
        }
        
        alert.addAction(enAction)
        alert.addAction(zhAction)
        alert.addAction(jpAction)
        
        alert.addAction(cancelAction)
        
        // On iPad, action sheets must be presented from a popover.
        //alert.popoverPresentationController?.barButtonItem = self.btnLanguage
        
        self.present(alert, animated: true) {
            // The alert was presented
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension AboutViewController: UITableViewDelegate{
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let storyboard = UIStoryboard(name: "Main", bundle: nil)
//        var controller = storyboard.instantiateInitialViewController()
//        let backItem = UIBarButtonItem()
//        backItem.title = ""
//        navigationItem.backBarButtonItem = backItem
//        self.navigationController?.pushViewController(controller!, animated: true)
//    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 5
    }
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
}
extension AboutViewController: UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var cell = tableView.dequeueReusableCell(withIdentifier: "logoCell") as! FTTableViewCell
        if(indexPath.section == 0){
            cell.imgView1.image = #imageLiteral(resourceName: "DF-Logo-Black-Wide")
            //cell.imgView1.backgroundColor = .clear
            
            return cell
        }
       
        else if(indexPath.section == 1){
            cell = tableView.dequeueReusableCell(withIdentifier: "detailCell") as! FTTableViewCell
            
            cell.label1.text = ""
            if(about.isEmpty){
                cell.label2.text = FTDataManager.getTBL(phrase: "AboutIntro")
            }
            else{
                cell.label2.text = FTDataManager.getTBL(phrase: "Bula")
            }
            return cell
        }
      
        else{
            var cell = tableView.dequeueReusableCell(withIdentifier: "tripCell") as! FTTableViewCell

            cell.imgView1.image = #imageLiteral(resourceName: "tripadvisor")
         
            return cell
        }

        
    }
    
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
//        return 80
//    }
    
    
}


