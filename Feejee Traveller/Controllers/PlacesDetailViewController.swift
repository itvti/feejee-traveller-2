//
//  PlacesDetailViewController.swift
//  Feejee Traveller
//
//  Created by Vinay Prasad on 28/3/18.
//  Copyright © 2018 Vinay Prasad. All rights reserved.
//

import UIKit
import Alamofire
import ImageSlideshow
import MessageUI
import MapKit

class PlacesDetailViewController: UIViewController, MFMailComposeViewControllerDelegate {
    @IBOutlet weak var tvDetails: UITableView!
    @IBOutlet weak var cvGallery: UICollectionView!
    @IBOutlet weak var btnBookNow: UIButton!
    @IBOutlet weak var btnBookHeightConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var imgBanner: UIImageView!
    @IBOutlet weak var lblBanner: UILabel!
    
    var currentPlace = Business()
    var sectionsCount = 2
    var arrImages = [String]()
    var arrFields = [String]()
    var HUD = LottieHUD("spinner")
    var arrAnnots : [MKAnnotation] = []
    override func viewDidLoad() {
        super.viewDidLoad()
        
        HUD.contentMode = .scaleAspectFill
        HUD.size = CGSize(width: 50, height: 50)
      
        // Do any additional setup after loading the view.
        tvDetails.delegate = self
        tvDetails.dataSource = self
        
        tvDetails.estimatedRowHeight = 100.00
        tvDetails.rowHeight = UITableViewAutomaticDimension
        
      //  self.updateImage()
        
    }
    override func viewDidAppear(_ animated: Bool) {
     
          HUD.showHUD()
        updateImage()
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func getData(){
        currentPlace = FTDataManager.getPlacesDataByLanguageAndID(langID: FTDataManager.languageID, businessID: currentPlace.id)[0]
        
        sortData()

        getGalleryImages()
    }
    func sortData(){
        if(currentPlace.bVoucher.voucherLink.isEmpty){
            
            btnBookNow.isHidden =  true
            DispatchQueue.main.async {
                self.btnBookHeightConstraint.constant = 0
                self.view.layoutIfNeeded()
                
                if(UIDevice.current.userInterfaceIdiom == .pad){
                    self.imgBanner.isHidden = true
                    self.lblBanner.isHidden = true
                }
            }
        }
        else{
            if(UIDevice.current.userInterfaceIdiom == .pad){
                imgBanner.isHidden = false
                lblBanner.isHidden = false
                lblBanner.text = FTDataManager.getTBL(phrase: "RedeemVoucher")
            }
            btnBookNow.isHidden = false
            
            
            btnBookHeightConstraint.constant = 47
            self.view.layoutIfNeeded()
            
        }
        
        DispatchQueue.global(qos: .userInitiated).async {
            self.sectionsCount = 2
            self.title = self.currentPlace.bName.uppercased()
            self.arrFields = [String]()
            
            if(!self.currentPlace.bDesc.isEmpty){
                self.arrFields.append("About")
            }
            if(!self.currentPlace.bService.isEmpty){
                self.arrFields.append("Service")
            }
            if(!self.currentPlace.bHours.isEmpty){
                self.arrFields.append("Hours")
            }
            if(!self.currentPlace.bFeature.isEmpty){
                self.arrFields.append("Feature")
            }
            if(!self.currentPlace.bWebsite.isEmpty){
                self.arrFields.append("Website")
            }
            if(!self.currentPlace.bEmail.isEmpty){
                self.arrFields.append("Email")
            }
            if(!self.currentPlace.bPhone.isEmpty){
                self.arrFields.append("Phone")
            }
            if(!self.currentPlace.bFacebook.isEmpty){
                self.arrFields.append("Facebook")
            }
            if(!self.currentPlace.bTwitter.isEmpty){
                self.arrFields.append("Twitter")
            }
            
            if(self.currentPlace.bListingType == "2" && self.currentPlace.bBranch.count > 0){
                self.sectionsCount = self.sectionsCount + 1
            }
            
            // show map if its upgraded business
            if(self.currentPlace.bListingType == "2"){
                self.sectionsCount = self.sectionsCount + 1
            }
            
            for branch in self.currentPlace.bBranch {
                let annotation = MKPointAnnotation()
                annotation.title = branch.bName + "-" + branch.bLocation
                annotation.subtitle = branch.bAddress
                
                if(Double(branch.bLatitude) != 0.0 || Double(branch.bLongitude) != 0.0){
                    annotation.coordinate = CLLocationCoordinate2D(latitude: Double(branch.bLatitude)!, longitude: Double(branch.bLongitude)!)
                    self.arrAnnots.append(annotation)
                    
                }
                
            }
            
            
            
            DispatchQueue.main.async {
                self.btnBookNow.setTitle(FTDataManager.getTBL(phrase: "DownloadVoucher"), for: .normal)
                self.tvDetails.isHidden = false
                self.tvDetails.reloadData()
                 self.HUD.stopHUD()
            }
        }
     
     
    }
    func getGalleryImages(){
        do{
            let imagesURL = URL(string:"\(FTDataManager.homeURL)images/business.php?id=\(currentPlace.id)")
            let strURL = try String(contentsOf: imagesURL!)
            FTDataManager.logStats(id: currentPlace.id, table: "business", type: "view")
            arrImages = strURL.trimmingCharacters(in: .whitespacesAndNewlines).components(separatedBy: "*")
            
            arrImages.removeLast()
           
            if(arrImages.count > 0 && UIDevice.current.userInterfaceIdiom == .pad){
                cvGallery.dataSource = self
                
                cvGallery.reloadData()
            }
        }
        catch{}
    }
    func updateImage(){
        self.title = currentPlace.bName.uppercased()
       
        
        switch FTDataManager.languageID {
        case "1":
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "english").withRenderingMode(.alwaysOriginal), style:.plain, target: self, action: #selector(changeLanguage))
            self.getData()
           // self.tvDetails.reloadData()
        case "2":
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "chinese").withRenderingMode(.alwaysOriginal), style:.plain, target: self, action: #selector(changeLanguage))
            self.getData()
            
        case "3":
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "japanese").withRenderingMode(.alwaysOriginal), style:.plain, target: self, action: #selector(changeLanguage))
            self.getData()
          //  self.tvDetails.reloadData()
        default:
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "english").withRenderingMode(.alwaysOriginal), style:.plain, target: self, action: #selector(changeLanguage))
        }
    }
    @IBAction func changeLanguage() {
        let enAction = UIAlertAction(title: FTDataManager.getTBL(phrase: "English"),
                                     style: .default) { (action) in
                                        FTDataManager.saveLanguage(langID: "1")
                                        self.updateImage()
                                        
        }
        let zhAction = UIAlertAction(title: FTDataManager.getTBL(phrase: "Chinese"),
                                     style: .default) { (action) in
                                        FTDataManager.saveLanguage(langID: "2")
                                        self.updateImage()
        }
        let jpAction = UIAlertAction(title: FTDataManager.getTBL(phrase: "Japanese"),
                                     style: .default) { (action) in
                                        FTDataManager.saveLanguage(langID: "3")
                                        self.updateImage()
                                        
                                        
        }
        let cancelAction = UIAlertAction(title: FTDataManager.getTBL(phrase: "Cancel"),
                                         style: .cancel) { (action) in
                                            // Respond to user selection of the action
        }
        
        var alert = UIAlertController(title: FTDataManager.getTBL(phrase: "SelectLanguage"),
                                      message: "",
                                      preferredStyle: .actionSheet)
        if(UIDevice.current.userInterfaceIdiom == .pad){
            alert = UIAlertController(title: FTDataManager.getTBL(phrase: "SelectLanguage"),
                                      message: "",
                                      preferredStyle:  .alert)
        }
        
        alert.addAction(enAction)
        alert.addAction(zhAction)
        alert.addAction(jpAction)
        
        alert.addAction(cancelAction)
        
        // On iPad, action sheets must be presented from a popover.
        //alert.popoverPresentationController?.barButtonItem = self.btnLanguage
        
        self.present(alert, animated: true) {
            // The alert was presented
        }
    }
  
    @IBAction func btnBookNowTouched(_ sender: Any) {
        FTDataManager.logStats(id: currentPlace.id, table: "business", type: "voucher")
        
        if(!currentPlace.bVoucher.voucherLink.isEmpty){
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "payVC") as? PaymentViewController
            controller?.webURL = currentPlace.bVoucher.voucherLink
            controller?.titleTag = currentPlace.bVoucher.voucherName
            controller?.isDownload = true
            let backItem = UIBarButtonItem()
            backItem.title = ""
            
            navigationItem.backBarButtonItem = backItem
            self.navigationController?.pushViewController(controller!, animated: true)
        }
    }
    @objc private func call(_ sender: UIButton) {
        //let indexPath = tvDetails.indexPath(for: sender.superview?.superview as! FTTableViewCell)
        
       // let branch = currentPlace.bBranch[(indexPath?.row)!]
        //TODO add business phone num here
        if let phoneCallURL = URL(string: "tel://\(sender.tag)") {
            let application:UIApplication = UIApplication.shared
            if (application.canOpenURL(phoneCallURL)) {
                if #available(iOS 10.0, *) {
                    application.open(phoneCallURL, options: [:], completionHandler: nil)
                } else {
                    // Fallback on earlier versions
                }
            }
            else{
                // show error
            }
        }
    }
    @objc private func mail(_ sender: UIButton) {
      //  let indexPath = tvDetails .indexPath(for: sender.superview?.superview as! UITableViewCell)
        
       // let email = currentPlace.bBranch[(indexPath?.row)!]
        //TODO add business phone num here
        if MFMailComposeViewController.canSendMail() {
            let mail = MFMailComposeViewController()
            mail.mailComposeDelegate = self
            mail.setToRecipients([currentPlace.bEmail])
            mail.setMessageBody("<p>Bula!</p>", isHTML: true)
            
            present(mail, animated: true)
        } else {
            // show failure alert
        }
    }
    
    func setMapData(sender: MKMapView){
        for branch in currentPlace.bBranch {
            let annotation = MKPointAnnotation()
            annotation.title = branch.bName + "-" + branch.bLocation
            annotation.subtitle = branch.bAddress
            
            if(Double(branch.bLatitude) != 0.0 || Double(branch.bLongitude) != 0.0){
                annotation.coordinate = CLLocationCoordinate2D(latitude: Double(branch.bLatitude)!, longitude: Double(branch.bLongitude)!)
                sender.addAnnotation(annotation)
            
            }
           
        }
        
        let location = CLLocation(latitude: -17.8195119, longitude: 177.8905473)
        
        sender.zoom(toCenterCoordinate: location.coordinate, zoomLevel: 7)
    }
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true)
    }
    @objc private func openLink(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "payVC") as? PaymentViewController
        controller?.webURL =  currentPlace.bWebsite
       
        let backItem = UIBarButtonItem()
        backItem.title = ""
        
        navigationItem.backBarButtonItem = backItem
        self.navigationController?.pushViewController(controller!, animated: true)
    }
    @objc private func openFeature(_ sender: UIButton) {
        FTDataManager.logStats(id: currentPlace.id, table: "business", type: "feature")
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller = storyboard.instantiateViewController(withIdentifier: "payVC") as? PaymentViewController
        controller?.webURL =  String(format:"\(FTDataManager.homeURL)doc/%@/%@",currentPlace.id,currentPlace.bFeature)
        let backItem = UIBarButtonItem()
        backItem.title = ""
        
        navigationItem.backBarButtonItem = backItem
        self.navigationController?.pushViewController(controller!, animated: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
extension PlacesDetailViewController: UITableViewDelegate{
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(indexPath.section == 0){
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "placesVC") as? PlacesViewController
            let backItem = UIBarButtonItem()
            backItem.title = ""
            navigationItem.backBarButtonItem = backItem
            self.navigationController?.pushViewController(controller!, animated: true)
        }
        
    }
   
    
}
extension PlacesDetailViewController: UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return sectionsCount
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if(section==0){
             if(UIDevice.current.userInterfaceIdiom == .pad){
                  return 0}
             else{return 1}
        }
        else if(section == 1) {return arrFields.count }
        else if(section == 2) {
            return currentPlace.bBranch.count
        }
        else{
            return 1
        }
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let cell = tableView.dequeueReusableCell(withIdentifier: "headerCell") as! FTTableViewCell
        cell.backgroundColor = UIColor.white
        if(section == 0){}
        else if(section == 1){
            cell.label1.text = FTDataManager.getTBL(phrase: "About").uppercased()
        }
       
        else if(section == 2){
            cell.label1.text = FTDataManager.getTBL(phrase: "Locations").uppercased()
            
        }
        else{}
        
        return cell
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(section == 2) {return 40}
        else{ return 0}
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 6
    }
    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = #colorLiteral(red: 0.8039215803, green: 0.8039215803, blue: 0.8039215803, alpha: 1)
        return headerView
    }
  
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cellIdentifier = "imageCell"
        var cell = tableView.dequeueReusableCell(withIdentifier:cellIdentifier) as! FTTableViewCell
       
        if cell == nil{
            cell = tableView.dequeueReusableCell(withIdentifier: "imageCell") as! FTTableViewCell
        }
        if(indexPath.section == 0){
            cell = tableView.dequeueReusableCell(withIdentifier: "imageCell") as! FTTableViewCell
          //  cell.layer.cornerRadius = 0.0
                
//            cell.slideShow.pageControl.currentPageIndicatorTintColor = UIColor.black
//            cell.slideShow.pageControl.pageIndicatorTintColor = UIColor.lightGray

            var imgURL = [AlamofireSource]()
            for url in arrImages{
                imgURL.append(AlamofireSource(urlString:"\(FTDataManager.homeURL)images/business/\(currentPlace.id)/\(url)")!)
            }
            cell.slideShow.setImageInputs(imgURL)
            
            if(currentPlace.bVoucher.voucherLink.isEmpty){
                cell.imgView1.isHidden = true
                cell.label1.isHidden = true
            }
            else{
                cell.imgView1.isHidden = false
                cell.label1.isHidden = false
                cell.label1.text = FTDataManager.getTBL(phrase: "RedeemVoucher")
            }
           
            return cell
        }
        else if(indexPath.section == 1){
            cell = tableView.dequeueReusableCell(withIdentifier: "detailCell") as! FTTableViewCell
            
            switch arrFields[indexPath.row] {
            case "About":
                cell.label1.text = FTDataManager.getTBL(phrase: "Description").uppercased()
                cell.label2.text = currentPlace.bDesc
            
            case "Service":
                cell.label1.text = FTDataManager.getTBL(phrase: "Services").uppercased()
                cell.label2.text = currentPlace.bService
            case "Hours":
                cell.label1.text = FTDataManager.getTBL(phrase: "Hours").uppercased()
                cell.label2.text = currentPlace.bHours
            case "Feature":
                cell = tableView.dequeueReusableCell(withIdentifier: "featureCell") as! FTTableViewCell
                cell.label1.text = FTDataManager.getTBL(phrase: "Featured").uppercased()
                cell.label2.text = currentPlace.bFeatureName
                
                cell.btn1.addTarget(self, action: #selector(openFeature(_:)), for: .touchUpInside)
                cell.btn1.setTitle( FTDataManager.getTBL(phrase: "View"), for: .normal)
            case "Website":
                cell = tableView.dequeueReusableCell(withIdentifier: "websiteCell") as! FTTableViewCell
                cell.label1.text = FTDataManager.getTBL(phrase: "Website").uppercased()
         //       cell.label2.text = currentPlace.bWebsite
                cell.btn1.addTarget(self, action: #selector(openLink(_:)), for: .touchUpInside)
           //     cell.btn1.setTitle(FTDataManager.getTBL(phrase: "Visit"), for: .normal)
            case "Email":
                cell = tableView.dequeueReusableCell(withIdentifier: "websiteCell") as! FTTableViewCell
                cell.label1.text = FTDataManager.getTBL(phrase: "Email").uppercased()
            //    cell.label2.text = currentPlace.bEmail
        //        cell.btn1.setTitle(FTDataManager.getTBL(phrase: "Message"), for: .normal)
                cell.btn1.addTarget(self, action: #selector(mail(_:)), for: .touchUpInside)
            case "Phone":
                cell = tableView.dequeueReusableCell(withIdentifier: "linkCell") as! FTTableViewCell
                cell.label1.text = FTDataManager.getTBL(phrase: "Phone").uppercased()
                if(!currentPlace.bPhone.isEmpty){
                    cell.btn1.tag = Int(currentPlace.bPhone.replacingOccurrences(of: " ", with: ""))!
                    cell.btn1.setTitle(currentPlace.bPhone, for: .normal)
                    cell.btn1.addTarget(self, action: #selector(call(_:)), for: .touchUpInside)
                }
                else{
                    cell.btn1.setTitle("", for: .normal)
                }
           
            case "Type":
                cell.label1.text = FTDataManager.getTBL(phrase: "Description").uppercased()
                cell.label2.text = currentPlace.bType
            default:
                cell.label1.text = FTDataManager.getTBL(phrase: "About").uppercased()
                cell.label2.text = currentPlace.bDesc
            }

            return cell
        }
        
        else if(indexPath.section == 2){
            // list branches here
            cell = tableView.dequeueReusableCell(withIdentifier: "branchCell") as! FTTableViewCell
            
            let branch = currentPlace.bBranch[indexPath.row]
            
            //cell.label1.text = branch.bLocation
            cell.label2.text = branch.bAddress
            if(!branch.bPhone.isEmpty){
                let phone = branch.bPhone.replacingOccurrences(of: " ", with: "")
                cell.btn1.tag = Int(phone)!
                cell.btn1.setTitle(branch.bPhone, for: .normal)
                cell.btn1.addTarget(self, action: #selector(call(_:)), for: .touchUpInside)
            }
            else{
                cell.btn1.setTitle("", for: .normal)
            }
            return cell
        }
        else{
            cell = tableView.dequeueReusableCell(withIdentifier: "mapCell") as! FTTableViewCell
//            for branch in currentPlace.bBranch {
//                let annotation = MKPointAnnotation()
//                annotation.title = branch.bName + "-" + branch.bLocation
//                annotation.subtitle = branch.bAddress
//
//                if(Double(branch.bLatitude) != 0.0 || Double(branch.bLongitude) != 0.0){
//                    annotation.coordinate = CLLocationCoordinate2D(latitude: Double(branch.bLatitude)!, longitude: Double(branch.bLongitude)!)
//                    cell.mapView.addAnnotation(annotation)
//                }
//                else{
//
//                }
//            }
            cell.mapView.addAnnotations(arrAnnots)
            let location = CLLocation(latitude: -17.8195119, longitude: 177.8905473)

            cell.mapView.zoom(toCenterCoordinate: location.coordinate, zoomLevel: 7)

            
            
            return cell
        }
    }
}

extension PlacesDetailViewController: UICollectionViewDataSource{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrImages.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imageCell", for: indexPath) as! FTCollectionViewCell
        let path = arrImages[indexPath.item]
        let url = URL(string:"\(FTDataManager.homeURL)images/business/\(currentPlace.id)/\(path)")
        
        cell.imgView1.kf.setImage(with: url)
        
        return cell
    }
    
    
}

extension MKMapView {
    
    var MERCATOR_OFFSET : Double {
        return 268435456.0
    }
    
    var MERCATOR_RADIUS : Double  {
        return 85445659.44705395
    }
    
    private func longitudeToPixelSpaceX(longitude: Double) -> Double {
        return round(MERCATOR_OFFSET + MERCATOR_RADIUS * longitude * Double.pi / 180.0)
    }
    
    private func latitudeToPixelSpaceY(latitude: Double) -> Double {
        return round(MERCATOR_OFFSET - MERCATOR_RADIUS * log((1 + sin(latitude * Double.pi / 180.0)) / (1 - sin(latitude * Double.pi / 180.0))) / 2.0)
    }
    
    private  func pixelSpaceXToLongitude(pixelX: Double) -> Double {
        return ((round(pixelX) - MERCATOR_OFFSET) / MERCATOR_RADIUS) * 180.0 / Double.pi;
    }
    
    private func pixelSpaceYToLatitude(pixelY: Double) -> Double {
        return (Double.pi / 2.0 - 2.0 * atan(exp((round(pixelY) - MERCATOR_OFFSET) / MERCATOR_RADIUS))) * 180.0 / Double.pi;
    }
    
    private func coordinateSpan(withMapView mapView: MKMapView, centerCoordinate: CLLocationCoordinate2D, zoomLevel: UInt) ->MKCoordinateSpan {
        let centerPixelX = longitudeToPixelSpaceX(longitude: centerCoordinate.longitude)
        let centerPixelY = latitudeToPixelSpaceY(latitude: centerCoordinate.latitude)
        
        let zoomExponent = Double(20 - zoomLevel)
        let zoomScale = pow(2.0, zoomExponent)
        
        let mapSizeInPixels = mapView.bounds.size
        let scaledMapWidth =  Double(mapSizeInPixels.width) * zoomScale
        let scaledMapHeight = Double(mapSizeInPixels.height) * zoomScale
        
        let topLeftPixelX = centerPixelX - (scaledMapWidth / 2);
        let topLeftPixelY = centerPixelY - (scaledMapHeight / 2);
        
        // find delta between left and right longitudes
        let minLng = pixelSpaceXToLongitude(pixelX: topLeftPixelX)
        let maxLng = pixelSpaceXToLongitude(pixelX: topLeftPixelX + scaledMapWidth)
        let longitudeDelta = maxLng - minLng;
        
        let minLat = pixelSpaceYToLatitude(pixelY: topLeftPixelY)
        let maxLat = pixelSpaceYToLatitude(pixelY: topLeftPixelY + scaledMapHeight)
        let latitudeDelta = -1 * (maxLat - minLat);
        
        let span = MKCoordinateSpanMake(latitudeDelta, longitudeDelta)
        return span
    }
    
    func zoom(toCenterCoordinate centerCoordinate:CLLocationCoordinate2D, zoomLevel: UInt) {
        let zoomLevel = min(zoomLevel, 20)
        let span = self.coordinateSpan(withMapView: self, centerCoordinate: centerCoordinate, zoomLevel: zoomLevel)
        let region = MKCoordinateRegionMake(centerCoordinate, span)
        self.setRegion(region, animated: true)
        
    }
}
