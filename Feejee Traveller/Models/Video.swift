//
//  Video.swift
//  Feejee Traveller
//
//  Created by Vinay Prasad on 28/5/18.
//  Copyright © 2018 Vinay Prasad. All rights reserved.
//

import UIKit

struct Video {
    var videoID: String
    var videoName: String
    var videoImage: String
    var videoURL: String
    var videoLangID: String
}

extension Video{
    init(){
        self.videoID = ""
        self.videoName = ""
        self.videoImage = ""
        self.videoURL = ""
        self.videoLangID = ""
    }
}
