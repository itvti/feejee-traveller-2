//
//  PaddedLabel.swift
//  Feejee Traveller
//
//  Created by Vinay Prasad on 23/6/18.
//  Copyright © 2018 Vinay Prasad. All rights reserved.
//

import UIKit

class PaddedLabel: UILabel {

    let padding = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
    
    override func drawText(in rect: CGRect) {
        super.drawText(in: UIEdgeInsetsInsetRect(rect, padding))
        super.layer.cornerRadius = 4.0
        super.layer.borderColor = UIColor.clear.cgColor
        super.layer.borderWidth = 1.0
        super.layer.masksToBounds = true
    }
    // Override -intrinsicContentSize: for Auto layout code
//    func intrinsicContentSize() -> CGSize {
//        let superContentSize = super.intrinsicContentSize
//        let width = superContentSize.width + padding.left + padding.right
//        let heigth = superContentSize.height + padding.top + padding.bottom
//        return CGSize(width: width, height: heigth)
//    }
    override public var intrinsicContentSize: CGSize {
        let superContentSize = super.intrinsicContentSize
        let width = superContentSize.width + padding.left + padding.right
        let heigth = superContentSize.height + padding.top + padding.bottom
        return CGSize(width: width, height: heigth)
    }
    // Override -sizeThatFits: for Springs & Struts code
    override func sizeThatFits(_ size: CGSize) -> CGSize {
        let superSizeThatFits = super.sizeThatFits(size)
        let width = superSizeThatFits.width + padding.left + padding.right
        let heigth = superSizeThatFits.height + padding.top + padding.bottom
        return CGSize(width: width, height: heigth)
    }

}
