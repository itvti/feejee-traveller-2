//
//  PaymentViewController.swift
//  Feejee Traveller
//
//  Created by Vinay Prasad on 3/4/18.
//  Copyright © 2018 Vinay Prasad. All rights reserved.
//

import UIKit
import WebKit
import LinearProgressBarMaterial
import Alamofire

class PaymentViewController: UIViewController, WKNavigationDelegate, WKUIDelegate, UIWebViewDelegate {
    var webURL : String = ""
    var webURL2 = URL(string: "")
    var titleTag : String = ""
    var isDownload: Bool = false
    
    var pvView: WKWebView!
    var pView: UIWebView!
    var HUD = LottieHUD("spinner")
    var docController:UIDocumentInteractionController!
    
    
    @IBOutlet weak var backButton: UIBarButtonItem!
    @IBOutlet weak var forwardButton: UIBarButtonItem!
    @IBOutlet weak var reloadButton: UIBarButtonItem!
    @IBOutlet weak var progressView: UIProgressView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        HUD.contentMode = .scaleAspectFill
        HUD.size = CGSize(width: 50, height: 50)

    }
    
    override func loadView() {
        let os = ProcessInfo().operatingSystemVersion
        if(os.majorVersion > 10){
            super.loadView()
            let webConfiguration = WKWebViewConfiguration()
            pvView = WKWebView(frame: .zero, configuration: webConfiguration)
            pvView.frame  = CGRect(x: 0, y: 66, width: UIScreen.main.bounds.width, height:  (UIScreen.main.bounds.height - 110))
            pvView.navigationDelegate = self
            pvView.uiDelegate = self
            view.addSubview(pvView)
        }
        else{
            super.loadView()
            pView    = UIWebView()
            pView.frame  = CGRect(x: 0, y: 66, width: UIScreen.main.bounds.width, height: (UIScreen.main.bounds.height - 110))
      //      webV.loadRequest(NSURLRequest(url: NSURL(string: "https://www.apple.com")! as URL) as URLRequest)
            pView.delegate = self
            view.addSubview(pView)
        }
    }
    @objc func downloadFile(){
        guard var stringPath = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true).first else {return}
        stringPath = stringPath + "Vouchers"
          let fileURL = URL(string: webURL)
        // New Folder is your folder name
        if !FileManager.default.fileExists(atPath: stringPath) {
            do {
                try FileManager.default.createDirectory(atPath: stringPath, withIntermediateDirectories: false, attributes: nil)
            } catch let error {
    
                return
            }
        }
        do{
        let fileManager = FileManager.default
        let url =  try fileManager.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
        let jsonURL = url.appendingPathComponent("places.pdf")
        
        let data =  try Data(contentsOf: fileURL!)
        try data.write(to: jsonURL)
       
        }catch{}
    }
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        self.title = titleTag.uppercased()
    
        let url = URL(string: webURL)
        
        let os = ProcessInfo().operatingSystemVersion
        if(os.majorVersion > 10){

            pvView.navigationDelegate = self
            pvView.contentMode = UIViewContentMode.scaleAspectFit
            
            
            if(webURL.isEmpty){
                pvView.load(URLRequest(url: webURL2!))
            }
            else{
                pvView.load(URLRequest(url: url!))
            }
        }
        else{
            if(webURL.isEmpty){
                pView.loadRequest(URLRequest(url: webURL2!))
            }
            else{
                pView.loadRequest(URLRequest(url: url!))
            }
            
            
        }
        if(isDownload){
            let downloadButton = UIBarButtonItem(image: #imageLiteral(resourceName: "download").withRenderingMode(.alwaysOriginal), style:.plain, target: self, action: #selector(shareBtn(_:)))
            
            self.navigationItem.rightBarButtonItems = [downloadButton]

        }
        
     
    }
    @IBAction func shareBtn(_ sender: AnyObject) {
        var localPath: NSURL?
        let url = URL(string: webURL)
        Alamofire.download(url!, method: .get, parameters: nil, headers: nil) { (tempUrl, response)  in
            
            let directoryURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)[0]
            let pathComponent = response.suggestedFilename
            
            localPath = directoryURL.appendingPathComponent(pathComponent!) as NSURL?
            return (destinationURL: localPath as! URL, options: .removePreviousFile)
            
            
            }.response { response in
                if localPath != nil{
                    
                    
                    self.docController = UIDocumentInteractionController(url: localPath! as URL)
                    self.docController.presentOptionsMenu(from: sender as! UIBarButtonItem, animated: true)
                    
                }
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        HUD.stopHUD()
      
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        HUD.showHUD()
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        HUD.stopHUD()
    }
    
    func webView(_ webView: WKWebView, createWebViewWith configuration: WKWebViewConfiguration, for navigationAction: WKNavigationAction, windowFeatures: WKWindowFeatures) -> WKWebView? {
        
        if(navigationAction.targetFrame == nil || !navigationAction.targetFrame!.isMainFrame){
            webView.load(navigationAction.request)
        }
        
        return nil
    }

    @IBAction func back(sender: UIBarButtonItem) {
        let os = ProcessInfo().operatingSystemVersion
        if(os.majorVersion > 10){
            if (self.pvView.canGoBack) {
                self.pvView.goBack()
            }
        }
        else{
            if (self.pView.canGoBack) {
                self.pView.goBack()
            }
        }
    }
    
    @IBAction func forward(sender: UIBarButtonItem) {
        let os = ProcessInfo().operatingSystemVersion
        if(os.majorVersion > 10){
            if (self.pvView.canGoForward) {
                self.pvView.goForward()
            }
        }
        else{
            if (self.pView.canGoForward) {
                self.pView.goForward()
            }
        }
    }
    
    @IBAction func stop(sender: UIBarButtonItem) {
        let os = ProcessInfo().operatingSystemVersion
        if(os.majorVersion > 10){
            pvView.stopLoading()
        }
        else{
            pView.stopLoading()
        }
        
   
    }
    
    @IBAction func reload(sender: UIBarButtonItem) {
        let os = ProcessInfo().operatingSystemVersion
        if(os.majorVersion > 10){
            pvView.reload()
        }
        else{
            pView.reload()
        }
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


extension UIColor {
    convenience init(hex: String) {
        let scanner = Scanner(string: hex)
        scanner.scanLocation = 0
        
        var rgbValue: UInt64 = 0
        
        scanner.scanHexInt64(&rgbValue)
        
        let r = (rgbValue & 0xff0000) >> 16
        let g = (rgbValue & 0xff00) >> 8
        let b = rgbValue & 0xff
        
        self.init(
            red: CGFloat(r) / 0xff,
            green: CGFloat(g) / 0xff,
            blue: CGFloat(b) / 0xff, alpha: 1
        )
    }
}


